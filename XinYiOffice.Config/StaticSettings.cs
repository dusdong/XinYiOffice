﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace XinYiOffice.Config
{
    public class StaticSettings
    {
        public static string SafetyKey = "xyoiloveyou";
        public static string Version = "1.0.1";

        public static string ResFilePath = "~/xyo_upload/";//系统资源上传路径



        /// <summary>
        /// 设置为one的时候表示独享模式,all则为租用模式
        /// </summary>
        public static string TenantModel {

            get
            {
                string _connectionString = ConfigurationManager.AppSettings["tenantmodel"]!=null?ConfigurationManager.AppSettings["tenantmodel"].ToString():string.Empty;

                return _connectionString;
            }
        }

        /// <summary>
        /// 删除保护，全站删除保护,所有数据不能删除 fase是自由删除,true 则不可以删除
        /// </summary>
        public static bool IsDeleteProtection {
            get {
                string _ts = ConfigurationManager.AppSettings["isdeleteprotection"] != null ?  ConfigurationManager.AppSettings["isdeleteprotection"].ToString() : "true";
                bool _t = true;
                _t = bool.Parse(_ts);

                return _t;
            }
        }
    }
}
