﻿using System;
using System.Collections.Generic;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Config;
using XinYiOffice.Model;
using M=XinYiOffice.Model;
using B= XinYiOffice.BLL;

namespace XinYiOffice.Web.UI
{
    public class BasicUserControl : System.Web.UI.UserControl
    {
        #region //属性成员
        /// <summary>
        /// 当前登录用户信息
        /// </summary>
        public Accounts CurrentAccount
        {
            get
            {
                if (_CurrentAccount == null)
                {
                    int uid = GetCliCookie();
                    if (uid != 0)
                    {
                        _CurrentAccount = new XinYiOffice.BLL.Accounts().GetModel(uid);
                    }
                }

                return _CurrentAccount;
            }

        }

        /// <summary>
        /// 当前登录的租户信息
        /// </summary>
        public M.LocalTenants CurrentTenant
        {
            get
            {
                if (_CurrentTenant == null)
                {
                    int tid = GetCliCookieTenant(); ;
                    if (tid != 0)
                    {
                        _CurrentTenant = new B.LocalTenants().GetModel(tid);
                    }
                }

                return _CurrentTenant;
            }
        }


        public string CurrentAccountNiceName
        {
            get
            {
                if (CurrentAccount != null)
                {
                    return CurrentAccount.NiceName;
                }

                return string.Empty;
            }
        }

        public int CurrentAccountId
        {
            get
            {
                if (CurrentAccount != null)
                {
                    return CurrentAccount.Id;
                }
                return 0;
            }
        }

        public int CurrentTenantId
        {
            get
            {
                if (CurrentTenant != null)
                {
                    return CurrentTenant.Id;
                }
                return 0;
            }
        }

        private Accounts _CurrentAccount;
        private M.LocalTenants _CurrentTenant;


        #endregion

        #region //方法
        public int GetCliCookie()
        {
            return SafeConvert.ToInt(DESEncrypt.Decrypt(CookieHelper.GetCookie("xyo_cookie_aid")));
        }

        /// <summary>
        /// 获取当前cookie的 租户ID
        /// </summary>
        /// <returns></returns>
        public int GetCliCookieTenant()
        {
            int i = 0;

            string str = SafeConvert.ToString(CookieHelper.GetCookie("xyo_cookie_tenantid"));
            if (!string.IsNullOrEmpty(str))
            {
                i = SafeConvert.ToInt(DESEncrypt.Decrypt(str));
            }

            return i;
        }

        #endregion
    }


}
