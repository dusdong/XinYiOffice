﻿<%@ Page Language="C#" MasterPageFile="~/MinLayer.Master" AutoEventWireup="true" CodeBehind="SelectAccount.aspx.cs" Inherits="XinYiOffice.Web.Sys.Accounts.SelectAccount" %>

<asp:Content ContentPlaceHolderID="head_layer" runat="server">

</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceBody" runat="server">
<form id="Form1" method="post" runat="server">
<INPUT id="hide" type="hidden" value="<%=pass()%>">
<table width="600" border="0">
  <tr>
    <td width="241">机构名:<asp:DropDownList ID="BranchDropList" runat="server" AutoPostBack="True" 
    DataTextField="BranchName" DataValueField="BranchID" Height="17px" 
   Width="174px">
</asp:DropDownList>

&nbsp;</td>
    <td width="113">&nbsp;</td>
    <td width="232">部门:
      <asp:DropDownList ID="DepartmentDropList" runat="server" AutoPostBack="True" 
    DataTextField="DepName" DataValueField="DepID" Height="23px" 
   Width="216px"> </asp:DropDownList>
&nbsp;</td>
  </tr>
  <tr>
    <td>人员：<br/>
    <asp:listbox id="UserList"  runat="server" Height="201px" Width="200px" SelectionMode="Multiple" DataTextField="name" DataValueField="empid"></asp:listbox>&nbsp;</td>
    <td><asp:Button ID="cmdAdd" runat="server" CssClass="bsbttn" Height="20px" Text="添加-&gt;" Width="52px" /><br/>
<asp:Button ID="cmdDel" runat="server" CssClass="bsbttn" Height="20px" Text="&lt;-删除" Width="52px" /><br/>
<asp:Button ID="cmdDelAll" runat="server" CssClass="bsbttn" Height="20px" Text="&lt;&lt;-删除" Width="52px" /><br/>
<asp:Button ID="cmdAddAll" runat="server" CssClass="bsbttn" Height="20px" Text="添加-&gt;&gt;" Width="52px" /><br/></td>
    <td>已选定人员：<br/>
<asp:listbox id="lstSelEmp"  runat="server" Height="202px" Width="211px"></asp:listbox>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><BUTTON id="ok" onclick="javascript:Ok();" type="button">确定</BUTTON>
<BUTTON id="cancel" onclick="javascript:Cancel();" type="button">取消</BUTTON></td>
  </tr>
</table>

</form>

<SCRIPT language="javascript">
	    function Ok() {
	        window.close();
	        opener.document.all.txtOtherMan.value = window.Form1.hide.value;
	    }
	    function Cancel() {
	        window.close();
	    }
</SCRIPT>
</asp:Content>

