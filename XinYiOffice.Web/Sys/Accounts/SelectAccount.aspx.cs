﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace XinYiOffice.Web.Sys.Accounts
{
    public partial class SelectAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        { 
            BranchDropList.DataSource=new BLL.Institution().GetAllList();
            BranchDropList.DataBind();
        }
        
    }
}