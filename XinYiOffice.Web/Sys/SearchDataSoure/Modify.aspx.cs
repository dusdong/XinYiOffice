﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.SearchDataSoure
{
    public partial class Modify : BasicPage
    {       

        		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					int Id=(Convert.ToInt32(Request.Params["id"]));
					ShowInfo(Id);
				}
			}
		}
			
	private void ShowInfo(int Id)
	{
		XinYiOffice.BLL.SearchDataSoure bll=new XinYiOffice.BLL.SearchDataSoure();
		XinYiOffice.Model.SearchDataSoure model=bll.GetModel(Id);
		this.lblId.Text=model.Id.ToString();
		this.txtDataSoureType.Text=model.DataSoureType.ToString();
		this.txtDataSoureConntion.Text=model.DataSoureConntion;
		this.txtDataType.Text=model.DataType.ToString();
		this.txtDisplayValue.Text=model.DisplayValue;
		this.txtTrueValue.Text=model.TrueValue;
		this.txtFormType.Text=model.FormType.ToString();
		this.txtFormStyle.Text=model.FormStyle;
		this.txtFormClass.Text=model.FormClass;
		this.txtCreateAccountId.Text=model.CreateAccountId.ToString();
		this.txtRefreshAccountId.Text=model.RefreshAccountId.ToString();
		this.txtCreateTime.Text=model.CreateTime.ToString();
		this.txtRefreshTime.Text=model.RefreshTime.ToString();

	}

		public void btnSave_Click(object sender, EventArgs e)
		{

			int Id=int.Parse(this.lblId.Text);
			int DataSoureType=int.Parse(this.txtDataSoureType.Text);
			string DataSoureConntion=this.txtDataSoureConntion.Text;
			int DataType=int.Parse(this.txtDataType.Text);
			string DisplayValue=this.txtDisplayValue.Text;
			string TrueValue=this.txtTrueValue.Text;
			int FormType=int.Parse(this.txtFormType.Text);
			string FormStyle=this.txtFormStyle.Text;
			string FormClass=this.txtFormClass.Text;
			int CreateAccountId=int.Parse(this.txtCreateAccountId.Text);
			int RefreshAccountId=int.Parse(this.txtRefreshAccountId.Text);
			DateTime CreateTime=DateTime.Parse(this.txtCreateTime.Text);
			DateTime RefreshTime=DateTime.Parse(this.txtRefreshTime.Text);


			XinYiOffice.Model.SearchDataSoure model=new XinYiOffice.Model.SearchDataSoure();
			model.Id=Id;
			model.DataSoureType=DataSoureType;
			model.DataSoureConntion=DataSoureConntion;
			model.DataType=DataType;
			model.DisplayValue=DisplayValue;
			model.TrueValue=TrueValue;
			model.FormType=FormType;
			model.FormStyle=FormStyle;
			model.FormClass=FormClass;
			model.CreateAccountId=CreateAccountId;
			model.RefreshAccountId=RefreshAccountId;
			model.CreateTime=CreateTime;
			model.RefreshTime=RefreshTime;
            model.TenantId = CurrentTenantId;

			XinYiOffice.BLL.SearchDataSoure bll=new XinYiOffice.BLL.SearchDataSoure();
			bll.Update(model);
			xytools.web_alert("保存成功！","list.aspx");

		}


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
