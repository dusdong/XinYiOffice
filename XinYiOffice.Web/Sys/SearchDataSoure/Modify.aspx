﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Sys.SearchDataSoure.Modify" Title="修改页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">
    <table style="width: 100%;" cellpadding="2" cellspacing="1" class="border">
        <tr>
            <td class="tdbg">
                
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		数据源 1-数据库,2-文本文
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDataSoureType" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		数据源连接字符串 若为数据库则
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDataSoureConntion" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		数据格式 字符串,xml,js
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDataType" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		代表显示的值 比如 查询出来的
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDisplayValue" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		代表实际的值 而 Id 则为下
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTrueValue" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		输入表单类型 1-文本,2-下
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFormType" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		表单样式 作为style=""
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFormStyle" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		表单使用Class
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFormClass" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		添加者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCreateAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		更新者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRefreshAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtCreateTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		更新时间 最近一次编辑的人员I
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtRefreshTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
</table>


            </td>
        </tr>
        <tr>
            <td class="tdbg" align="center" valign="bottom">
                <asp:Button ID="btnSave" runat="server" Text="保存"
                    OnClick="btnSave_Click" class="inputbutton" onmouseover="this.className='inputbutton_hover'"
                    onmouseout="this.className='inputbutton'"></asp:Button>
                <asp:Button ID="btnCancle" runat="server" Text="取消"
                    OnClick="btnCancle_Click" class="inputbutton" onmouseover="this.className='inputbutton_hover'"
                    onmouseout="this.className='inputbutton'"></asp:Button>
            </td>
        </tr>
    </table>
    </form>

</asp:Content>

 