﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.SearchDataSoure
{
    public partial class delete : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                XinYiOffice.BLL.SearchDataSoure bll = new XinYiOffice.BLL.SearchDataSoure();
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    bll.Delete(Id);
                    Response.Redirect("list.aspx");
                }
            }

        }
    }
}