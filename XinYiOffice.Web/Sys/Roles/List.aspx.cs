﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.Roles
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.Roles bll = new XinYiOffice.BLL.Roles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindData();
            }            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        #region gridView
        public void BindData()
        {
            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            strWhere.AppendFormat("  and TenantId={0} ", CurrentTenantId);
            ds = bll.GetList(strWhere.ToString());
            repRoles.DataSource = ds;
            repRoles.DataBind();
        }

        #endregion





    }
}
