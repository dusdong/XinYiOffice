﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.Roles
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

 

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string Name = this.txtName.Text;
            int CreateAccountId = SafeConvert.ToInt(CurrentAccountId);
            int RefreshAccountId = SafeConvert.ToInt(CurrentAccountId);
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.Roles model = new XinYiOffice.Model.Roles();
            model.Name = Name;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Roles bll = new XinYiOffice.BLL.Roles();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx");

        }
    }
}
