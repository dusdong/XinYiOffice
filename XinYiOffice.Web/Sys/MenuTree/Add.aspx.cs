﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Sys.MenuTree
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("MENUTREE_LIST_ADD"))
            {
                base.NoPermissionPage();
            }

            if(!IsPostBack)
            {
                txtGuid.Text = Guid.NewGuid().ToString();

                AppDataCacheServer.GetMenuTreeList(0,ref DropDownList_MenuTreeId);
                DropDownList_MenuTreeId.Items.Insert(0, new ListItem("无上级菜单", "0"));

            }
            
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string Guid = this.txtGuid.Text;
            string Name = this.txtName.Text;
            int MenuTreeId = SafeConvert.ToInt(DropDownList_MenuTreeId.SelectedValue);
            string ChainedAddress = this.txtChainedAddress.Text;
            int IsShow = int.Parse(this.txtIsShow.Text);
            int Sort = int.Parse(this.txtSort.Text);
            
            string Icon = this.txtIcon.Text;
            string IconMax = this.txtIconMax.Text;
            string Description = this.txtDescription.Text;
            
            int CreateAccountId = SafeConvert.ToInt(CurrentAccountId);
            int RefreshAccountId = SafeConvert.ToInt(CurrentAccountId);
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.MenuTree model = new XinYiOffice.Model.MenuTree();
            model.Guid = Guid;
            model.Name = Name;
            model.MenuTreeId = MenuTreeId;
            model.ChainedAddress = ChainedAddress;
            model.IsShow = IsShow;
            model.Sort = Sort;
            model.Icon = Icon;
            model.IconMax = IconMax;
            model.Description = Description;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.MenuTree bll = new XinYiOffice.BLL.MenuTree();
            bll.Add(model);

            AppDataCacheServer.ClearDataCacheByName("MenuTreeList");

            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
