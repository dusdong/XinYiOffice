﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Sys.PermissionCategories.Modify" Title="修改页" %>


<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

 <form id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>修改分类名</h4></div>
 
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		分类id
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		分类名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtName" runat="server" Width="200px"></asp:TextBox>
	    <asp:HiddenField ID="txtCreateAccountId" runat="server" />
        <asp:HiddenField ID="txtRefreshAccountId" runat="server" />
        <asp:HiddenField ID="txtCreateTime" runat="server" />
        <asp:HiddenField ID="txtRefreshTime" runat="server" />
	</td></tr>
</table>
</div>

   <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>
    </form>

</asp:Content>
