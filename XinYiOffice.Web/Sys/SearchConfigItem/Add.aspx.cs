﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Sys.SearchConfigItem
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            if (!string.IsNullOrEmpty(xytools.url_get("SearchConfigId")))
            {
                int SearchConfigId = SafeConvert.ToInt(xytools.url_get("SearchConfigId"), 0);
                Model.SearchConfig sc = new BLL.SearchConfig().GetModel(SearchConfigId);

                Label_SearchConfigId.Text = sc.Name;
                HiddenField_SearchConfigId.Value = sc.Id.ToString();

                DropDownList_txtFieldName.DataSource = AppDataCacheServer.GetAllColumsByTableName(sc.TableName);
                DropDownList_txtFieldName.DataTextField = "Name";
                DropDownList_txtFieldName.DataValueField = "Name";
                DropDownList_txtFieldName.DataBind();
            }
            else
            {
                //入口非法
                xytools.write("入口不正确");
            }

            
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string DisplayName = this.txtDisplayName.Text;
            string FieldName = DropDownList_txtFieldName.SelectedValue;
            int SearchDataSoureId = 0;
            int Sort = int.Parse(this.txtSort.Text);
            int ValueType = SafeConvert.ToInt(DropDownList_ValueType.SelectedValue);
            string ValueRelChar = SafeConvert.ToString(DropDownList_ValueRelChar.SelectedValue);
            string PrefixChar = this.txtPrefixChar.Text;
            string PrefixRelChar = DropDownList_PrefixRelChar.SelectedValue;
            string SuffixChar = this.txtSuffixChar.Text;
            string SuffixRelChar = DropDownList_SuffixRelChar.SelectedValue;
            int SearchConfigId = int.Parse(this.HiddenField_SearchConfigId.Value);
            int UseValueType = SafeConvert.ToInt(DropDownList_UseValueType.SelectedValue);
            string DefaultValue = this.txtDefaultValue.Text;
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            #region //添加数据源设置
            if (SafeConvert.ToInt(DropDownList_DataSoureType.SelectedValue) != 0)
            {
                XinYiOffice.Model.SearchDataSoure model_sds = new XinYiOffice.Model.SearchDataSoure();
                model_sds.DataSoureType = SafeConvert.ToInt(DropDownList_DataSoureType.SelectedValue);
                model_sds.DataSoureConntion = SafeConvert.ToString(txtDataSoureConntion.Text.Trim());
                model_sds.DataType = SafeConvert.ToInt(DropDownList_DataType.SelectedValue);
                model_sds.DisplayValue = txtDisplayValue.Text;
                model_sds.TrueValue = txtTrueValue.Text;
                model_sds.FormType = SafeConvert.ToInt(DropDownList_FormType.SelectedValue);
                model_sds.FormStyle = txtFormStyle.Text;
                model_sds.FormClass = txtFormClass.Text;
                model_sds.CreateAccountId = CreateAccountId;

                model_sds.CreateTime = CreateTime;

                XinYiOffice.BLL.SearchDataSoure bll_sds = new XinYiOffice.BLL.SearchDataSoure();
                SearchDataSoureId = bll_sds.Add(model_sds);
            }

            #endregion

            XinYiOffice.Model.SearchConfigItem model = new XinYiOffice.Model.SearchConfigItem();
            model.DisplayName = DisplayName;
            model.FieldName = FieldName;
            model.SearchDataSoureId = SearchDataSoureId;
            model.Sort = Sort;
            model.ValueType = ValueType;
            model.ValueRelChar = ValueRelChar;
            model.PrefixChar = PrefixChar;
            model.PrefixRelChar = PrefixRelChar;
            model.SuffixChar = SuffixChar;
            model.SuffixRelChar = SuffixRelChar;
            model.SearchConfigId = SearchConfigId;
            model.UseValueType = UseValueType;
            model.DefaultValue = DefaultValue;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.SearchConfigItem bll = new XinYiOffice.BLL.SearchConfigItem();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx?SearchConfigId=" + model.SearchConfigId);

        }
    }
}
