﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" enableEventValidation="false" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Sys.DictionaryTable.Add" Title="增加页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<script>
    $(function () {

        var ddlTableName = $("#<%=DropDownList_TableName.ClientID%>");
        var ddlFieldName = $("#<%=DropDownList_FieldName.ClientID%>");
        var hidFieldName = $("#<%=hid_FieldName.ClientID%>");

        ddlTableName.click(function () {
            var ddlTableNameValue = $("#ddlTableName").val();
            ddlFieldName.html("");
            $.getJSON("/Call/Ajax.aspx?action=getcolumns&tablename=" + ddlTableName.val() + "&sj=" + Math.random(), function (json) {
                //alert(json);
                $.each(json.Data, function (i) {

                    ddlFieldName.append("<option value='" + json.Data[i].Name + "'>" + json.Data[i].Name + "</option>");
                });
            });
        });

        ddlFieldName.change(function () {
            var ddlFieldNameValue = ddlFieldName.val();
            hidFieldName.val(ddlFieldNameValue);
        });

    })
</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form  id="f2" name="f2" runat="server">
<div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>添加字典</h4></div>
 
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		字典总名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		说明
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCon" runat="server" Width="425px" Height="67px" 
            TextMode="MultiLine"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		表名：</td>
	<td height="25" width="*" align="left">
	    <asp:DropDownList ID="DropDownList_TableName" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		字段名：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_FieldName" runat="server">
        </asp:DropDownList>
        
	    <asp:HiddenField ID="hid_FieldName" runat="server" />
        
	</td></tr>
	</table>
    </div>

<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
</div>

    </form>
</asp:Content>

