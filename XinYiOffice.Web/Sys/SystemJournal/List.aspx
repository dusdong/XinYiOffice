﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="XinYiOffice.Web.Sys.SystemJournal.List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cp2" runat="server">

<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            
            <a href="javascript:;" class="tdel">删除</a>
           </div>
                <div class="del clear"></div>
            </div>
            <form id="f2" name="f2"  >
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">
              
             
               
                </div>
                <div class="clear btn_box btn_box_p">
                <a href="javascript:;" class="save btn_search">查询</a>
               <a href="javascript:;" class="cancel res_search">取消</a>
                </div>
        	</div>
            </form>
        
        </div>
        
        <div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="ClientTracking_table">

                <asp:Repeater ID="repSystemJournal" runat="server" >
                <HeaderTemplate>
                <tr>
                <th><input type="checkbox" id="cb_all"></th>
                <th>操作内容</th>
                <th>创建时间</th>
                <th>当前用户</th>
                <th>IP地址</th>
      
                </tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td ><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td>
               <a href="Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>" title="<%#DataBinder.Eval(Container.DataItem, "OperationContent")%>">
               <%#Eval("OperationContent").ToString().Length > 50 ? Eval("OperationContent").ToString().Substring(0, 50) + "..." : Eval("OperationContent")%>
               </a>
               </td>
               <td><%#DataBinder.Eval(Container.DataItem, "CreateTime")%></td>
                <td><a href="/Sys/Accounts/Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "CurrentAccountId")%>" ><%#DataBinder.Eval(Container.DataItem, "AccountName")%></a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "IPAddress")%></td>
               
                </tr>    
                </ItemTemplate>
                </asp:Repeater>


            </table>
        </div>
        
</div>

<%=pagehtml%>

<script>
    $('#ClientTracking_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>

</asp:Content>

