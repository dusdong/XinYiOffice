﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"    AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Sys.Permissions.Modify" Title="修改页" %>

<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>修改权限节点名称</h4></div>
 
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		权限id
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		权限名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		系统内签名：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSign" runat="server" Width="200px"></asp:TextBox>
比如&quot;CONTENT_ARTICLE_ADD&quot;即为内容模块的 </td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		权限所属分类
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_PermissionCategoriesId" runat="server">
        </asp:DropDownList>
        <asp:HiddenField ID="HiddenField_txtCreateAccountId" runat="server" />
        <asp:HiddenField ID="HiddenField_txtRefreshAccountId" runat="server" />
        <asp:HiddenField ID="HiddenField_txtCreateTime" runat="server" />
        <asp:HiddenField ID="HiddenField_txtRefreshTime" runat="server" />
	</td></tr>
</table>
 </div>

 <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
          <a href="javascript:history.go(-1);" class="cancel">取消</a> 
        
    </div>
    </form>
</asp:Content>

 
