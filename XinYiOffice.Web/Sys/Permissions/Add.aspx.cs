﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.Permissions
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            DropDownList_PermissionCategoriesId.DataSource=new BLL.PermissionCategories().GetAllList();
            DropDownList_PermissionCategoriesId.DataTextField = "Name";
            DropDownList_PermissionCategoriesId.DataValueField = "Id";
            DropDownList_PermissionCategoriesId.DataBind();
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string Name = this.txtName.Text;
            string Sign = this.txtSign.Text;
            int PermissionCategoriesId = SafeConvert.ToInt(DropDownList_PermissionCategoriesId.SelectedValue);
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.Permissions model = new XinYiOffice.Model.Permissions();
            model.Name = Name;
            model.Sign = Sign;
            model.PermissionCategoriesId = PermissionCategoriesId;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Permissions bll = new XinYiOffice.BLL.Permissions();
            bll.Add(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
