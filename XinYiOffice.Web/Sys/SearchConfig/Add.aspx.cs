﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Sys.SearchConfig
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                InitData();
            }
        }
        protected void InitData()
        {
            txtGuid.Text=Guid.NewGuid().ToString();

            DropDownList_TableName.DataSource = AppDataCacheServer.GetAllTableName();
            DropDownList_TableName.DataTextField = "name";
            DropDownList_TableName.DataValueField = "name";
            DropDownList_TableName.DataBind();

            DropDownList_TableName.Items.Insert(0, new ListItem("选择一个系统表名", "0"));

        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string Guid = this.txtGuid.Text;
            string Name = this.txtName.Text;

            string TableName = this.HiddenField_DropDownList_TableName.Value;
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            DataSet ds = new BLL.SearchConfig().GetList(string.Format("TableName='{0}' TenantId={1} ", TableName,CurrentTenantId));
            if (ds != null && ds.Tables[0].Rows.Count >= 1)
            {
                xytools.web_alert("以存在此表设置,不能重复添加");
                return;
            }
            else
            {
                XinYiOffice.Model.SearchConfig model = new XinYiOffice.Model.SearchConfig();
                model.Guid = Guid;
                model.Name = Name;
                model.TableName = TableName;
                model.CreateAccountId = CreateAccountId;
                model.RefreshAccountId = RefreshAccountId;
                model.CreateTime = CreateTime;
                model.RefreshTime = RefreshTime;
                model.TenantId = CurrentTenantId;

                XinYiOffice.BLL.SearchConfig bll = new XinYiOffice.BLL.SearchConfig();
                bll.Add(model);
                xytools.web_alert("保存成功！", "add.aspx");
            }

        }
    }
}
