﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.SearchConfig
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.SearchConfig bll = new XinYiOffice.BLL.SearchConfig();
            XinYiOffice.Model.SearchConfig model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtGuid.Text = model.Guid;
            this.txtName.Text = model.Name;
            this.txtTableName.Text = model.TableName;

            HiddenField_CreateAccountId.Value = model.CreateAccountId.ToString();
            HiddenField_RefreshAccountId.Value =CurrentAccountId.ToString();

            HiddenField_CreateTime.Value = SafeConvert.ToString(model.CreateTime.ToString());
            HiddenField_RefreshTime.Value = SafeConvert.ToString(model.RefreshTime);

        }




        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string Guid = this.txtGuid.Text;
            string Name = this.txtName.Text;
            string TableName = this.txtTableName.Text;
            int CreateAccountId = SafeConvert.ToInt(HiddenField_CreateAccountId.Value);
            int RefreshAccountId = SafeConvert.ToInt(HiddenField_RefreshAccountId.Value);
            DateTime CreateTime = SafeConvert.ToDateTime(HiddenField_CreateTime.Value);
            DateTime RefreshTime = SafeConvert.ToDateTime(HiddenField_RefreshTime.Value);


            XinYiOffice.Model.SearchConfig model = new XinYiOffice.Model.SearchConfig();
            model.Id = Id;
            model.Guid = Guid;
            model.Name = Name;
            model.TableName = TableName;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.SearchConfig bll = new XinYiOffice.BLL.SearchConfig();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");

        }
    }
}
