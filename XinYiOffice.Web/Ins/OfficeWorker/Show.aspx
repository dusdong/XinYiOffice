﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Ins.OfficeWorker.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">
<div class="setup_box" style=" margin-bottom:0">
<div class="h_title"><h4>人员信息</h4></div>

<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		Id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		姓名
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblFullName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		曾用名
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblUsedName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		性别
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSex" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		电子邮件
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblEmail" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		电话
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblTel" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		移动电话
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPhone" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		内部编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblIntNumber" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属机构
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblInstitutionId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		部门
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDepartmentId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		职位
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPosition" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		住址-省
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblProvince" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		市
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCity" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		县
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCounty" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		街道
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblStreet" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		邮编
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblZipCode" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		出生年月
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblBirthDate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		身份证
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblChinaID" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		民族
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblNationality" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		籍贯
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblNativePlace" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		家庭电话1
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPhone1" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		家庭电话2
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPhone2" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		政治面貌
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPoliticalStatus" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		入职时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblEntryTime" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		进入方式
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblEntranceMode" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		岗位级别
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPostGrades" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		工资级别
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblWageLevel" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		保险福利
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblInsuranceWelfare" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		毕业学校
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblGraduateSchool" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		学历
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblFormalSchooling" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		专业
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblMajor" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		英语水平
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblEnglishLevel" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家工作单位
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPreWork" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家职位
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPrePosition" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家开始时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPreStartTime" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家结束时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPreEndTime" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上一家部门
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPreEpartment" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		离职时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblTurnoverTime" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		备注
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRemarks" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		添加者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		更新者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRefreshAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		更新时间 
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRefreshTime" runat="server"></asp:Label>
	</td></tr>
</table>
    </div>        </form>
</asp:Content>

