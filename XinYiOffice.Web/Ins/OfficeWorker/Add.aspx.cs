﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Ins.OfficeWorker
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("OFFICEWORKER_MANAGE") && ValidatePermission("OFFICEWORKER_MANAGE_LIST_ADD")))
            {
                base.NoPermissionPage();
            }

            if(!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData()
        {
            DropDownList_InstitutionId.DataSource = new BLL.Institution().GetList(string.Format(" TenantId={0} ", CurrentTenantId));
            DropDownList_InstitutionId.DataTextField="OrganizationName";
            DropDownList_InstitutionId.DataValueField="Id";
            DropDownList_InstitutionId.DataBind();
            DropDownList_InstitutionId.Items.Insert(0,new ListItem("请选择所属机构","0"));

            //DropDownList_Position 职位
            SetDropDownList("Position", ref DropDownList_Position);
            //状态
            SetDropDownList("Sate", ref DropDownList_Sate);

            //岗位级别
            SetDropDownList("PostGrades", ref DropDownList_PostGrades);

            //工资级别
            SetDropDownList("WageLevel", ref DropDownList_WageLevel);

            txtIntNumber.Text = string.Format("OW-{0}",new BLL.OfficeWorker().GetMaxId()+1);

        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.OfficeWorkerDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

            string FullName = this.txtFullName.Text;
            string UsedName = this.txtUsedName.Text;
            int Sex = SafeConvert.ToInt(DropDownList_Sex.SelectedValue);
            string Email = this.txtEmail.Text;
            string Tel = this.txtTel.Text;
            string Phone = this.txtPhone.Text;
            string IntNumber = this.txtIntNumber.Text;

            int InstitutionId = SafeConvert.ToInt(DropDownList_InstitutionId.SelectedValue);

            int DepartmentId = SafeConvert.ToInt(HiddenField_DepartmentId.Value);
            int Position = SafeConvert.ToInt(DropDownList_Position.SelectedValue);

            string Province = this.txtProvince.Text;
            string City = this.txtCity.Text;
            string County = this.txtCounty.Text;
            string Street = this.txtStreet.Text;
            string ZipCode = this.txtZipCode.Text;
            string BirthDate = this.txtBirthDate.Text;
            string ChinaID = this.txtChinaID.Text;
            string Nationality = this.txtNationality.Text;
            string NativePlace = this.txtNativePlace.Text;
            string Phone1 = this.txtPhone1.Text;
            string Phone2 = this.txtPhone2.Text;
            string PoliticalStatus = this.txtPoliticalStatus.Text;
            string EntryTime = this.txtEntryTime.Text;
            string EntranceMode = this.txtEntranceMode.Text;

            int PostGrades = SafeConvert.ToInt(DropDownList_PostGrades.SelectedValue);
            int WageLevel = SafeConvert.ToInt(DropDownList_WageLevel.SelectedValue);

            string InsuranceWelfare = this.txtInsuranceWelfare.Text;
            string GraduateSchool = this.txtGraduateSchool.Text;
            string FormalSchooling = this.txtFormalSchooling.Text;
            string Major = this.txtMajor.Text;
            string EnglishLevel = this.txtEnglishLevel.Text;
            string PreWork = this.txtPreWork.Text;
            string PrePosition = this.txtPrePosition.Text;
            string PreStartTime = this.txtPreStartTime.Text;
            string PreEndTime = this.txtPreEndTime.Text;
            string PreEpartment = this.txtPreEpartment.Text;
            string TurnoverTime = this.txtTurnoverTime.Text;
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
            string Remarks = this.txtRemarks.Text;


            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.OfficeWorker model = new XinYiOffice.Model.OfficeWorker();
            model.FullName = FullName;
            model.UsedName = UsedName;
            model.Sex = Sex;
            model.Email = Email;
            model.Tel = Tel;
            model.Phone = Phone;
            model.IntNumber = IntNumber;
            model.InstitutionId = InstitutionId;
            model.DepartmentId = DepartmentId;
            model.Position = Position;
            model.Province = Province;
            model.City = City;
            model.County = County;
            model.Street = Street;
            model.ZipCode = ZipCode;
            model.BirthDate = BirthDate;
            model.ChinaID = ChinaID;
            model.Nationality = Nationality;
            model.NativePlace = NativePlace;
            model.Phone1 = Phone1;
            model.Phone2 = Phone2;
            model.PoliticalStatus = PoliticalStatus;
            model.EntryTime = EntryTime;
            model.EntranceMode = EntranceMode;
            model.PostGrades = PostGrades;
            model.WageLevel = WageLevel;
            model.InsuranceWelfare = InsuranceWelfare;
            model.GraduateSchool = GraduateSchool;
            model.FormalSchooling = FormalSchooling;
            model.Major = Major;
            model.EnglishLevel = EnglishLevel;
            model.PreWork = PreWork;
            model.PrePosition = PrePosition;
            model.PreStartTime = PreStartTime;
            model.PreEndTime = PreEndTime;
            model.PreEpartment = PreEpartment;
            model.TurnoverTime = TurnoverTime;
            model.Sate = Sate;
            model.Remarks = Remarks;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.OfficeWorker bll = new XinYiOffice.BLL.OfficeWorker();
            bll.Add(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
