﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Ins.Department
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.Department bll = new XinYiOffice.BLL.Department();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("DEPARTMENT_MANAGE") && ValidatePermission("DEPARTMENT_MANAGE_LIST")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                BindData();
            }            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }
        
 
        
        #region gridView
                        
        public void BindData()
        {
            repDepartment.DataSource = DepartmentServer.GetDepartmentList(" 1=1 ",CurrentTenantId);
            repDepartment.DataBind();
        }
        #endregion





    }
}
