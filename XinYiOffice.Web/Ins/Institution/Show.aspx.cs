﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Ins.Institution
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("INSTITUTION_MANAGE") && ValidatePermission("INSTITUTION_MANAGE_LIST_VIEW")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.Institution bll = new XinYiOffice.BLL.Institution();
            XinYiOffice.Model.Institution model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.lblOrganizationName.Text = model.OrganizationName;
            this.lblIntroduction.Text = model.Introduction;
            this.lblLogo.Text = model.Logo;
            this.lblURL.Text = model.URL;
            this.lblCreateAccountId.Text = model.CreateAccountId.ToString();
            this.lblRefreshAccountId.Text = model.RefreshAccountId.ToString();
            this.lblCreateTime.Text = model.CreateTime.ToString();
            this.lblRefreshTime.Text = model.RefreshTime.ToString();

        }


    }
}
