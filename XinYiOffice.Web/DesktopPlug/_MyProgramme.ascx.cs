﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;
using XinYiOffice.Basic;
using System.Data;

namespace XinYiOffice.Web.DesktopPlug
{
    public partial class _MyProgramme : BasicUserControl
    {
        public int week;
        public DateTime dtNow;

        public DateTime dtimeWeek1;
        public DateTime dtimeWeek2;
        public DateTime dtimeWeek3;
        public DateTime dtimeWeek4;
        public DateTime dtimeWeek5;
        public DateTime dtimeWeek6;
        public DateTime dtimeWeek7;

        public DataTable MyProgramme;

        protected void Page_Load(object sender, EventArgs e)
        {
            week = (int)DateTime.Today.DayOfWeek;
            dtNow = DateTime.Now;

            InitData();
        }

        protected void InitData()
        {
            #region 计算本周时间
            switch (week)
            {
                case 0:
                    dtWeek7.BgColor = "#0066cc";
                    dtimeWeek7 = dtNow;
                    dtimeWeek6 = dtNow.AddDays(-1);
                    dtimeWeek5 = dtNow.AddDays(-2);
                    dtimeWeek4 = dtNow.AddDays(-3);
                    dtimeWeek3 = dtNow.AddDays(-4);
                    dtimeWeek2 = dtNow.AddDays(-5);
                    dtimeWeek1 = dtNow.AddDays(-6);
                    break;

                case 1:
                    dtWeek1.BgColor = "#0066cc";
                    dtimeWeek1 = dtNow;
                    dtimeWeek2 = dtNow.AddDays(1);
                    dtimeWeek3 = dtNow.AddDays(2);
                    dtimeWeek4 = dtNow.AddDays(3);
                    dtimeWeek5 = dtNow.AddDays(4);
                    dtimeWeek6 = dtNow.AddDays(5);
                    dtimeWeek7 = dtNow.AddDays(6);
                    break;

                case 2:
                    dtWeek2.BgColor = "#0066cc";
                    dtimeWeek1 = dtNow.AddDays(-1);
                    dtimeWeek2 = dtNow;
                    dtimeWeek3 = dtNow.AddDays(1);
                    dtimeWeek4 = dtNow.AddDays(1);
                    dtimeWeek5 = dtNow.AddDays(1);
                    dtimeWeek6 = dtNow.AddDays(1);
                    dtimeWeek7 = dtNow.AddDays(1);
                    break;

                case 3:
                    dtWeek3.BgColor = "#0066cc";
                    dtimeWeek1 = dtNow.AddDays(-2);
                    dtimeWeek2 = dtNow.AddDays(-1);
                    dtimeWeek3 = dtNow;
                    dtimeWeek4 = dtNow.AddDays(1);
                    dtimeWeek5 = dtNow.AddDays(2);
                    dtimeWeek6 = dtNow.AddDays(3);
                    dtimeWeek7 = dtNow.AddDays(4);
                    break;

                case 4:
                    dtWeek4.BgColor = "#0066cc";
                    dtimeWeek1 = dtNow.AddDays(-3);
                    dtimeWeek2 = dtNow.AddDays(-2);
                    dtimeWeek3 = dtNow.AddDays(-1);
                    dtimeWeek4 = dtNow;
                    dtimeWeek5 = dtNow.AddDays(1);
                    dtimeWeek6 = dtNow.AddDays(2);
                    dtimeWeek7 = dtNow.AddDays(3);
                    break;

                case 5:
                    dtWeek5.BgColor = "#0066cc";

                    dtimeWeek1 = dtNow.AddDays(-4);
                    dtimeWeek2 = dtNow.AddDays(-3);
                    dtimeWeek3 = dtNow.AddDays(-2);
                    dtimeWeek4 = dtNow.AddDays(-1);
                    dtimeWeek5 = dtNow;
                    dtimeWeek6 = dtNow.AddDays(1);
                    dtimeWeek7 = dtNow.AddDays(2);
                    break;

                case 6:
                    dtWeek6.BgColor = "#0066cc";

                    dtimeWeek1 = dtNow.AddDays(-5);
                    dtimeWeek2 = dtNow.AddDays(-4);
                    dtimeWeek3 = dtNow.AddDays(-3);
                    dtimeWeek4 = dtNow.AddDays(-2);
                    dtimeWeek5 = dtNow.AddDays(-1);
                    dtimeWeek6 = dtNow;
                    dtimeWeek7 = dtNow.AddDays(1);
                    break;


            }
            #endregion


            //dtWeek1Con.Controls.Add();

            MyProgramme = ProgrammeServer.GetMyProgramme(CurrentAccountId);

            SetWeekCon(dtWeek1Con, dtimeWeek1);
            SetWeekCon(dtWeek2Con, dtimeWeek2);
            SetWeekCon(dtWeek3Con, dtimeWeek3);
            SetWeekCon(dtWeek4Con, dtimeWeek4);
            SetWeekCon(dtWeek5Con, dtimeWeek5);
            SetWeekCon(dtWeek6Con, dtimeWeek6);
            SetWeekCon(dtWeek7Con, dtimeWeek7);

            SetQianDao();
        }

        protected void SetQianDao()
        {
            List<Model.Programme> pList = new BLL.Programme().GetModelList(string.Format(" CreateAccountId={0} and (DATEPART(year,StartTime)={1} and DATEPART(month,StartTime)={2} and DATEPART(day,StartTime)={3}) and ScheduleType=-99", CurrentAccountId, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
            if (pList != null && pList.Count > 0)
            {
                //证明有签到记录
                qiandao.Style.Value = "display:none;";
                qian_span.Style.Value = "display:inline; color:red;";

                //12点之后才显示签退按钮
                if (DateTime.Now.Hour >= 12)
                {
                    qiantui.Style.Value = "display:inline;";
                    qian_span.Style.Value = "display:none;";
                }

                //如果已经签退,那么就应该显示 已签退
                pList = new BLL.Programme().GetModelList(string.Format(" CreateAccountId={0} and (DATEPART(year,EndTime)={1} and DATEPART(month,EndTime)={2} and DATEPART(day,EndTime)={3}) and ScheduleType=-99", CurrentAccountId, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
                if (pList != null && pList.Count > 0)
                {
                    qian_span.Style.Value = "display:inline;color:#06C";
                    qian_span.InnerText = "已签退";
                    qiantui.Style.Value = "display:none;";
                    qiandao.Style.Value = "display:none;";
                }
            }
            else
            {
                qiandao.Style.Value = "display:inline;";
                qiantui.Style.Value = "display:none;";
                qian_span.Style.Value = "display:none;";
            }
        }

        protected void SetWeekCon(System.Web.UI.HtmlControls.HtmlTableCell htc, DateTime week)
        {
            DataRow[] dr = MyProgramme.Select(string.Format("'{0}'>=StartTime and '{0}'<=EndTime", week.ToString()));
            foreach (DataRow _dr in dr)
            {
                System.Web.UI.WebControls.HyperLink hl = new HyperLink();
                hl.Text = SafeConvert.ToString(_dr["Title"]);
                //hl.NavigateUrl = "/Programme/Programme.aspx";
                hl.ToolTip = xytools.CutString(SafeConvert.ToString(_dr["Note"]), 20);
                hl.Target = "_self";
                hl.Style.Add("display", "inline");
                htc.Controls.Add(hl);
            }
        }



    }
}