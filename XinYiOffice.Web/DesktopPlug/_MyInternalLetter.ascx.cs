﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;
using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.Web.DesktopPlug
{
    public partial class _MyInternalLetter : BasicUserControl
    {
        public string InternalLetterCount = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            InitData();
        }

        protected void InitData()
        {
            DataTable dt = InternalLetterServer.GetMyInternalLetter(CurrentAccountId, CurrentTenantId);
            InternalLetterCount = SafeConvert.ToInt(dt.Rows.Count,0).ToString();
            
            repInternalLetter.DataSource = dt; 
            repInternalLetter.DataBind();
        }
    }
}