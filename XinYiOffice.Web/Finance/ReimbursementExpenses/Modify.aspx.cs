﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Finance.ReimbursementExpenses
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("REIMBURSEMENTEXPENSES_MANAGE") && base.ValidatePermission("REIMBURSEMENTEXPENSES_MANAGE_LIST_EDIT")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.ReimbursementExpenses bll = new XinYiOffice.BLL.ReimbursementExpenses();
            XinYiOffice.Model.ReimbursementExpenses model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtCostTilte.Text = model.CostTilte;
            this.txtCostCon.Text = model.CostCon;
            this.txtRemarks.Text = model.Remarks;
            this.txtApplicationDate.Text = model.ApplicationDate.ToString();
            this.txtAmount.Text = model.Amount.ToString();
            
            //this.txtCostType.Text = model.CostType.ToString();
            this.txtBillNumber.Text = model.BillNumber.ToString();
            this.txtApplicantName.Text = model.ApplicantName;
            //this.txtUserAccountId.Text = model.UserAccountId.ToString();
            this.txtRelatedClient.Text = model.RelatedClient;
            this.txtRelatedProject.Text = model.RelatedProject;
            this.txtAuditOpinion.Text = model.AuditOpinion;
            //this.txtState.Text = model.State.ToString();
            //this.txtDealingPeopleAccountId.Text = model.DealingPeopleAccountId.ToString();
            this.txtCreateAccountId.Value = model.CreateAccountId.ToString();
            this.txtCreateTime.Value = model.CreateTime.ToString();
            InitData();
        }

        public void InitData()
        {
            DataTable dtAccountList = AccountsServer.GetAccountAllList(CurrentTenantId);

            DropDownList_UserAccountId.DataSource = dtAccountList;
            DropDownList_UserAccountId.DataTextField = "FullName";
            DropDownList_UserAccountId.DataValueField = "Id";
            DropDownList_UserAccountId.DataBind();

            DropDownList_DealingPeopleAccountId.DataSource = dtAccountList;
            DropDownList_DealingPeopleAccountId.DataTextField = "FullName";
            DropDownList_DealingPeopleAccountId.DataValueField = "Id";
            DropDownList_DealingPeopleAccountId.DataBind();

            SetDropDownList_ReimbursementExpenses("CostType", ref DropDownList_CostType);

            SetDropDownList_ReimbursementExpenses("State", ref DropDownList_State);
        }

        protected void SetDropDownList_ReimbursementExpenses(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ReimbursementExpensesDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string CostTilte = this.txtCostTilte.Text;
            string CostCon = this.txtCostCon.Text;
            string Remarks = this.txtRemarks.Text;
            DateTime ApplicationDate = DateTime.Parse(this.txtApplicationDate.Text);
            decimal Amount = decimal.Parse(this.txtAmount.Text);
            int CostType = SafeConvert.ToInt(DropDownList_CostType.SelectedValue);

            int BillNumber = int.Parse(this.txtBillNumber.Text);
            string ApplicantName = this.txtApplicantName.Text;
            int UserAccountId = SafeConvert.ToInt(DropDownList_UserAccountId.SelectedValue);
            string RelatedClient = this.txtRelatedClient.Text;
            string RelatedProject = this.txtRelatedProject.Text;
            string AuditOpinion = this.txtAuditOpinion.Text;
            int State = SafeConvert.ToInt(DropDownList_State.SelectedValue);
            int DealingPeopleAccountId = SafeConvert.ToInt(DropDownList_DealingPeopleAccountId.SelectedValue);
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Value);
            DateTime CreateTime = SafeConvert.ToDateTime(txtCreateTime.Value);


            XinYiOffice.Model.ReimbursementExpenses model = new XinYiOffice.Model.ReimbursementExpenses();
            model.Id = Id;
            model.CostTilte = CostTilte;
            model.CostCon = CostCon;
            model.Remarks = Remarks;
            model.ApplicationDate = ApplicationDate;
            model.Amount = Amount;
            model.CostType = CostType;
            model.BillNumber = BillNumber;
            model.ApplicantName = ApplicantName;
            model.UserAccountId = UserAccountId;
            model.RelatedClient = RelatedClient;
            model.RelatedProject = RelatedProject;
            model.AuditOpinion = AuditOpinion;
            model.State = State;
            model.DealingPeopleAccountId = DealingPeopleAccountId;
            model.CreateAccountId = CreateAccountId;
            model.CreateTime = CreateTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ReimbursementExpenses bll = new XinYiOffice.BLL.ReimbursementExpenses();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
