﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Finance.ReimbursementExpenses.Modify" Title="修改页" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <script src="/js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/js/chosen/chosen.css" />

        <script>
            $(function () {

                window.setTimeout(function () {
                    $(".chzn-select").chosen({ width: "220px" });
                }, 100);


            });
    </script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

 <form id="f2" name="f2" runat="server">
  <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>修改费用报销</h4></div>

  <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		费用主题
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCostTilte" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		内容描述
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCostCon" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		备注
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRemarks" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		申请日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtApplicationDate" runat="server" Width="200px"  
            class="Wdate"   onClick="WdatePicker()" ></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		费用金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAmount" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		费用类型：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_CostType" runat="server" >
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		票据张数
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBillNumber" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		申请人姓名
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtApplicantName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		申请人用户帐号 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_UserAccountId" runat="server"  CssClass="chzn-select">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		相关客户
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRelatedClient" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		相关项目
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRelatedProject" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		审核意见
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAuditOpinion" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_State" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		处理人
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_DealingPeopleAccountId" runat="server"  CssClass="chzn-select">
        </asp:DropDownList>
	    <asp:HiddenField ID="txtCreateTime" runat="server" />
        <asp:HiddenField ID="txtCreateAccountId" runat="server" />
	</td></tr>
	</table>

</div>

<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>

    </form>
    </asp:Content>