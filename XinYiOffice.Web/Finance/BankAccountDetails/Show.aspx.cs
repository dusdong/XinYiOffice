﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace XinYiOffice.Web.Finance.BankAccountDetails
{
    public partial class Show : Page
    {        
        		public string strid=""; 
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					strid = Request.Params["id"];
					int Id=(Convert.ToInt32(strid));
					ShowInfo(Id);
				}
			}
		}
		
	private void ShowInfo(int Id)
	{
		XinYiOffice.BLL.BankAccountDetails bll=new XinYiOffice.BLL.BankAccountDetails();
		XinYiOffice.Model.BankAccountDetails model=bll.GetModel(Id);
		this.lblId.Text=model.Id.ToString();
		this.lblBankAccountId.Text=model.BankAccountId.ToString();
		this.lblIncreaseOrReduce.Text=model.IncreaseOrReduce.ToString();
		this.lblSpecificAmount.Text=model.SpecificAmount;
		this.lblRemarks.Text=model.Remarks;
		this.lblOccurrenceTime.Text=model.OccurrenceTime.ToString();
		this.lblMakeCollectionsId.Text=model.MakeCollectionsId.ToString();
		this.lblAdvicePaymentId.Text=model.AdvicePaymentId.ToString();
		this.lblCreateAccountId.Text=model.CreateAccountId.ToString();
		this.lblCreateTime.Text=model.CreateTime.ToString();

	}


    }
}
