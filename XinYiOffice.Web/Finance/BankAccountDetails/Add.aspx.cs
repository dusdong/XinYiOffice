﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Finance.BankAccountDetails
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                       
        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int BankAccountId = int.Parse(this.txtBankAccountId.Text);
            int IncreaseOrReduce = int.Parse(this.txtIncreaseOrReduce.Text);
            string SpecificAmount = this.txtSpecificAmount.Text;
            string Remarks = this.txtRemarks.Text;
            DateTime OccurrenceTime = DateTime.Parse(this.txtOccurrenceTime.Text);
            int MakeCollectionsId = int.Parse(this.txtMakeCollectionsId.Text);
            int AdvicePaymentId = int.Parse(this.txtAdvicePaymentId.Text);
            int CreateAccountId = SafeConvert.ToInt(CurrentAccountId);
            DateTime CreateTime = DateTime.Now;

            XinYiOffice.Model.BankAccountDetails model = new XinYiOffice.Model.BankAccountDetails();
            model.BankAccountId = BankAccountId;
            model.IncreaseOrReduce = IncreaseOrReduce;
            model.SpecificAmount = SpecificAmount;
            model.Remarks = Remarks;
            model.OccurrenceTime = OccurrenceTime;
            model.MakeCollectionsId = MakeCollectionsId;
            model.AdvicePaymentId = AdvicePaymentId;
            model.CreateAccountId = CreateAccountId;
            model.CreateTime = CreateTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.BankAccountDetails bll = new XinYiOffice.BLL.BankAccountDetails();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx");
        }
    }
}
