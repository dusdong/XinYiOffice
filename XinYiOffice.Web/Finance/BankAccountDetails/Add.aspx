﻿<%@ Page Language="C#"  MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Finance.BankAccountDetails.Add" Title="增加页" %>

<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form  id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>添加资金明细表</h4></div>

  <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属银行帐号Id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBankAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		增加减少：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIncreaseOrReduce" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		具体金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSpecificAmount" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		备注
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRemarks" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		发生时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtOccurrenceTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款单 ：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtMakeCollectionsId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款单 ：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAdvicePaymentId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	</table>

</div>

<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>

    </form>
</asp:Content>
