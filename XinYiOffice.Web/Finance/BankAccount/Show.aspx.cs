﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;

namespace XinYiOffice.Web.BankAccount
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!(base.ValidatePermission("BANKACCOUNT_MANAGE") && base.ValidatePermission("BANKACCOUNT_MANAGE_LIST_VIEW")))
            {
                base.NoPermissionPage();
            } 

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            //XinYiOffice.BLL.BankAccount bll = new XinYiOffice.BLL.BankAccount();
            //XinYiOffice.Model.BankAccount model = bll.GetModel(Id);

            try
            {

                DataSet ds = XinYiOffice.Common.DbHelperSQL.Query(string.Format("select * from vBankAccount where Id={0}", Id));
                DataRow dr = ds.Tables[0].Rows[0];

                this.lblId.Text = SafeConvert.ToString(dr["Id"]);
                this.lblBankInfo.Text = SafeConvert.ToString(dr["BankInfo"]);
                this.lblBankAccountName.Text = SafeConvert.ToString(dr["BankAccountName"]);
                this.lblBankAccountNumber.Text = SafeConvert.ToString(dr["BankAccountNumber"]);
                this.lblBankAccountTel.Text = SafeConvert.ToString(dr["BankAccountTel"]);
                this.lblCashIn.Text = SafeConvert.ToString(dr["CashIn"]);
                this.lblSate.Text = SafeConvert.ToString(dr["SateName"]);
                this.lblOpeningTime.Text = SafeConvert.ToString(dr["OpeningTime"]);

                this.lblInstitutionId.Text = SafeConvert.ToString(dr["InstitutionName"]);
                this.lblDepartmentId.Text = SafeConvert.ToString(dr["DepartmentName"]);
                this.lblStorageAmount.Text = SafeConvert.ToString(dr["StorageAmount"]);
                this.lblBearerAccountId.Text = SafeConvert.ToString(dr["BearerAccountName"]);
                this.lblAccountType.Text = SafeConvert.ToString(dr["AccountType"]);
                this.lblAccountPurpose.Text = SafeConvert.ToString(dr["AccountPurpose"]);
                this.lblCreateAccountId.Text = SafeConvert.ToString(dr["CreateAccountId"]);
                this.lblCreateTime.Text = SafeConvert.ToString(dr["CreateTime"]);
            }
            catch( Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            finally
            { 
            }


        }


    }
}
