﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.BankAccount.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">

 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>银行帐号信息</h4></div>

<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		开户行信息 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblBankInfo" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		户名
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblBankAccountName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		银行帐号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblBankAccountNumber" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		联系手机号 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblBankAccountTel" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		现有金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCashIn" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		账户状态 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		开户时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblOpeningTime" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属机构
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblInstitutionId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属部门
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDepartmentId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		入库时金额 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblStorageAmount" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号持有人
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblBearerAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号类型 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAccountType" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号用途 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAccountPurpose" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号录入人
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
</table>

</div>
            </form>


</asp:Content>



