﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Finance.PayrollControl.Show" Title="显示页" %>


<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>工资单详情</h4></div>

  <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		工资年月日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPayDate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		基本工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblBasePay" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		考勤工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAttendanceWages" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		通讯费
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCommunicationExpense" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		个人所得税
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPersonalIncomeTax" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		住房公积金
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblHousingFund" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		剩余还款金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblResidualAmount" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		全部工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAllWages" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		加班工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblOvertimeWage" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		提成工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPercentageWages" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		其他补助
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblOtherBenefits" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		社保
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSocialSecurity" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		扣钱
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDeductMoney" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		本次抵扣还款金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRepaymentAmount" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		实际发放工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblActualPayment" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		支付账户
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblBankAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		人员帐号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPersonnelAccount" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		工资状态 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSate" runat="server"></asp:Label>
	&nbsp;1-已发,2-未发
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		工资单创建人
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
</table>

</div>
            </form>





</asp:Content>

