﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Finance.PayrollControl
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("PAYROLLCONTROL_MANAGE") && base.ValidatePermission("PAYROLLCONTROL_MANAGE_LIST_ADD")))
            {
                base.NoPermissionPage();
            }

            if(!IsPostBack)
            {
                InitData();
            }
                         
        }

        public void InitData()
        {
            DataTable dtAccountList = AccountsServer.GetAccountAllList(CurrentTenantId);

            DropDownList_PersonnelAccount.DataSource = dtAccountList;
            DropDownList_PersonnelAccount.DataTextField = "FullName";
            DropDownList_PersonnelAccount.DataValueField = "Id";
            DropDownList_PersonnelAccount.DataBind();

            //银行帐号
            DropDownList_BankAccountId.DataSource = new BLL.BankAccount().GetList(string.Format("1=1 and TenantId={0} ",CurrentTenantId)); ;
            DropDownList_BankAccountId.DataTextField = "BankAccountName";
            DropDownList_BankAccountId.DataValueField = "Id";
            DropDownList_BankAccountId.Items.Insert(0, new ListItem("暂无可选", "0"));
            DropDownList_BankAccountId.DataBind();

            SetDropDownList_PayrollControl("Sate", ref DropDownList_Sate);
        }

        protected void SetDropDownList_PayrollControl(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.PayrollControlDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            DateTime PayDate = DateTime.Parse(this.txtPayDate.Text);
            decimal BasePay = decimal.Parse(this.txtBasePay.Text);
            decimal AttendanceWages = decimal.Parse(this.txtAttendanceWages.Text);
            decimal CommunicationExpense = decimal.Parse(this.txtCommunicationExpense.Text);
            decimal PersonalIncomeTax = decimal.Parse(this.txtPersonalIncomeTax.Text);
            decimal HousingFund = decimal.Parse(this.txtHousingFund.Text);
            decimal ResidualAmount = decimal.Parse(this.txtResidualAmount.Text);
            decimal AllWages = decimal.Parse(this.txtAllWages.Text);
            decimal OvertimeWage = decimal.Parse(this.txtOvertimeWage.Text);
            decimal PercentageWages = decimal.Parse(this.txtPercentageWages.Text);
            decimal OtherBenefits = decimal.Parse(this.txtOtherBenefits.Text);
            decimal SocialSecurity = decimal.Parse(this.txtSocialSecurity.Text);
            decimal DeductMoney = decimal.Parse(this.txtDeductMoney.Text);
            decimal RepaymentAmount = decimal.Parse(this.txtRepaymentAmount.Text);
            decimal ActualPayment = decimal.Parse(this.txtActualPayment.Text);
            int BankAccountId = SafeConvert.ToInt(DropDownList_BankAccountId.SelectedValue);
            int PersonnelAccount = SafeConvert.ToInt(DropDownList_PersonnelAccount.SelectedValue);
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
            int CreateAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;

            XinYiOffice.Model.PayrollControl model = new XinYiOffice.Model.PayrollControl();
            model.PayDate = PayDate;
            model.BasePay = BasePay;
            model.AttendanceWages = AttendanceWages;
            model.CommunicationExpense = CommunicationExpense;
            model.PersonalIncomeTax = PersonalIncomeTax;
            model.HousingFund = HousingFund;
            model.ResidualAmount = ResidualAmount;
            model.AllWages = AllWages;
            model.OvertimeWage = OvertimeWage;
            model.PercentageWages = PercentageWages;
            model.OtherBenefits = OtherBenefits;
            model.SocialSecurity = SocialSecurity;
            model.DeductMoney = DeductMoney;
            model.RepaymentAmount = RepaymentAmount;
            model.ActualPayment = ActualPayment;
            model.BankAccountId = BankAccountId;
            model.PersonnelAccount = PersonnelAccount;
            model.Sate = Sate;
            model.CreateAccountId = CreateAccountId;
            model.CreateTime = CreateTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.PayrollControl bll = new XinYiOffice.BLL.PayrollControl();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx");
        }
    }
}
