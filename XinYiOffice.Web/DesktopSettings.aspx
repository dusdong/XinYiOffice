﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="DesktopSettings.aspx.cs" Inherits="XinYiOffice.Web.DesktopSettings" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <link href="/css/windowsz.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            height: 30px;
        }
    </style>

</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 <form id="form1" runat="server">
 
<div class="sz_box">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th width="20%">
                        是否显示在桌面
                    </th>
                    <th width="60%">
                        可选择的桌面项目
                    </th>
                    <th>
                        排序(数值从小到大)
                    </th>
                </tr>
               <asp:Repeater ID="repMyDesktopSettings" runat="server" OnItemDataBound="repMyDesktopSettings_OnItemDataBound">
               <ItemTemplate>
                <tr>
                    <td>
                        <asp:CheckBox ID="ck" runat="server" />
                        <asp:HiddenField ID="hid_DesktopId" runat="server" />
                        <asp:HiddenField ID="hid_DesktopPlugId" runat="server" />
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem, "Name")%>
                    </td>
                    <td>
                        <asp:TextBox ID="txt_sort" runat="server"></asp:TextBox>
                    </td>
                </tr>
                       </ItemTemplate>
               </asp:Repeater>

            </table>
            
        </div>

        <div class="clear btn_box">
            <asp:LinkButton ID="LinkButton1" runat="server" class="save" 
                onclick="LinkButton1_Click">保存</asp:LinkButton>
            <asp:LinkButton ID="LinkButton2" runat="server" class="cancel">取消</asp:LinkButton>
            
        </div>
</form>
</asp:Content>