﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="XinYiOffice.Web.Error" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML><HEAD><TITLE>所访问的资源不存在</TITLE>
<META content="text/html; charset=utf-8" http-equiv=Content-Type>
<STYLE>BODY {
	PADDING-BOTTOM: 32px; PADDING-LEFT: 32px; WIDTH: 80%; PADDING-RIGHT: 32px; FONT-FAMILY: verdana, arial, sans-serif; BACKGROUND: #F0F7FD; COLOR: #000; MARGIN-LEFT: auto; FONT-SIZE: 13px; MARGIN-RIGHT: auto; PADDING-TOP: 40px
}
DIV {
	BORDER-BOTTOM: #A0D1F0 1px solid; BORDER-LEFT: #A0D1F0 1px solid; PADDING-BOTTOM: 32px; PADDING-LEFT: 32px; PADDING-RIGHT: 32px; BACKGROUND: #fff; BORDER-TOP: #A0D1F0 1px solid; BORDER-RIGHT: #A0D1F0 1px solid; PADDING-TOP: 32px
}
H1 {
	PADDING-BOTTOM: 20px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: "trebuchet ms", ""lucida grande"", verdana, arial, sans-serif; COLOR: #0066cc; FONT-SIZE: 120%; FONT-WEIGHT: bold; PADDING-TOP: 0px
}
H2 {
	BORDER-BOTTOM: #ddd 1px solid; PADDING-BOTTOM: 0px; TEXT-TRANSFORM: uppercase; MARGIN: 0px 0px 8px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: "trebuchet ms", ""lucida grande"", verdana, arial, sans-serif; COLOR: #999; FONT-SIZE: 105%; FONT-WEIGHT: bold; PADDING-TOP: 0px
}
P {
	PADDING-BOTTOM: 6px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 6px
}
A:link {
	COLOR: #002c99; TEXT-DECORATION: none
}
A:visited {
	COLOR: #002c99; TEXT-DECORATION: none
}
A:hover {
	BACKGROUND-COLOR: #f5f5f5; COLOR: #cc0066; TEXT-DECORATION: underline
}
</STYLE>

<META name=GENERATOR content="MSHTML 8.00.6001.18241"></HEAD>
<BODY>
<FORM id=frmMain method=post>
<DIV>
<H1>对不起,请求可能由于服务器过忙或网络问题没能相应!</H1>
<H2>ERROR:404,ServerHostError</H2>
<P>URL:<%=sErrorUrl%><br/>所访问的资源不存在, 有关事宜至管理员联系。或致电XinYiOffice客服 400-8600-282</P>
<P style="MARGIN-TOP: 24px"><A 
href="/Desktop.aspx">返回桌面</A></P></DIV></FORM></BODY></HTML>