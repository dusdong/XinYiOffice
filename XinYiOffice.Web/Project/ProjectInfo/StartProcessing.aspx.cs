﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;
using XinYiOffice.Basic;
using System.Data;

namespace XinYiOffice.Web.Project.ProjectInfo
{
    public partial class StartProcessing : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindData();
            }
        }

        protected void BindData()
        {
            int pid=SafeConvert.ToInt( xytools.url_get("pid"));
            Model.ProjectInfo p=new BLL.ProjectInfo().GetModel(pid);

            if(p!=null)
            {
                HiddenField_ProctId.Value = SafeConvert.ToString(p.Id);
                lableTitle.Text = p.Title;

                SetDropDownList("State", ref DropDownList_State);
                DropDownList_State.SelectedValue = p.State.ToString();
            }
            
        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ProjectInfoDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int pid = SafeConvert.ToInt(HiddenField_ProctId.Value);
            Model.ProjectInfo p = new BLL.ProjectInfo().GetModel(pid);

            try
            {
                p.Remarks = txtTypeDescribe.Text;
                p.CreateTime = DateTime.Now;
                p.RefreshAccountId = CurrentAccountId;
                p.RefreshTime = DateTime.Now;

                p.State = SafeConvert.ToInt(DropDownList_State.SelectedValue);

                new BLL.ProjectInfo().Update(p);

                xytools.href_url_new("show.aspx?id="+p.Id);
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            finally { 
            
            }

        }
    }
}