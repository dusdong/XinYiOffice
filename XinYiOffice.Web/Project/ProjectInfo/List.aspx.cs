﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Web.Project.ProjectInfo
{
    public partial class List : BasicPage
    {

        XinYiOffice.BLL.ProjectInfo bll = new XinYiOffice.BLL.ProjectInfo();

        public int _page = 1;
        public int _pagezs = 0;
        public int _datazs = 0;
        string benye_url;//当前url
        public string pagehtml = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("PROJECTINFO_BASIC") && base.ValidatePermission("PROJECTINFO_MANAGE") && base.ValidatePermission("PROJECTINFO_LIST")))
            {
                base.NoPermissionPage();
            }

          
            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;

                    default:
                        BindData();
                        break;
                }
            }
            else if (!Page.IsPostBack)
            {
                BindData();
            }
        }


        #region gridView

        public void BindData()
        {
            string s = xytools.url_get("s");
            DataTable dt = new DataTable();
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            strWhere.Append(SearchApp1.SqlWhere);

            if(!string.IsNullOrEmpty(s))
            {
                switch(s)
                {
                    case "mylist":
                        if(strWhere.ToString()!=string.Empty)
                        {
                            strWhere.Append(" and ");
                        }
                        string myProject = string.Format("select {0} Id from vProjectInfoByAccount where AccountId={1} or OrganiserAccountId={1} or ExecutorAccountId={1} or ProjectManagerAccountId={1} or ByClientInfoId={1} or SurveyorAccountId={1}", string.Empty, CurrentAccountId);
                        strWhere.AppendFormat("Id in({0}) ",myProject);
                        
                        break;
                }
            }

        
            string mysql = ProjectInfoServer.GetProjectListSql(strWhere.ToString(), CurrentTenantId);

            try
            {
                #region 分页
                if (!string.IsNullOrEmpty(xytools.url_get("page")))
                {
                    _page = SafeConvert.ToInt(xytools.url_get("page"), 1);
                }
                PageDataSet pd = new PageDataSet();
                dt = pd.GetListPageBySql(mysql, _page, 10, ref _pagezs, ref _datazs);
                PageAttribute pa = new PageAttribute();
                pa.PagCur = _page.ToString();
                pa.PageLinage = "10";
                pa.PageShowFL = "1";
                pa.PageShowGo = "1";

                benye_url = "list.aspx?" + xyurl.GetQueryRemovePage(Request.QueryString);//获取当前url字符串
                pa.PageUrl = benye_url;
                pagehtml = ParseHTML.PageLabel(pa, _pagezs);
                #endregion

                repProject.DataSource = dt;
                repProject.DataBind();
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.write(mysql);
            }
            finally
            { 
            }


        }

        #endregion





    }
}
