﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.IssueTracker
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("ISSUETRACKER_LIST_EDIT"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.IssueTracker bll = new XinYiOffice.BLL.IssueTracker();
            XinYiOffice.Model.IssueTracker model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtProjectInfoId.Text = model.ProjectInfoId.ToString();
            this.txtTitle.Text = model.Title;
            this.txtTabloid.Text = model.Tabloid;
            this.txtItem.Text = model.Item;
            this.txtPriority.Text = model.Priority;
            this.txtSolver.Text = model.Solver;
            this.txtSate.Text = model.Sate.ToString();
            this.txtVersion.Text = model.Version;
            this.txtUrl.Text = model.Url;
            this.txtConstitute.Text = model.Constitute;
            this.txtSerious.Text = model.Serious;
            this.txtAuthorsAccountId.Text = model.AuthorsAccountId.ToString();
            this.txtAssignerAccountId.Text = model.AssignerAccountId.ToString();
            this.txtStartTime.Text = model.StartTime.ToString();
            this.txtEndTime.Text = model.EndTime.ToString();
            this.txtTYPE.Text = model.TYPE.ToString();
            this.txtIsEdit.Text = model.IsEdit.ToString();
            this.txtCreateAccountId.Text = model.CreateAccountId.ToString();
            this.txtRefreshAccountId.Text = model.RefreshAccountId.ToString();
            this.txtCreateTime.Text = model.CreateTime.ToString();
            this.txtRefreshTime.Text = model.RefreshTime.ToString();

        }

        public void btnSave_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            int ProjectInfoId = int.Parse(this.txtProjectInfoId.Text);
            string Title = this.txtTitle.Text;
            string Tabloid = this.txtTabloid.Text;
            string Item = this.txtItem.Text;
            string Priority = this.txtPriority.Text;
            string Solver = this.txtSolver.Text;
            int Sate = int.Parse(this.txtSate.Text);
            string Version = this.txtVersion.Text;
            string Url = this.txtUrl.Text;
            string Constitute = this.txtConstitute.Text;
            string Serious = this.txtSerious.Text;
            int AuthorsAccountId = int.Parse(this.txtAuthorsAccountId.Text);
            int AssignerAccountId = int.Parse(this.txtAssignerAccountId.Text);
            DateTime StartTime = DateTime.Parse(this.txtStartTime.Text);
            DateTime EndTime = DateTime.Parse(this.txtEndTime.Text);
            int TYPE = int.Parse(this.txtTYPE.Text);
            int IsEdit = int.Parse(this.txtIsEdit.Text);
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Text);
            int RefreshAccountId = int.Parse(this.txtRefreshAccountId.Text);
            DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Text);
            DateTime RefreshTime = DateTime.Parse(this.txtRefreshTime.Text);


            XinYiOffice.Model.IssueTracker model = new XinYiOffice.Model.IssueTracker();
            model.Id = Id;
            model.ProjectInfoId = ProjectInfoId;
            model.Title = Title;
            model.Tabloid = Tabloid;
            model.Item = Item;
            model.Priority = Priority;
            model.Solver = Solver;
            model.Sate = Sate;
            model.Version = Version;
            model.Url = Url;
            model.Constitute = Constitute;
            model.Serious = Serious;
            model.AuthorsAccountId = AuthorsAccountId;
            model.AssignerAccountId = AssignerAccountId;
            model.StartTime = StartTime;
            model.EndTime = EndTime;
            model.TYPE = TYPE;
            model.IsEdit = IsEdit;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.IssueTracker bll = new XinYiOffice.BLL.IssueTracker();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
