﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.IssueTracker
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("ISSUETRACKER_LIST_VIEW"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            DataTable dt= ProjectInfoServer.GetIssueTrackerList(string.Format("Id={0}",Id),CurrentTenantId);
            DataRow dr = dt.Rows[0];

            HiddenField_IssueTrackerId.Value = SafeConvert.ToString(dr["Id"]);
            SetDropDownList("Sate", ref DropDownList_Sate);
            TextBox_AssignerAccountId.Text = CurrentAccountTrueName;
            HiddenField_AssignerAccountId.Value = CurrentAccountId.ToString();

            HiddenField_ProjectInfoId.Value = SafeConvert.ToString(dr["ProjectInfoId"]);
            HiddenField_Type.Value = SafeConvert.ToString(dr["TYPE"]);
            //DropDownList_Sate;

            //一下为信息显示
            this.lblId.Text = SafeConvert.ToString(dr["Id"]);
            this.lblProjectInfoId.Text = SafeConvert.ToString(dr["ProjectInfoTitle"]); //model.ProjectInfoId.ToString();
            this.lblTitle.Text = SafeConvert.ToString(dr["Title"]);//model.Title;
            //this.lblTabloid.Text = model.Tabloid;
            this.lblItem.Text = SafeConvert.ToString(dr["Item"]);//model.Item;
            this.lblPriority.Text = SafeConvert.ToString(dr["Priority"]); //model.Priority;
            this.lblSolver.Text = SafeConvert.ToString(dr["AssignerAccountName"]); //model.Solver;
            this.lblSate.Text = SafeConvert.ToString(dr["SateName"]);//model.Sate.ToString();
            this.lblVersion.Text = SafeConvert.ToString(dr["Version"]);//model.Version;
            this.lblUrl.Text = SafeConvert.ToString(dr["Url"]);//model.Url;
            this.lblConstitute.Text = SafeConvert.ToString(dr["Constitute"]); //model.Constitute;
            this.lblSerious.Text = SafeConvert.ToString(dr["Serious"]); //model.Serious;
            this.lblAuthorsAccountId.Text=SafeConvert.ToString(dr["AuthorsAccountlName"]); //model.AuthorsAccountId.ToString();
            //this.lblAssignerAccountId.Text = model.AssignerAccountId.ToString();
            
            this.lblStartTime.Text = SafeConvert.ToString(dr["StartTime"]); //model.StartTime.ToString();
            this.lblEndTime.Text = SafeConvert.ToString(dr["EndTime"]); //model.EndTime.ToString();
            this.lblTYPE.Text = SafeConvert.ToString(dr["ClassName"]); //model.TYPE.ToString();
            this.lblIsEdit.Text = SafeConvert.ToString(dr["IsEdit"])=="1"?"锁定":"未锁定";  //model.IsEdit.ToString();
            this.lblCreateAccountId.Text = SafeConvert.ToString(dr["CreateAccountName"]);  //model.CreateAccountId.ToString();
            this.lblRefreshAccountId.Text = SafeConvert.ToString(dr["RefreshAccountName"]);  //model.RefreshAccountId.ToString();
            this.lblCreateTime.Text = SafeConvert.ToString(dr["CreateTime"]); //model.CreateTime.ToString();
            this.lblRefreshTime.Text = SafeConvert.ToString(dr["RefreshTime"]); //model.RefreshTime.ToString();

            //以下为 跟踪处理
            string strWhere = string.Empty;
            strWhere += String.Format("IssueTrackerId={0}", Id);

            repIssueTrackerSubsequent.DataSource = ProjectInfoServer.GetIssueTrackerSubsequentList(strWhere);
            repIssueTrackerSubsequent.DataBind();

        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.IssueTrackerDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                Model.IssueTrackerSubsequent its = new Model.IssueTrackerSubsequent();
                its.AssignerAccountId = SafeConvert.ToInt(HiddenField_AssignerAccountId.Value, 0);
                its.Con = SafeConvert.ToString(TextBox_Con.Text);
                its.CreateAccountId = CurrentAccountId;
                its.CreateTime = DateTime.Now;
                its.IssueTrackerId = SafeConvert.ToInt(HiddenField_IssueTrackerId.Value, 0);
                its.RefreshAccountId = CurrentAccountId;
                its.RefreshTime = DateTime.Now;
                its.Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue, 0);//处理追踪表 状态
                its.Id=new BLL.IssueTrackerSubsequent().Add(its);

                #region //上传附件
                ResAttachmentServer.UpLoadRes(Request.Files, "IssueTrackerSubsequent", its.Id.ToString(), CurrentAccount);
                #endregion

                int IssueTrackerId = SafeConvert.ToInt(its.IssueTrackerId);

                #region //更新追踪主表
                Model.IssueTracker it = new BLL.IssueTracker().GetModel(IssueTrackerId);
                it.Sate = its.Sate;
                it.RefreshTime = its.CreateTime;
                it.RefreshAccountId = CurrentAccountId;
                it.AssignerAccountId = its.AssignerAccountId;
                it.EndTime = its.CreateTime;
                new BLL.IssueTracker().Update(it);
                #endregion

                int pid = SafeConvert.ToInt(HiddenField_ProjectInfoId.Value, 0);
                int type = SafeConvert.ToInt(HiddenField_Type.Value, 0);

                xytools.href_url(string.Format("list.aspx?projectId={0}&typeId={1}", pid, type));
            }
            catch (Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.web_alert("服务器忙,请稍候");
            }
            finally
            {
            }


        }

        protected void repIssueTrackerSubsequent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater repResAttachment = (Repeater)e.Item.FindControl("repResAttachment") as Repeater;
            DataRowView dv = (DataRowView)e.Item.DataItem;

            repResAttachment.DataSource = new BLL.ResAttachment().GetList(string.Format("KeyTable='{0}' and KeyId={1} and TenantId={2} ", "IssueTrackerSubsequent", SafeConvert.ToInt(dv["Id"], 0),CurrentTenantId));
            repResAttachment.DataBind();
        }


        public string GetAccountsHeadPortrait(string _head)
        {
            return AccountsServer.GetAccountsHeadPortrait(_head);
        }

    }
}
