﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Project.IssueTracker.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link href="/css/project.css" rel="stylesheet" type="text/css" />

<script src="/js/ui/jquery.ui.core.js"></script>
	<script src="/js/ui/jquery.ui.widget.js"></script>
	<script src="/js/ui/jquery.ui.position.js"></script>
	<script src="/js/ui/jquery.ui.autocomplete.js"></script>
	<link rel="stylesheet" href="/js/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="/js/themes/base/jquery.ui.autocomplete.css">



<style>
.setup_box table .table_left
{
    width:10%;
}
.setup_box_cl table td{ padding-left:10px; padding-top:5px; padding-bottom:5px; line-height:30px; height:30px;}
.plan_table input, .plan_table select
{
    width:200px;
}
.plan_table input.wj
{
    width:250px;
}
</style>

<script>
    $(function () {

        var TextBox_AssignerAccountId = $("#<%=TextBox_AssignerAccountId.ClientID%>");
        var HiddenField_IssueTrackerId = $("#<%=HiddenField_AssignerAccountId.ClientID%>");
        var url_getwork = "/Call/Ajax.aspx?action=getofficeworkeraccount&sj=" + Math.random();

        //解决者
        TextBox_AssignerAccountId.autocomplete({
            source: url_getwork,
            select: function (event, ui) {
                //alert(ui.item.value);
                TextBox_AssignerAccountId.val(ui.item.label);
                HiddenField_IssueTrackerId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                TextBox_AssignerAccountId.val(ui.item.label);
                return false;
            }
        });


        $("#btn_sing").toggle(function () {
			$("#tr_form").show();
            $("#btn_sing").val("取消");

        },
        function () {

            $("#tr_form").hide();
            $("#btn_sing").val("开始解决");
        }
        );

        $(".cancel").click(function () {

            $("#tr_form").hide();
			$("#btn_sing").val("开始解决");
        });
    })
</script>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server" enctype="multipart/form-data">

<div class="setup_box">
      <div class="h_title clear"><h4>监视追踪项</h4></div>
        <table cellpadding="0" cellspacing="0" class="plan_table">
            <tr><td class="table_left">议题追踪标题：</td><td colspan="3"><strong>
		<asp:Label id="lblTitle" runat="server"></asp:Label>
	            </strong>(编号:<asp:Label id="lblId" runat="server"></asp:Label>
	            )</td></tr>
            <tr>
              <td class="table_left">所属项目：</td>
              <td colspan="3">
		<asp:Label id="lblProjectInfoId" runat="server"></asp:Label>
	            </td>
            </tr>
            <tr><td class="table_left">描述：</td><td colspan="3">
		<asp:Label id="lblItem" runat="server"></asp:Label>
	            </td></tr>
            <tr>
              <td class="table_left">发起者：</td>
              <td><asp:Label id="lblAuthorsAccountId" runat="server"></asp:Label></td>
              <td class="table_left">解决者：</td>
              <td>
		<asp:Label id="lblSolver" runat="server"></asp:Label>
	            </td>
            </tr>
            <tr>
              <td class="table_left">开启时间：</td>
              <td><asp:Label id="lblStartTime" runat="server"></asp:Label></td>
              <td class="table_left">解决时间：</td>
              <td><asp:Label id="lblEndTime" runat="server"></asp:Label></td>
            </tr>
            <tr>
              <td class="table_left">状态：</td>
              <td>
		<asp:Label id="lblSate" runat="server"></asp:Label>
	            </td>
              <td class="table_left">优先级：</td>
              <td>
		<asp:Label id="lblPriority" runat="server"></asp:Label>
	            </td>
            </tr>
            <tr>
              <td class="table_left">版本：</td>
              <td>
		<asp:Label id="lblVersion" runat="server"></asp:Label>
	            </td>
              <td class="table_left">URL：</td>
              <td>
		<asp:Label id="lblUrl" runat="server"></asp:Label>
	            </td>
            </tr>
            <tr>
              <td class="table_left">影响度：</td>
              <td>
		<asp:Label id="lblSerious" runat="server"></asp:Label>
	            </td>
              <td class="table_left">所属类型：</td>
              <td><asp:Label id="lblTYPE" runat="server"></asp:Label></td>
            </tr>
            <tr>
              <td class="table_left">组成
	    ：</td>
              <td><asp:Label id="lblConstitute" runat="server"></asp:Label></td>
              <td class="table_left">修改锁定：</td>
              <td><asp:Label id="lblIsEdit" runat="server"></asp:Label></td>
            </tr>
            <tr>
              <td class="table_left">创建者：</td>
              <td><asp:Label id="lblCreateAccountId" runat="server"></asp:Label></td>
              <td class="table_left">创建时间：</td>
              <td><asp:Label id="lblCreateTime" runat="server"></asp:Label></td>
            </tr>
            <tr>
              <td class="table_left">更新时间：</td>
              <td><asp:Label id="lblRefreshTime" runat="server"></asp:Label>&nbsp;</td>
              <td class="table_left">最后更新人：</td>
              <td><asp:Label id="lblRefreshAccountId" runat="server"></asp:Label></td>
            </tr>
            <tr>
              <td colspan="4" class=""><input type="button" value="开始解决" id="btn_sing" name="btn_sing"/>
              </td>
            </tr>


        </table>
    </div>

<div class="setup_box" id="tr_form" style="display:none;">
      <div class="h_title clear"><h4>开始处理</h4></div>
    <table cellpadding="0" cellspacing="0" class="plan_table">
            <tr>
            	<td class="table_left">
处理结果：
            	</td>
                <td class="table_right">
                    <asp:DropDownList ID="DropDownList_Sate" runat="server">
                    </asp:DropDownList>
                    <asp:HiddenField ID="HiddenField_IssueTrackerId" runat="server" />
                </td>
            </tr>
            <tr>
              <td class="table_left">解决人：</td>
              <td class="table_right">
                  <asp:TextBox ID="TextBox_AssignerAccountId" runat="server"></asp:TextBox>
                  <asp:HiddenField ID="HiddenField_AssignerAccountId" runat="server" />
                </td>
            </tr>
            <tr>
              <td class="table_left">解决备注：</td>
              <td class="table_right">
                  <asp:TextBox ID="TextBox_Con" runat="server" Height="118px" TextMode="MultiLine" 
                      Width="520px"></asp:TextBox>
                  <asp:HiddenField ID="HiddenField_Type" runat="server" />
                  <asp:HiddenField ID="HiddenField_ProjectInfoId" runat="server" />
                </td>
            </tr>
            <tr>
              <td class="table_left">附件资源：</td>
              <td class="table_right">
              
              <a href="javascript:addimg()" >增加</a> 
                <div class="mdiv" id="mdiv"> 
                    <div class="iptdiv" > 
                  <!--  <input type="file" name="file_res" class="wj" /><a href="javascript:void(0);" name="rmlink">[X]</a> -->
                    </div> 
                </div> 
                
                </td>

            </tr>
            </table>
            
        <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">提交</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
        </div>
        </div>
    
<div class="setup_box_cl">
        <div class="h_title clear"><h4>处理历史</h4></div>
        
        <asp:Repeater ID="repIssueTrackerSubsequent" runat="server" 
            onitemdatabound="repIssueTrackerSubsequent_ItemDataBound">
        <ItemTemplate>
        <table cellpadding="0" cellspacing="0" class="plan_table">
            <tr>
            	<td class="table_left">
            		<div>
                    	<a href="javascript:;" class="tx_img"><img src='<%#GetAccountsHeadPortrait(DataBinder.Eval(Container.DataItem,"HeadPortrait").ToString())%>' /></a>
                        <span><a href="javascript:;" class="user_name"><%#DataBinder.Eval(Container.DataItem, "AssignerAccountName")%></a></span>
                        <p><%#DataBinder.Eval(Container.DataItem, "CreateTime", "{0:yyyy-MM-dd}")%><br /><%#DataBinder.Eval(Container.DataItem, "CreateTime", "{0:hh:mm:ss}")%></p>
                    </div>
            	</td>
                <td class="table_right">
                	<p class="text"><%#DataBinder.Eval(Container.DataItem, "Con")%></p>
                    <ul class="clear">
                    <asp:Repeater ID="repResAttachment" runat="server">
                    <ItemTemplate>
                       <li><%#DataBinder.Eval(Container.DataItem, "Title")%><a href="<%#DataBinder.Eval(Container.DataItem, "Annex")%>">下载</a></li>
                    </ItemTemplate>
                    </asp:Repeater>
                    	
                    </ul>
                    <span>处理后状态：<a href="javascript:;"> <%#DataBinder.Eval(Container.DataItem, "SateName")%> </a></span>
                </td>
            </tr>
        </table>
        </ItemTemplate>
        </asp:Repeater>
        
        
        
    </div>
    
</form>


<script type="text/javascript" > 
$(document).ready(function(){
   bindListener();
});
// 用来绑定事件(使用unbind避免重复绑定)
function bindListener(){
     $("a[name=rmlink]").unbind().click(function(){
         $(this).parent().remove(); 
     })
}
function addimg(){
    $("#mdiv").append('<div class="iptdiv"><input type="file" name="file_res" class="wj" /><a href="javascript:void(0);" name="rmlink">[X]</a></div>');

// 为新元素节点添加事件侦听器
//download jb51.net
   bindListener();
} 
</script>
</asp:Content>



