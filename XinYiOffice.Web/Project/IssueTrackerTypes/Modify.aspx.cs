﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.IssueTrackerTypes
{
    public partial class Modify : BasicPage
    {       

        		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					int Id=(Convert.ToInt32(Request.Params["id"]));
					ShowInfo(Id);
				}
			}
		}
			
	private void ShowInfo(int Id)
	{
		XinYiOffice.BLL.IssueTrackerTypes bll=new XinYiOffice.BLL.IssueTrackerTypes();
		XinYiOffice.Model.IssueTrackerTypes model=bll.GetModel(Id);
		this.lblId.Text=model.Id.ToString();
		this.txtClassName.Text=model.ClassName;
		this.txtSuggest.Text=model.Suggest;
		this.txtCreateAccountId.Text=model.CreateAccountId.ToString();
		this.txtRefreshAccountId.Text=model.RefreshAccountId.ToString();
		this.txtCreateTime.Text=model.CreateTime.ToString();
		this.txtRefreshTime.Text=model.RefreshTime.ToString();

	}

		public void btnSave_Click(object sender, EventArgs e)
		{
			
            //string strErr="";
            //if(this.txtClassName.Text.Trim().Length==0)
            //{
            //    strErr+="分类名不能为空！\\n";	
            //}
            //if(this.txtSuggest.Text.Trim().Length==0)
            //{
            //    strErr+="介绍不能为空！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtCreateAccountId.Text))
            //{
            //    strErr+="添加者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtRefreshAccountId.Text))
            //{
            //    strErr+="更新者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtCreateTime.Text))
            //{
            //    strErr+="创建时间格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtRefreshTime.Text))
            //{
            //    strErr+="更新时间 最近一次编辑的人员I格式错误！\\n";	
            //}

            //if(strErr!="")
            //{
            //    MessageBox.Show(this,strErr);
            //    return;
            //}
			int Id=int.Parse(this.lblId.Text);
			string ClassName=this.txtClassName.Text;
			string Suggest=this.txtSuggest.Text;
			int CreateAccountId=int.Parse(this.txtCreateAccountId.Text);
			int RefreshAccountId=int.Parse(this.txtRefreshAccountId.Text);
			DateTime CreateTime=DateTime.Parse(this.txtCreateTime.Text);
			DateTime RefreshTime=DateTime.Parse(this.txtRefreshTime.Text);


			XinYiOffice.Model.IssueTrackerTypes model=new XinYiOffice.Model.IssueTrackerTypes();
			model.Id=Id;
			model.ClassName=ClassName;
			model.Suggest=Suggest;
			model.CreateAccountId=CreateAccountId;
			model.RefreshAccountId=RefreshAccountId;
			model.CreateTime=CreateTime;
			model.RefreshTime=RefreshTime;
            model.TenantId = CurrentTenantId;

			XinYiOffice.BLL.IssueTrackerTypes bll=new XinYiOffice.BLL.IssueTrackerTypes();
			bll.Update(model);
			xytools.web_alert("保存成功！","list.aspx");

		}


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
