﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectMembers
{
    public partial class Modify : BasicPage
    {       

        		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					int Id=(Convert.ToInt32(Request.Params["id"]));
					ShowInfo(Id);
				}
			}
		}
			
	private void ShowInfo(int Id)
	{
		XinYiOffice.BLL.ProjectMembers bll=new XinYiOffice.BLL.ProjectMembers();
		XinYiOffice.Model.ProjectMembers model=bll.GetModel(Id);
		this.lblId.Text=model.Id.ToString();
		this.txtProjectId.Text=model.ProjectId.ToString();
		this.txtAccountId.Text=model.AccountId.ToString();
		this.txtProjectRoleId.Text=model.ProjectRoleId.ToString();
		this.txtCreateAccountId.Text=model.CreateAccountId.ToString();
		this.txtRefreshAccountId.Text=model.RefreshAccountId.ToString();
		this.txtCreateTime.Text=model.CreateTime.ToString();
		this.txtRefreshTime.Text=model.RefreshTime.ToString();

	}

		public void btnSave_Click(object sender, EventArgs e)
		{
			
            //string strErr="";
            //if(!PageValidate.IsNumber(txtProjectId.Text))
            //{
            //    strErr+="项目ID格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtAccountId.Text))
            //{
            //    strErr+="用户id格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtProjectRoleId.Text))
            //{
            //    strErr+="所担当的角色格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtCreateAccountId.Text))
            //{
            //    strErr+="添加者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtRefreshAccountId.Text))
            //{
            //    strErr+="更新者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtCreateTime.Text))
            //{
            //    strErr+="创建时间格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtRefreshTime.Text))
            //{
            //    strErr+="更新时间 最近一次编辑的人员I格式错误！\\n";	
            //}

            //if(strErr!="")
            //{
            //    MessageBox.Show(this,strErr);
            //    return;
            //}
			int Id=int.Parse(this.lblId.Text);
			int ProjectId=int.Parse(this.txtProjectId.Text);
			int AccountId=int.Parse(this.txtAccountId.Text);
			int ProjectRoleId=int.Parse(this.txtProjectRoleId.Text);
			int CreateAccountId=int.Parse(this.txtCreateAccountId.Text);
			int RefreshAccountId=int.Parse(this.txtRefreshAccountId.Text);
			DateTime CreateTime=DateTime.Parse(this.txtCreateTime.Text);
			DateTime RefreshTime=DateTime.Parse(this.txtRefreshTime.Text);


			XinYiOffice.Model.ProjectMembers model=new XinYiOffice.Model.ProjectMembers();
			model.Id=Id;
			model.ProjectId=ProjectId;
			model.AccountId=AccountId;
			model.ProjectRoleId=ProjectRoleId;
			model.CreateAccountId=CreateAccountId;
			model.RefreshAccountId=RefreshAccountId;
			model.CreateTime=CreateTime;
			model.RefreshTime=RefreshTime;
            model.TenantId = CurrentTenantId;
			XinYiOffice.BLL.ProjectMembers bll=new XinYiOffice.BLL.ProjectMembers();
			bll.Update(model);
			xytools.web_alert("保存成功！","list.aspx");
            
		}


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
