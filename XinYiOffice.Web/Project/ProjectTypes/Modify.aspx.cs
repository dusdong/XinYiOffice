﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectTypes
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("PROJECTTYPES_LIST_EDIT"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.ProjectTypes bll = new XinYiOffice.BLL.ProjectTypes();
            XinYiOffice.Model.ProjectTypes model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtTypeName.Text = model.TypeName;
            this.txtTypeDescribe.Text = model.TypeDescribe;
            if (!string.IsNullOrEmpty(model.TypeIco))
            {
                Image_TypeIco.ImageUrl = model.TypeIco;
            }
            else
            {
                Image_TypeIco.Visible = false;
            }

            if (!string.IsNullOrEmpty(model.TypeIcoMax))
            {
                Image_TypeIcoMax.ImageUrl = model.TypeIcoMax;
            }
            else
            {
                Image_TypeIcoMax.Visible = false;
            }

            HiddenField_CreateAccountId.Value=  model.CreateAccountId.ToString();
            HiddenField_CreateTime.Value = model.CreateTime.ToString();
            HiddenField_RefreshAccountId.Value = CurrentAccountId.ToString();
            HiddenField_RefreshTime.Value = DateTime.Now.ToString();

        }

       


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string TypeName = this.txtTypeName.Text;
            string TypeDescribe = this.txtTypeDescribe.Text;
            string TypeIco =string.Empty;
            if (!string.IsNullOrEmpty(FileUpload_TypeIco.FileName))
            {
                TypeIco = WebUpLoad.PostActionUpLoad(FileUpload_TypeIco.ID, "ProjectTypes");
            }
            else
            {
                TypeIco = Image_TypeIco.ImageUrl;
            }


            string TypeIcoMax = string.Empty;
            if (!string.IsNullOrEmpty(FileUpload_TypeIcoMax.FileName))
            {
                TypeIcoMax = WebUpLoad.PostActionUpLoad(FileUpload_TypeIcoMax.ID, "ProjectTypes");
            }
            else
            {
                TypeIcoMax=Image_TypeIcoMax.ImageUrl;
            }

            int CreateAccountId = int.Parse(this.HiddenField_CreateAccountId.Value);
            int RefreshAccountId = int.Parse(this.HiddenField_RefreshAccountId.Value);
            DateTime CreateTime = DateTime.Parse(this.HiddenField_CreateTime.Value);
            DateTime RefreshTime = DateTime.Parse(this.HiddenField_RefreshTime.Value);


            XinYiOffice.Model.ProjectTypes model = new XinYiOffice.Model.ProjectTypes();
            model.Id = Id;
            model.TypeName = TypeName;
            model.TypeDescribe = TypeDescribe;
            model.TypeIco = TypeIco;
            model.TypeIcoMax = TypeIcoMax;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ProjectTypes bll = new XinYiOffice.BLL.ProjectTypes();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
