﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.ClientLiaisons
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            //XinYiOffice.BLL.ClientLiaisons bll = new XinYiOffice.BLL.ClientLiaisons();
           //XinYiOffice.Model.ClientLiaisons model = bll.GetModel(Id);
            
            DataSet ds=ClientServer.GetClientLiaisons(string.Format("Id="+Id),CurrentTenantId);
            if(ds!=null&&ds.Tables[0].Rows.Count>0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                this.lblId.Text = dr["Id"].ToString();
                this.lblClientInfoId.Text = SafeConvert.ToString(dr["ClientInfoName"]);
                this.lblPersonNumber.Text =SafeConvert.ToString(dr["PersonNumber"]);
                this.lblFullName.Text = SafeConvert.ToString(dr["FullName"]);
                this.lblPower.Text = SafeConvert.ToString(dr["PowerName"]);
                this.lblPhone.Text =  SafeConvert.ToString(dr["Phone"]);
                this.lblOtherPhone.Text =  SafeConvert.ToString(dr["OtherPhone"]);
                this.lblFax.Text =  SafeConvert.ToString(dr["Fax"]);
                this.lblEmail.Text =  SafeConvert.ToString(dr["Email"]);
                this.lblQQ.Text =  SafeConvert.ToString(dr["QQ"]);
                this.lblBirthday.Text = SafeConvert.ToString(dr["Birthday"]);
                this.lblByClientInfoId.Text = SafeConvert.ToString(dr["ByAccountFullName"]);
                this.lblDepartment.Text = SafeConvert.ToString(dr["Department"]);
                this.lblFUNCTION.Text =  SafeConvert.ToString(dr["FunctionName"]);
                this.lblDirectSuperior.Text = SafeConvert.ToString(dr["DirectSuperior"]);
                this.lblSource.Text = SafeConvert.ToString(dr["SourceName"]);
                this.lblIfContact.Text = SafeConvert.ToString(dr["IfContactName"]);
                this.lblClassiFication.Text = SafeConvert.ToString(dr["ClassiFicationName"]);
                this.lblBusiness.Text =  SafeConvert.ToString(dr["Business"]);
                this.lblPrimaryContact.Text = SafeConvert.ToString(dr["PrimaryContactName"]);
                this.lblShortPhone.Text =  SafeConvert.ToString(dr["ShortPhone"]);
                this.lblSex.Text = SafeConvert.ToString(dr["SexName"]);
                this.lblCountriesRegions.Text =  SafeConvert.ToString(dr["CountriesRegions"]);
                this.lblZip.Text = SafeConvert.ToString(dr["Zip"]);
                this.lblAddress.Text =  SafeConvert.ToString(dr["Address"]);
                this.lblProvince.Text = SafeConvert.ToString(dr["ProvinceName"]);
                this.lblCounty.Text = SafeConvert.ToString(dr["CountyName"]);
                this.lblRemarks.Text =  SafeConvert.ToString(dr["Remarks"]);

            }

           

        }


    }
}
