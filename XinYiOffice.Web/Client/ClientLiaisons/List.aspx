﻿<%@ Page Title="客户联络人" MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Client.ClientLiaisons.List" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" >
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
                <div class="btn clear"><a href="javascript:;" id="search_a"  class="sj">条件查询</a><a href="add.aspx">添加</a><a href="javascript:;" class="edit">修改</a><a href="javascript:;" class="tdel">删除</a></div>
                <div class="del clear"></div>
            </div>

            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">

                    <uc1:SearchApp ID="SearchApp1" runat="server" TableName="vClientLiaisons"/>

                </div>
                <div class="clear btn_box btn_box_p">
<a href="javascript:;" class="save btn_search">查询</a>
<a href="javascript:;" class="cancel res_search">取消</a>
              
                </div>
        	</div>

        </div>


<div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="_table">
            <asp:Repeater ID="repClientLiaisons" runat="server">
                <HeaderTemplate>
                <tr><th><input type="checkbox" id="cb_all"></th><th>联系人姓名</th><th>客户名</th><th>手机</th><th>电子邮件</th><th>职务</th></tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                
                <td><a href="Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#Eval("FullName").ToString().Length > 30 ? Eval("FullName").ToString().Substring(0, 30) + "..." : Eval("FullName")%></a></td>
                                <td><a href="../ClientInfo/Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "ClientInfoId")%>"><%#DataBinder.Eval(Container.DataItem, "ClientInfoName")%></a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "Phone")%></td>
                <td><a href="mailto:<%#DataBinder.Eval(Container.DataItem, "Email")%>"><%#DataBinder.Eval(Container.DataItem, "Email")%></a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "FunctionName")%></td>

                
                </tr>    
                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
        
</div>
<%=pagehtml%>
</form>

<script>
    $('#_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>
