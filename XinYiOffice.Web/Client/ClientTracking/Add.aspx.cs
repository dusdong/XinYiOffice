﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.ClientTracking
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("CLIENTTRACKING_LIST_ADD"))
            {
                NoPermissionPage();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            //string strErr="";
            //if(!PageValidate.IsNumber(txtClientHeat.Text))
            //{
            //    strErr+="当前客户热度格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtSalesOpportunitiesId.Text))
            //{
            //    strErr+="销售机会Id格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtSalesOpportunitiesSate.Text))
            //{
            //    strErr+="销售机会阶段 状态格式错误！\\n";	
            //}
            //if(this.txtBackNotes.Text.Trim().Length==0)
            //{
            //    strErr+="回访备注不能为空！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtSate.Text))
            //{
            //    strErr+="回访状态 未回访格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtIsFollowUp.Text))
            //{
            //    strErr+="是否下步跟进 1-是,0-否格式错误！\\n";	
            //}
            //if(this.txtCloseNotes.Text.Trim().Length==0)
            //{
            //    strErr+="不跟进原因 (关闭原因)不能为空！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtReturnAccountId.Text))
            //{
            //    strErr+="回访人格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtActualTime.Text))
            //{
            //    strErr+="实际回访时间格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtPlanTime.Text))
            //{
            //    strErr+="计划回访时间格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtCreateAccountId.Text))
            //{
            //    strErr+="添加者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtRefreshAccountId.Text))
            //{
            //    strErr+="更新者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtCreateTime.Text))
            //{
            //    strErr+="创建时间格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtRefreshTime.Text))
            //{
            //    strErr+="更新时间 最近一次编辑的人员I格式错误！\\n";	
            //}

            //if(strErr!="")
            //{
            //    MessageBox.Show(this,strErr);
            //    return;
            //}
            int ClientHeat = int.Parse(this.txtClientHeat.Text);
            int SalesOpportunitiesId = int.Parse(this.txtSalesOpportunitiesId.Text);
            int SalesOpportunitiesSate = int.Parse(this.txtSalesOpportunitiesSate.Text);
            string BackNotes = this.txtBackNotes.Text;
            int Sate = int.Parse(this.txtSate.Text);
            int IsFollowUp = int.Parse(this.txtIsFollowUp.Text);
            string CloseNotes = this.txtCloseNotes.Text;
            int ReturnAccountId = int.Parse(this.txtReturnAccountId.Text);
            DateTime ActualTime = DateTime.Parse(this.txtActualTime.Text);
            DateTime PlanTime = DateTime.Parse(this.txtPlanTime.Text);
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Text);
            int RefreshAccountId = int.Parse(this.txtRefreshAccountId.Text);
            DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Text);
            DateTime RefreshTime = DateTime.Parse(this.txtRefreshTime.Text);

            XinYiOffice.Model.ClientTracking model = new XinYiOffice.Model.ClientTracking();
            model.ClientHeat = ClientHeat;
            model.SalesOpportunitiesId = SalesOpportunitiesId;
            model.SalesOpportunitiesSate = SalesOpportunitiesSate;
            model.BackNotes = BackNotes;
            model.Sate = Sate;
            model.IsFollowUp = IsFollowUp;
            model.CloseNotes = CloseNotes;
            model.ReturnAccountId = ReturnAccountId;
            model.ActualTime = ActualTime;
            model.PlanTime = PlanTime;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ClientTracking bll = new XinYiOffice.BLL.ClientTracking();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx");

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
