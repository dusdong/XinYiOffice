﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true"
    CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Client.ClientTracking.Show"
    Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    #<%=RadioButtonList_IsFollowUp.ClientID%>{ border:0px;}
    #<%=RadioButtonList_IsFollowUp.ClientID%> table{ border:0px;}
    #<%=RadioButtonList_IsFollowUp.ClientID%> table td{ border:0px; border-bottom:0px;}
    #<%=RadioButtonList_IsFollowUp.ClientID%> input{ border:0px; height:20px;}
    
    #zjtable{ font-size:12px;}
    #zjtable th{padding:0px; padding-left:5px; margin:0px; height:18px; width:auto; color:#fff;  font-weight:normal; background-color:#0092C1; text-align:left;}
    #zjtable td{ padding:0px; padding-left:5px; height:18px; width:auto; text-align:left;}
    </style>
    <script>

        $(function () {

            var name = '<%=RadioButtonList_IsFollowUp.ClientID%>';
            var radList = $("#<%=RadioButtonList_IsFollowUp.ClientID%>");

            var pan1 = $("#<%=pan.ClientID%>");
            var pan2 = $("#<%=Panel2.ClientID%>");

            $("#<%=RadioButtonList_IsFollowUp.ClientID%> input").click(function () {
                var sel_val = $(this).val();
                if (sel_val == 1) {
                    pan1.show();
                    pan2.hide();
                }
                else if (sel_val == 0) {
                    pan2.show();
                    pan1.hide();
                } else {
                    pan1.hide(); pan2.hide();
                }

            });

        })

        function canhf() {

            if (confirm("您确定将此跟踪单设置为'取消回访'么?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <form id="f2" name="f2" runat="server">
    <div class="setup_box">
        <div class="h_title">
            <h4>
                客户回访单</h4>
        </div>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" class="plan_table">
            <tr>
                <td height="25" width="30%" align="right" class="table_left">
                    客户名称：
                </td>
                <td height="25" width="*" align="left">
                    <asp:Label ID="LabelClientName" runat="server"></asp:Label>
                    <asp:Panel ID="panCLL" runat="server">
                    <table width="100%" border="0" class="zjtable">
                        <tr>
                            <td width="40%">
                                联系人:<asp:Label ID="Label_CLLName" 
                                    runat="server"></asp:Label>
                                <br />
                            </td>
                            <td width="60%">
                                手机号码:<asp:Label ID="Label_Client_phone" 
                                    runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                地址:<asp:Label ID="Label_CLL_address" 
                                    runat="server"></asp:Label>
                            </td>
                            <td>&nbsp;
                                
                            </td>
                        </tr>
                    </table>
                    </asp:Panel>
                </td>
            </tr>

            <tr>
                <td height="25" align="right" class="table_left">
                    客户热度:
                </td>
                <td height="25" align="left">
                    <asp:Label ID="Label_Heat" runat="server"></asp:Label>
                    <asp:HiddenField ID="HiddenField_Id" runat="server" />
                </td>
            </tr>
            <tr>
                <td height="25" align="right" class="table_left">
                    销售机会:
                </td>
                <td height="25" align="left">
                    <asp:Label ID="Label_SalesOpportunitiesName" runat="server"></asp:Label>
                    <asp:HiddenField ID="HiddenField_SalesOpportunitiesId" runat="server" />
                </td>
            </tr>
            <tr>
                <td height="25" align="right" class="table_left">
                    当前机会阶段
                </td>
                <td height="25" align="left">
                    <asp:DropDownList ID="DropDownList_Current_SalesOpportunitiesSate" 
                        runat="server" Width="226px">
                    </asp:DropDownList>
                    (只能通过继续跟进来更新此状态)</td>
            </tr>
            <tr>
                <td height="25" align="right" class="table_left">
                    历史回访记录:
                </td>
                <td height="25" align="left">
                    <table  border="0" id="zjtable">
                        <asp:Repeater ID="repHis" runat="server">
                            <HeaderTemplate>
                                <tr>
                                                                    <th>
                                        回访次序
                                    </th>

                                    <th>
                                        阶段
                                    </th>

                                    <th>
                                        备注
                                    </th>
                                    <th>
                                        回访时间
                                    </th>
                                    <th>&nbsp;
                                        
                                    </th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                                          <td>
                                       <%# Container.ItemIndex + 1%>
                                    </td>

                                    <td>
                                         <%#DataBinder.Eval(Container.DataItem, "SalesOpportunitiesSateName")%>
                                    </td>

                                    <td> 
                                    <a title="<%#DataBinder.Eval(Container.DataItem, "BackNotes")%>" ><%#EscString(DataBinder.Eval(Container.DataItem, "BackNotes").ToString(),50)%></a>
                                    </td>
                                    <td>
                                        <%#DataBinder.Eval(Container.DataItem, "CreateTime","{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td>
                                        详情
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <asp:Label ID="labHisMsg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="25" align="right" class="table_left">
                    回访备注:
                </td>
                <td height="25" align="left">
                    <asp:TextBox ID="TextBox_BackNotes" runat="server" Height="118px" 
                        TextMode="MultiLine" Width="667px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td height="25" align="right" class="table_left">回访单生成时间:
                    
                </td>
                <td height="25" align="left">&nbsp;
                    
                    <asp:Label ID="Label_CreateTime" runat="server"></asp:Label>
                    
                </td>
            </tr>
            <tr>
                <td height="25" align="right" class="table_left">
                    是否继续跟进:
                </td>
                <td height="25" align="left">
                    <asp:RadioButtonList ID="RadioButtonList_IsFollowUp" runat="server" 
                        Width="468px" RepeatDirection="Horizontal">
                        <asp:ListItem Value="-1" Selected="True" style="border: 0px;">暂不处理</asp:ListItem>
                        <asp:ListItem Value="1" style="border: 0px;">继续跟进</asp:ListItem>
                        <asp:ListItem Value="0" style="border: 0px;">不跟进</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Panel ID="pan" runat="server" CssClass="noneinput" Style="display: none;">
                        计划回访时间:
                        <asp:TextBox ID="TextBox_PlanTime" runat="server"  class="Wdate"  onClick="WdatePicker()"></asp:TextBox>(默认值为现在时间往后推3天)
                        <br />
                        客户热诚:
                        <asp:DropDownList ID="DropDownList_ClientHeat" runat="server">
                        </asp:DropDownList>
                        <br />
                        更新销售机会阶段为：<asp:DropDownList ID="DropDownList_SalesOpportunitiesSate" runat="server">
                        </asp:DropDownList>
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server" CssClass="noneinput" Style="display:none;">
                        不跟进原因:<br />
                        <asp:TextBox ID="TextBox_CloseNotes" runat="server" Height="91px" TextMode="MultiLine" Width="611px"></asp:TextBox>
                        <br/>更新销售机会状态为:
                        <asp:DropDownList ID="DropDownList_OpportunitiesSate" runat="server">
                        </asp:DropDownList>
                    </asp:Panel>
                </td>
            </tr>
           
        
        </table>
    </div>
    <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" OnClick="LinkButton1_Click">确定</asp:LinkButton>
        <asp:LinkButton ID="LinkButton2" class="cancel" runat="server" 
            onclick="LinkButton2_Click" OnClientClick="return canhf();"  >取消回访</asp:LinkButton>
   
    </div>
    </form>
</asp:Content>
