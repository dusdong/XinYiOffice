﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.MarketingPlan
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("MARKETINGPLAN_LIST_VIEW"))
            {
                NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.MarketingPlan bll = new XinYiOffice.BLL.MarketingPlan();
            //XinYiOffice.Model.MarketingPlan model = bll.GetModel(Id);
            DataSet ds = ClientServer.GetMarketingPlan(string.Format("Id={0}", Id),CurrentTenantId);
            DataRowCollection drc = ds.Tables[0].Rows;

            this.lblId.Text = SafeConvert.ToString(drc[0]["Id"]);
            this.lblProgramName.Text = SafeConvert.ToString(drc[0]["ProgramName"]);
            this.lblProgramDescription.Text = SafeConvert.ToString(drc[0]["ProgramDescription"]);
            this.lblSate.Text = SafeConvert.ToString(drc[0]["SateName"]);  
            this.lblStartTime.Text = SafeConvert.ToString(drc[0]["StartTime"]);
            this.lblEndTime.Text = SafeConvert.ToString(drc[0]["EndTime"]);
            this.lblTYPE.Text = SafeConvert.ToString(drc[0]["TypeName"]); 
            this.lblAnticipatedRevenue.Text =SafeConvert.ToString(drc[0]["AnticipatedRevenue"]); 
            this.lblBudgetCost.Text = SafeConvert.ToString(drc[0]["BudgetCost"]);
            this.lblSchemerAccountId.Text = SafeConvert.ToString(drc[0]["SchemerName"]);
            this.lblExecuteAccountId.Text = SafeConvert.ToString(drc[0]["ExecuteName"]);
            //this.lblCreateAccountId.Text = model.CreateAccountId.ToString();
            //this.lblRefreshAccountId.Text = model.RefreshAccountId.ToString();
            this.lblCreateTime.Text =  SafeConvert.ToString(drc[0]["CreateTime"]);
            this.lblRefreshTime.Text = SafeConvert.ToString(drc[0]["RefreshTime"]); 

        }


    }
}
