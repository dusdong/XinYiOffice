﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.ClientInfoExpand
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            //string strErr = "";
            //if (this.txtFieldName.Text.Trim().Length == 0)
            //{
            //    strErr += "扩展字段名不能为空！\\n";
            //}
            //if (!PageValidate.IsNumber(txtFormType.Text))
            //{
            //    strErr += "扩展的表单类型 1-文本格式错误！\\n";
            //}
            //if (this.txtInitData.Text.Trim().Length == 0)
            //{
            //    strErr += "表单初始化数据 json不能为空！\\n";
            //}
            //if (!PageValidate.IsNumber(txtCreateAccountId.Text))
            //{
            //    strErr += "添加者id格式错误！\\n";
            //}
            //if (!PageValidate.IsNumber(txtRefreshAccountId.Text))
            //{
            //    strErr += "更新者id格式错误！\\n";
            //}
            //if (!PageValidate.IsDateTime(txtCreateTime.Text))
            //{
            //    strErr += "创建时间格式错误！\\n";
            //}
            //if (!PageValidate.IsDateTime(txtRefreshTime.Text))
            //{
            //    strErr += "更新时间 最近一次编辑的人员I格式错误！\\n";
            //}

            //if (strErr != "")
            //{
            //    MessageBox.Show(this, strErr);
            //    return;
            //}
            string FieldName = this.txtFieldName.Text;
            int FormType = int.Parse(this.txtFormType.Text);
            string InitData = this.txtInitData.Text;
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Text);
            int RefreshAccountId = int.Parse(this.txtRefreshAccountId.Text);
            DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Text);
            DateTime RefreshTime = DateTime.Parse(this.txtRefreshTime.Text);

            XinYiOffice.Model.ClientInfoExpand model = new XinYiOffice.Model.ClientInfoExpand();
            model.FieldName = FieldName;
            model.FormType = FormType;
            model.InitData = InitData;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ClientInfoExpand bll = new XinYiOffice.BLL.ClientInfoExpand();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx");
            
        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
