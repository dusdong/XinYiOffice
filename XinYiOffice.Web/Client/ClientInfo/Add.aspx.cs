﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Client.ClientInfo
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("CLIENTINFO_LIST_ADD"))
            {
                NoPermissionPage();
            }

            if (!IsPostBack)
            {
                InitData();
            }

        }

        protected void InitData()
        {
            txtIntNumber.Text = "CL" + DateTime.Now.ToString("yyyyMMddmmss");
            txtByAccountId.Text = CurrentAccountTrueName;

            hidSupClientInfoId.Value = "0";

            SetDropDownList("Heat", ref DropDownList_txtHeat);
            SetDropDownList("ClientType", ref DropDownList_txtClientType);
            SetDropDownList("Industry", ref DropDownList1_txtIndustry);
            SetDropDownList("Source", ref DropDownList_txtSource);
            SetDropDownList("Power", ref DropDownList_Power);
            SetDropDownList("QualityRating", ref DropDownList_txtQualityRating);
            SetDropDownList("ValueAssessment ", ref DropDownList_txtValueAssessment);
            SetDropDownList("CustomerLevel", ref DropDownList_txtCustomerLevel);
            SetDropDownList("RelBetweenGrade", ref DropDownList_txtRelBetweenGrade);
            SetDropDownList("Seedtime", ref DropDownList_txtSeedtime);
            SetDropDownList("CountriesRegions", ref DropDownListCountriesRegions);
            SetDropDownList_ClientLiaisons("Sex", ref ddl_ClientLiaisons_Sex);
            SetDropDownList_ClientLiaisons("ClassiFication", ref DropDownList_ClassiFication);
            

            DropDownList_Province.DataSource = AppDataCacheServer.Province;
            DropDownList_Province.DataTextField = "Name";
            DropDownList_Province.DataValueField = "Id";
            DropDownList_Province.DataBind();
            DropDownList_Province.Items.FindByText("北京").Selected = true;

            DropDownList_City.DataSource = AppDataCacheServer.GetCityByPid(SafeConvert.ToInt(DropDownList_Province.SelectedValue));
            DropDownList_City.DataTextField = "Name";
            DropDownList_City.DataValueField = "Id";
            DropDownList_City.DataBind();
            HiddenField_City.Value=DropDownList_City.SelectedValue;

            int MarketingPlanId = SafeConvert.ToInt(new BLL.MarketingPlan().GetMaxId(), 0);
            if (MarketingPlanId != 0)
            {
                MarketingPlanId = MarketingPlanId - 1;
                Model.MarketingPlan mp = new BLL.MarketingPlan().GetModel(MarketingPlanId);

                if (mp != null)
                {
                    TextBox_MarketingPlanId.Text = mp.ProgramName;//默认绑定最近的营销计划
                    hid_MarketingPlanId.Value = mp.Id.ToString();
                }
            }

        }

        protected void SetDropDownList_ClientLiaisons(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ClientLiaisonsDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ClientInfoDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int ByAccountId = SafeConvert.ToInt(CurrentAccountId);
            string Name = this.txtName.Text;
            
            int Heat = SafeConvert.ToInt(DropDownList_txtHeat.SelectedValue);
            int ClientType = SafeConvert.ToInt(DropDownList_txtClientType.SelectedValue);
            int Industry = SafeConvert.ToInt(DropDownList1_txtIndustry.SelectedValue);
            int Source = SafeConvert.ToInt(DropDownList_txtSource.SelectedValue);
            
            string Tag = this.txtTag.Text;
            string BankDeposit = this.txtBankDeposit.Text;
            string TaxNo = this.txtTaxNo.Text;
            string IntNumber = this.txtIntNumber.Text;

            int Power = SafeConvert.ToInt(DropDownList_Power.SelectedValue);
            int QualityRating = SafeConvert.ToInt(DropDownList_txtQualityRating.SelectedValue);
            int ValueAssessment = SafeConvert.ToInt(DropDownList_txtValueAssessment.SelectedValue);
            int CustomerLevel = SafeConvert.ToInt(DropDownList_txtCustomerLevel.SelectedValue);
            int RelBetweenGrade = SafeConvert.ToInt(DropDownList_txtRelBetweenGrade.SelectedValue);
            int Seedtime = SafeConvert.ToInt(DropDownList_txtSeedtime.SelectedValue);
            //int SupClientInfoId = SafeConvert.ToInt(DropDownList_txtSupClientInfoId.SelectedValue);

            int SupClientInfoId = SafeConvert.ToInt(hidSupClientInfoId.Value);

            string BankAccount = this.txtBankAccount.Text;
            string MobilePhone = this.txtMobilePhone.Text;
            int CountriesRegions = SafeConvert.ToInt(DropDownListCountriesRegions.SelectedValue);
            string Phone = this.txtPhone.Text;
            string Fax = this.txtFax.Text;
            string Email = this.txtEmail.Text;
            string ZipCode = this.txtZipCode.Text;
            int Province = SafeConvert.ToInt(DropDownList_Province.SelectedValue);
            int City = SafeConvert.ToInt(HiddenField_City.Value);
            string County = this.txtCounty.Text;
            string Address = this.txtAddress.Text;
            string CompanyProfile = this.txtCompanyProfile.Text;
            string Remarks = this.txtRemarks.Text;
            string GeneralManager = this.txtGeneralManager.Text;
            string BusinessEntity = this.txtBusinessEntity.Text;
            string BusinessLicence = this.txtBusinessLicence.Text;
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.ClientInfo model = new XinYiOffice.Model.ClientInfo();
            model.ByAccountId = ByAccountId;
            model.Name = Name;
            model.Heat = Heat;
            model.ClientType = ClientType;
            model.Industry = Industry;
            model.Source = Source;
            model.MarketingPlanId = SafeConvert.ToInt(hid_MarketingPlanId.Value);
            model.Tag = Tag;
            model.BankDeposit = BankDeposit;
            model.TaxNo = TaxNo;
            model.IntNumber = IntNumber;
            model.Power = Power;
            model.QualityRating = QualityRating;
            model.ValueAssessment = ValueAssessment;
            model.CustomerLevel = CustomerLevel;
            model.RelBetweenGrade = RelBetweenGrade;
            model.Seedtime = Seedtime;
            model.SupClientInfoId = SupClientInfoId;
            model.BankAccount = BankAccount;
            model.MobilePhone = MobilePhone;
            model.CountriesRegions = CountriesRegions;
            model.Phone = Phone;
            model.Fax = Fax;
            model.Email = Email;
            model.ZipCode = ZipCode;
            model.Province = Province;
            model.City = City;
            model.County = County;
            model.Address = Address;
            model.CompanyProfile = CompanyProfile;
            model.Remarks = Remarks;
            model.GeneralManager = GeneralManager;
            model.BusinessEntity = BusinessEntity;
            model.BusinessLicence = BusinessLicence;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            int ClientId, SalesOpportunitiesId, ClientTrackingId;

            XinYiOffice.BLL.ClientInfo bll = new XinYiOffice.BLL.ClientInfo();
            ClientId = bll.Add(model);

            //创建销售机会,只要是热度不是冷淡
            if (SafeConvert.ToInt(model.Heat) < 3)
            {
                #region 创建销售机会
                Model.SalesOpportunities so = new Model.SalesOpportunities();
                so.CreateAccountId = CurrentAccountId;
                so.PersonalAccountId = CurrentAccountId;
                so.CreateTime = DateTime.Now;
                so.OccTime = so.CreateTime;
                so.CurrencyNotes = string.Empty;
                so.CustomerService = ClientId;
                so.EstAmount = SafeConvert.ToDecimal("0.00");
                so.MarketingPlanId = model.MarketingPlanId;//营销计划
                so.NextStep = 0;
                so.Sate = 1;
                so.Source = model.Source;
                //so.StageRemarks = string.Format("创建客户({0})时同步写入空备注", model.Name);
                so.StageRemarks = model.Remarks;
                so.TenantId = CurrentTenantId;

                so.OppName = string.Format("JH-{0}({1}-{2})", model.Name, DateTime.Now.Year, DateTime.Now.Month);
                SalesOpportunitiesId = new BLL.SalesOpportunities().Add(so);
                #endregion

                #region //依据销售机会,创建客户回访
                Model.ClientTracking ct = new Model.ClientTracking();
                //ct.BackNotes = string.Format("创建客户({0})时同步写入空备注", model.Name);
                ct.BackNotes = model.Remarks;

                ct.ClientHeat = model.Heat;
                ct.CreateAccountId = CurrentAccountId;
                ct.CreateTime = DateTime.Now;
                ct.IsFollowUp = 0;
                ct.PlanTime = DateTime.Now.AddDays(3);//计划回访时间
                ct.ReturnAccountId = CurrentAccountId;
                ct.SalesOpportunitiesId = SalesOpportunitiesId;
                ct.SalesOpportunitiesSate = so.Sate;
                ct.Sate = 1;
                ct.TenantId = CurrentTenantId;

                ClientTrackingId = new BLL.ClientTracking().Add(ct);
                #endregion
            }

            //创建联系人,若联系人不为空时
            if (!string.IsNullOrEmpty(TextBox_ClientLiaisons_Name.Text))
            {
                #region 创建联系人
                Model.ClientLiaisons cl = new Model.ClientLiaisons();
                cl.ClientInfoId = ClientId;
                cl.PersonNumber = "CLL" + DateTime.Now.ToString("yyyyMMddmmss");
                cl.FullName = TextBox_ClientLiaisons_Name.Text;
                cl.QQ = txt_QQ.Text;
                cl.Email = txt_ClientLiaisons_Email.Text;
                cl.Sex = SafeConvert.ToInt(ddl_ClientLiaisons_Sex.SelectedValue);
                cl.OtherPhone = txt_OtherPhone.Text;
                cl.Source = model.Source;
                cl.ClassiFication = SafeConvert.ToInt(DropDownList_ClassiFication.SelectedValue);
                cl.Phone = txt_ClientLiaisons_phone.Text;
                cl.OtherPhone = txt_OtherPhone.Text;
                cl.ByAccountId = CurrentAccountId;
                cl.CreateAccountId = CurrentAccountId;
                cl.CreateTime = DateTime.Now;
                cl.TenantId = CurrentTenantId;

                new BLL.ClientLiaisons().Add(cl);
                #endregion
            }

            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
