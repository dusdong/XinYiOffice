﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.ClientInfo
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        public string strclientid = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("CLIENTINFO_LIST_VIEW"))
            {
                NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    strclientid = strid;

                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                    ShowOppInfo(Id);
                    ShowClientLiaisons(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            DataSet ds=ClientServer.GetClientInfoList(string.Format("Id={0}",Id));
            if(ds!=null&&ds.Tables[0].Rows.Count>0)
            {
                DataRow _dr = ds.Tables[0].Rows[0];


                Litera_ByAccountId.Text = SafeConvert.ToString(_dr["FullName"]);
                Literal_IntNumber.Text = SafeConvert.ToString(_dr["IntNumber"]);
                Literal_Name.Text = SafeConvert.ToString(_dr["Name"]);

                Literal_Power.Text = SafeConvert.ToString(_dr["PowerName"]);


                Literal_Heat.Text = SafeConvert.ToString(_dr["HeatName"]);

                Literal_QualityRating.Text = SafeConvert.ToString(_dr["QualityRatingName"]);

                Literal_ClientType.Text = SafeConvert.ToString(_dr["ClientTypeName"]);


                Literal_ValueAssessment.Text = SafeConvert.ToString(_dr["ValueAssessmentName"]);

                Literal_Industry.Text = SafeConvert.ToString(_dr["IndustryName"]);

                Literal_RelBetweenGrade.Text = SafeConvert.ToString(_dr["RelBetweenGradeName"]);

                Literal_Source.Text = SafeConvert.ToString(_dr["SourceName"]);

                Literal_CustomerLevel.Text = SafeConvert.ToString(_dr["CustomerLevelName"]);


                Literal_Tag.Text = SafeConvert.ToString(_dr["Tag"]);


                Literal_Seedtime.Text = SafeConvert.ToString(_dr["SeedtimeName"]);


                Literal_BankDeposit.Text = SafeConvert.ToString(_dr["BankDeposit"]);

                Literal_SupClientInfoId.Text = SafeConvert.ToString(_dr["SupClientInfoName"]);

                Literal_TaxNo.Text = SafeConvert.ToString(_dr["TaxNo"]);

                Literal_MarketingPlanId.Text = SafeConvert.ToString(_dr["ProgramName"]);

                Literal_MobilePhone.Text = SafeConvert.ToString(_dr["MobilePhone"]);

                Literal_BankAccount.Text = SafeConvert.ToString(_dr["BankAccount"]);
                Literal_Province.Text = SafeConvert.ToString(_dr["ProvinceName"]);

                Literal_CountriesRegions.Text = SafeConvert.ToString(_dr["CountriesRegionsName"]);

                Literal_City.Text = SafeConvert.ToString(_dr["CityName"]);

                Literal_Phone.Text = SafeConvert.ToString(_dr["Phone"]);
                Literal_County.Text = SafeConvert.ToString(_dr["County"]);
                Literal_Fax.Text = SafeConvert.ToString(_dr["Fax"]);
                Literal_Address.Text = SafeConvert.ToString(_dr["Address"]);
                Literal_Email.Text = SafeConvert.ToString(_dr["Email"]);
                Literal_ZipCode.Text = SafeConvert.ToString(_dr["ZipCode"]);
                Literal_CompanyProfile.Text = SafeConvert.ToString(_dr["CompanyProfile"]);

                Literal_Remarks.Text = SafeConvert.ToString(_dr["Remarks"]);

                Literal_GeneralManager.Text = SafeConvert.ToString(_dr["GeneralManager"]);


                Literal_BusinessEntity.Text = SafeConvert.ToString(_dr["BusinessEntity"]);

                Literal_BusinessLicence.Text = SafeConvert.ToString(_dr["BusinessLicence"]);
                
                btnPhone.Attributes.Add("data-cid", SafeConvert.ToString(_dr["Id"]));

            }

        }

        private void ShowOppInfo(int Id)
        {
            repOpp.DataSource = new BLL.SalesOpportunities().GetList(string.Format("CustomerService={0} and TenantId={0}",Id,CurrentTenantId));
            repOpp.DataBind();
        }

        private void ShowClientLiaisons(int Id)
        {
            repClientLiaisons.DataSource = ClientServer.GetClientLiaisons(string.Format("ClientInfoId={0}", Id),CurrentTenantId);
            repClientLiaisons.DataBind();
        }
    }
}
