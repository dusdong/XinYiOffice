﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Web.UI;
using System.Text;
using XinYiOffice.Common;

namespace XinYiOffice.Web.WebExplorer
{
    public partial class GetTenant : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentTenant != null)
            {
                StringBuilder jsString = new StringBuilder();
                jsString.Append("var nd = new ZxjayTreeNode();");
                jsString.Append("nd.container = $(\"tree\");");
                jsString.Append("nd.text = \"企业网盘\";");
                jsString.Append("nd.path = \"~/xyo_upload/{0}/\";");
                jsString.Append("nd.Show();");
                jsString.Append("currentNode = nd;");
                jsString.Append("currentNode.Refersh();");

                xytools.write(string.Format(jsString.ToString(), CurrentTenant.TenantGuid));
            }
        }
    }
}