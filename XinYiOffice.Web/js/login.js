﻿$(function () {

    $("#btn").click(function () {
        var msg = $("#span_msg");
        msg.html("正在登录...");
        var data1 = "username=" + $("#txt_username").val() + "&pas=" + $("#txt_pas").val() + "&online=" + $("#online").val() + "&TenantId=" + $("#TenantId").val();

        $.ajax({
            type: "GET",
            url: "/call/ajax.aspx?action=login&" + data1 + "&sj=" + Math.random(),
            data: {},
            timeout: 6000,
            error: function (info) {
                msg.html("登录超时,请重新登陆!");
            },
            success: function (d) {
                // 回调函数
                if (d != '') {
                    if (d == '-1') {
                        msg.html("用户名或密码错误!");
                    }
                    else if (d == '2') {
                        msg.html("用户未激活!");
                    }
                    else if (d == '3') {
                        msg.html("用户异常,请联系管理员!");
                    }
                    else {
                        window.location = "/index.aspx?sj=" + Math.random();
                    }
                }
                else {
                    msg.html("登录异常,请稍后再试!");
                }

            }
        });



    });


    $("#kanbuq").click(function () {

        var vimg = $("#vimg");
        vimg.attr("src", "/call/VerifyCode.aspx?rd=" + Math.random());
    })

})