﻿<%@ Page Title="内部信" MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Internal.InternalLetter.List" %>

<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2"  >
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            <%if (ValidatePermission("INTERNALLETTER_ZHANNEI_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("INTERNALLETTER_ZHANNEI_LIST_ADD"))
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("INTERNALLETTER_ZHANNEI_LIST_EDIT"))
              { %>
           <%-- <a href="javascript:;" class="edit">修改</a>--%>
            <%} %>
             <%if (ValidatePermission("INTERNALLETTER_ZHANNEI_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>

            </div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">
                <uc1:searchapp ID="SearchApp1" runat="server" TableName="vInternalReceiver"/>

                </div>
                <div class="clear btn_box btn_box_p">
<a href="javascript:;" class="save btn_search">查询</a>
<a href="javascript:;" class="cancel res_search">取消</a>
                </div>
        	</div>
    
        </div>
        
   
   <div class="marketing_table">
        <table cellpadding="0" cellspacing="0" id="_table">

<asp:Repeater ID="repInternalLetter" runat="server" >
                <HeaderTemplate>
                <tr>
                <th width="30" class="center"><input type="checkbox" id="cb_all"></th>
                <th width="60">编号</th>
                <th width="60"><img  src="/images/youxianji.png" border="0" title="重要性" /></th>
                <th width="70">发件人</th>
                <th width="">标题</th>
                <th width="180">时间</th>
                

                </tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td  class="center"><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                 <td><%#DataBinder.Eval(Container.DataItem, "Id")%></td>
                <td><img  src="/images/youxianji_<%#DataBinder.Eval(Container.DataItem, "ImpDegree")%>.png" border="0" /></td>
                
                <td><%#DataBinder.Eval(Container.DataItem, "FullName")%></td>
                <td><a href="show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "Title")%></a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CreateTime")%></td>
                </tr>

                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
        

</div>

<%=pagehtml%>

<script>
    $('#_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</form>
</asp:Content>

