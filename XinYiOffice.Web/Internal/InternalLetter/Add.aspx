﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" validateRequest="false"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Internal.InternalLetter.Add" Title="增加页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
  <script type="text/javascript" charset="utf-8">
      window.UEDITOR_HOME_URL = "/js/ueditor/";
    </script>
    <script type="text/javascript" charset="utf-8" src="/js/ueditor/editor_config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/js/ueditor/editor_all_min.js"></script>


<link rel="stylesheet" href="/js/ueditor/themes/default/css/ueditor.css">  

  <script src="/js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/js/chosen/chosen.css" />

    <script>
        $(function () {

            window.setTimeout(function () {
                $(".chzn-select").chosen({ width: "100%" });
            }, 100);

            var HiddenField_InternalReceiver = $("#<%=HiddenField_InternalReceiver.ClientID%>");

            $(".chzn-select").change(function () {
                HiddenField_InternalReceiver.val($(this).val());
            });
        });
    </script>
    <style>
    .int_table tr td{ width:50px;}
	.input_int{
	    -moz-box-sizing: border-box;
    background-color: #FFFFFF;
    background-image: linear-gradient(#EEEEEE 1%, #FFFFFF 15%);
    border: 1px solid #AAAAAA;
    cursor: text;
height: 27px;
	 line-height:27px;
    margin: 0px;
    overflow: hidden;
	padding:0px;
   /*  padding-left:5px;*/
   text-indent:5px;
    width: 100%;	
	
		}
		
		.int_table tr td textarea{ text-indent:5px;}
    </style>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form  id="f2" name="f2" runat="server" enctype="multipart/form-data">
 <div class="setup_box" style="margin-bottom: 0">
 <div class="h_title"><h4>撰写新信息</h4></div>
 
<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="int_table">
	<tr>
	<td width="10%" align="right" class="">
		收件人：</td>
	<td align="left">
		
		
	    <asp:DropDownList ID="DropDownList_Account" runat="server"  
            class="chzn-select"  multiple="multiple" data-placeholder="点击选择收信人..." style="width:98%;" tabindex="4">
        </asp:DropDownList>
     
		
	    <asp:HiddenField ID="HiddenField_InternalReceiver" runat="server" />
     
		
	    <asp:HiddenField ID="HiddenField_InternalLetterId" runat="server" />
	</td></tr>
	<tr>
	<td align="right" class="">
		标题
	：</td>
	<td align="left">
		<asp:TextBox id="txtTitle" runat="server" CssClass="input_int"></asp:TextBox>
	</td></tr>
	<tr>
	<td colspan="2"  class="">
        
        <asp:TextBox ID="txtCon" runat="server" TextMode="MultiLine" ></asp:TextBox>
<script type="text/javascript">
    var editor = new UE.ui.Editor({
        autoClearinitialContent: true,
        textarea: '<%=txtCon.ClientID%>',
    toolbars:[
            ['fullscreen', 'source', '|', 'undo', 'redo', '|',
                'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch','autotypeset','blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist','selectall', 'cleardoc', '|',
                'rowspacingtop', 'rowspacingbottom','lineheight','|',
                'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                'directionalityltr', 'directionalityrtl', 'indent', '|',
                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|','touppercase','tolowercase','|',
                'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright','imagecenter', '|',
                'insertimage', 'emotion','scrawl', 'insertvideo','music','attachment', 'map', 'gmap', 'insertframe','highlightcode','webapp','pagebreak','template','background', '|',
                'horizontal', 'date', 'time', 'spechars','snapscreen', 'wordimage', '|',
                'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', '|',
                'print', 'preview', 'searchreplace']
        ]
    });

editor.render('<%=txtCon.ClientID%>');


function SetCon() {

    $("#<%=txtCon.ClientID%>").val(editor.getContent());
    return true;
}
</script>

            </td>
	</tr>
	<tr>
	<td   align="right" class="">
		重要程度：</td>
	<td align="left">
		<asp:RadioButtonList ID="RadioButtonList1" runat="server" 
            RepeatDirection="Horizontal" RepeatLayout="Flow" Height="40px" 
            RepeatColumns="5" Width="596px">
            <asp:ListItem Value="1" Selected="True">普通<img  src="/images/youxianji_1.png" border="0" /> </asp:ListItem>
            <asp:ListItem Value="2">高<img  src="/images/youxianji_2.png" border="0" /> </asp:ListItem>
            <asp:ListItem Value="3">重要<img  src="/images/youxianji_3.png" border="0" /> </asp:ListItem>
            <asp:ListItem Value="3">非常重要<img  src="/images/youxianji_4.png" border="0" /> </asp:ListItem>
        </asp:RadioButtonList>
	</td></tr>
	<tr>
	<td width="10%" align="right" class="">
		附件：</td>
	<td align="left">
		  <a href="javascript:addimg()" >增加</a>
                <div id="mdiv" style="display:none;"></div>
                </td></tr>
	</table>


        <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" OnClick="LinkButton1_Click" OnClientClick="return SetCon();">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>

</div>

  <script type="text/javascript" >
      $(document).ready(function () {
          bindListener();
      });
      // 用来绑定事件(使用unbind避免重复绑定)
      function bindListener() {
          $("a[name=rmlink]").unbind().click(function () {
              $(this).parent().remove();
              $("#mdiv").hide();
          })
      }
      function addimg() {
          $("#mdiv").append('<div class="iptdiv"><input type="file" name="file_res" class="wj" /><a href="javascript:void(0);" name="rmlink">[X]</a></div>');
          $("#mdiv").show();

          // 为新元素节点添加事件侦听器

          bindListener();
      } 
</script>
  </form>



</asp:Content>

