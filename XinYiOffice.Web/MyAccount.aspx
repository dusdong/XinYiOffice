﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="MyAccount.aspx.cs" Inherits="XinYiOffice.Web.MyAccount" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
 <script>
     $(function () {

         var DropDownList_HeadPortrait = $("#<%=DropDownList_HeadPortrait.ClientID%>");
         var Image1 = $("#<%=Image1.ClientID%>");

         DropDownList_HeadPortrait.change(function () {
             var _va = DropDownList_HeadPortrait.val();
             Image1.attr("src", "/img/face/" + _va + ".png");
         });

     });


 </script>

</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">
    <div class="setup_box">
    	<div class="h_title"><h4>我的帐号</h4></div>
        
        <table cellpadding="0" cellspacing="0">
        	<tr><td class="table_left">用户名：</td><td colspan="3" style="padding-left:10px;"><asp:HiddenField  ID="hidAccountId" runat="server"/><asp:Literal ID="litAccountName" runat="server"></asp:Literal></td></tr>
            <tr><td class="table_left">昵称：</td><td colspan="3"><asp:TextBox id="txtNiceName" runat="server" Width="200px"></asp:TextBox></td></tr>
            <tr><td class="table_left">姓名：</td><td><asp:TextBox id="txtFull" runat="server" Width="200px"></asp:TextBox>（<span class="red">*</span>必填）</td>
          <td class="table_left"> 英文名:</td><td><asp:TextBox id="txtEnName" runat="server" Width="200px"></asp:TextBox></td></tr>
            <tr><td class="table_left">时区：</td><td colspan="3"><asp:TextBox id="txtTimeZone" runat="server" Width="200px"></asp:TextBox> </td></tr>
            <tr><td class="table_left">设置头像：</td><td colspan="3">
		<asp:DropDownList ID="DropDownList_HeadPortrait" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
            <asp:ListItem>7</asp:ListItem>
            <asp:ListItem>8</asp:ListItem>
            <asp:ListItem>9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
        </asp:DropDownList>
        <asp:Image ID="Image1" runat="server" ImageUrl="/img/face/1.png" />
	            </td></tr>
            <tr><td class="table_left">Email：</td><td colspan="3"><asp:TextBox id="txtEmail" runat="server" Width="200px"></asp:TextBox></td></tr>
            <tr><td class="table_left">地址：</td><td><input type="text" value="" /></td><td class="table_left">地址2：</td><td><input type="text" value="" /><font color="#fff">（*必填）</font></td></tr>
            <tr><td class="table_left">电话：</td><td><asp:TextBox id="txtPhone" runat="server" Width="200px"></asp:TextBox></td><td class="table_left">传真：</td><td><input type="text" value="" /></td></tr>
            <tr>
          <td class="table_left">帐号类型：</td><td colspan="3" style="padding-left:10px;"><asp:Literal ID="litAccountType" runat="server"></asp:Literal></td></tr>
        </table>
    </div>
    
    <div class="clear btn_box">
    <asp:LinkButton ID="alinkSave" CssClass="save" runat="server" onclick="alinkSave_Click">确认更改</asp:LinkButton>
    <asp:LinkButton ID="alinkCancel" runat="server" CssClass="cancel" Type="reset">重置</asp:LinkButton>
    </div>

        </form>
</asp:Content>

