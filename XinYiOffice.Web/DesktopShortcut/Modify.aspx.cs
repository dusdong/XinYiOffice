﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.DesktopShortcut
{
    public partial class Modify : BasicPage
    {       

        		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					int Id=(Convert.ToInt32(Request.Params["id"]));
					ShowInfo(Id);
				}
			}
		}
			
	private void ShowInfo(int Id)
	{
		XinYiOffice.BLL.DesktopShortcut bll=new XinYiOffice.BLL.DesktopShortcut();
		XinYiOffice.Model.DesktopShortcut model=bll.GetModel(Id);
		this.lblId.Text=model.Id.ToString();
		this.txtName.Text=model.Name;
		this.txtSort.Text=model.Sort.ToString();
		this.txtMenuTreeId.Text=model.MenuTreeId.ToString();
		this.txtCreateAccountId.Text=model.CreateAccountId.ToString();
		this.txtRefreshAccountId.Text=model.RefreshAccountId.ToString();
		this.txtCreateTime.Text=model.CreateTime.ToString();
		this.txtRefreshTime.Text=model.RefreshTime.ToString();

	}

		public void btnSave_Click(object sender, EventArgs e)
		{
			
			string strErr="";
            //if(this.txtName.Text.Trim().Length==0)
            //{
            //    strErr+="快捷方式名称不能为空！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtSort.Text))
            //{
            //    strErr+="排序 1-10排序格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtMenuTreeId.Text))
            //{
            //    strErr+="菜单ID格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtCreateAccountId.Text))
            //{
            //    strErr+="添加者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtRefreshAccountId.Text))
            //{
            //    strErr+="更新者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtCreateTime.Text))
            //{
            //    strErr+="创建时间格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtRefreshTime.Text))
            //{
            //    strErr+="更新时间 最近一次编辑的人员I格式错误！\\n";	
            //}

            //if(strErr!="")
            //{
            //    MessageBox.Show(this,strErr);
            //    return;
            //}
			int Id=int.Parse(this.lblId.Text);
			string Name=this.txtName.Text;
			int Sort=int.Parse(this.txtSort.Text);
			int MenuTreeId=int.Parse(this.txtMenuTreeId.Text);
			int CreateAccountId=int.Parse(this.txtCreateAccountId.Text);
			int RefreshAccountId=int.Parse(this.txtRefreshAccountId.Text);
			DateTime CreateTime=DateTime.Parse(this.txtCreateTime.Text);
			DateTime RefreshTime=DateTime.Parse(this.txtRefreshTime.Text);


			XinYiOffice.Model.DesktopShortcut model=new XinYiOffice.Model.DesktopShortcut();
			model.Id=Id;
			model.Name=Name;
			model.Sort=Sort;
			model.MenuTreeId=MenuTreeId;
			model.CreateAccountId=CreateAccountId;
			model.RefreshAccountId=RefreshAccountId;
			model.CreateTime=CreateTime;
			model.RefreshTime=RefreshTime;
            model.TenantId = CurrentTenantId;
			XinYiOffice.BLL.DesktopShortcut bll=new XinYiOffice.BLL.DesktopShortcut();
			bll.Update(model);
			xytools.web_alert("保存成功！","list.aspx");

		}


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
