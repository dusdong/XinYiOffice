﻿<%@ Page Title="桌面快捷方式-组件" MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.DesktopShortcut.List" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" >
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            <a href="javascript:;" class="tdel">删除</a>
            <a href="javascript:history.go(-1);" class="">返回</a>
            </div>
                <div class="del clear"></div>
            </div>
          

    
        </div>
        
   
   <div class="marketing_table">
        <table cellpadding="0" cellspacing="0" id="_table">

<asp:Repeater ID="repDesktopShortcut" runat="server" >
                <HeaderTemplate>
                <tr><th class="center"><input type="checkbox" id="cb_all"></th>
                <th>快捷方式名称</th>
                <th>排序</th>
                <th>菜单ID</th>
                <th>URL</th>
                <th>创建时间</th></tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td class="center"><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                 <td><%#DataBinder.Eval(Container.DataItem, "MenuTreeName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "Sort")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "MenuTreeId")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ChainedAddress")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CreateTime")%></td>

                </tr>    
                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
        

</div>
</form>
<script>
    $('#_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>