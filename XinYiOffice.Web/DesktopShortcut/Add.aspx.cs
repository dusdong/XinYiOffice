﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.DesktopShortcut
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = xytools.url_get("url");
            if(!string.IsNullOrEmpty(url))
            {
                InsertDesktopShortcut(url);
            }

        }

        /// <summary>
        /// 添加快捷方式,根据url
        /// </summary>
        protected void InsertDesktopShortcut(string url)
        {
            //1-添加成功  2-地址已存在快捷方式中 3-添加的地址不存在系统 4-服务器忙,请稍候再试
            try
            {
                DataRow[] dr=AppDataCacheServer.MenuTreeList().Select(string.Format("ChainedAddress='{0}'",url));
                if (dr.Length >= 1)
                {
                    int mid = SafeConvert.ToInt(dr[0]["Id"], 0);
                    string name = SafeConvert.ToString(dr[0]["Name"], "暂无标题");

                    if (mid != 0)
                    {
                        DataSet ds = new BLL.DesktopShortcut().GetList(string.Format("CreateAccountId={0} and MenuTreeId={1} and TenantId={2} ", CurrentAccountId, mid,CurrentTenantId));

                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            //已存在无需添加
                            xytools.write("2");
                        }
                        else
                        {
                            //新添加
                            try
                            {
                                Model.DesktopShortcut desktop = new Model.DesktopShortcut();
                                desktop.CreateAccountId = CurrentAccountId;
                                desktop.CreateTime = DateTime.Now;
                                desktop.MenuTreeId = mid;
                                desktop.Name = name;
                                desktop.Sort = 1;

                                new BLL.DesktopShortcut().Add(desktop);
                                xytools.write("1");
                            }
                            catch(Exception ex)
                            {
                                EasyLog.WriteLog(ex);
                                xytools.write("4");
                            }
                            finally { }

                        }
                    }
                    else
                    {
                        //添加的地址不存在库中
                        xytools.write("3");
                    }

                }
                else
                {
                    //添加的地址不存在库中
                    xytools.write("3");
                }

            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.write("4");
            }
            finally
            {
 
            }
            

        }

    }
}
