﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Discuss
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("DISCUSS_LIST_EDIT"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.Discuss bll = new XinYiOffice.BLL.Discuss();
            XinYiOffice.Model.Discuss model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtProjectInfoId.Text = model.ProjectInfoId.ToString();
            this.txtDiscussClassId.Text = model.DiscussClassId.ToString();
            this.txtTitle.Text = model.Title;
            this.txtCon.Text = model.Con;
            this.txtIsTop.Text = model.IsTop.ToString();
            this.txtIsShow.Text = model.IsShow.ToString();
            this.txtSate.Text = model.Sate.ToString();
            this.txtSort.Text = model.Sort.ToString();
            this.txtCreateAccountId.Text = model.CreateAccountId.ToString();
            this.txtRefreshAccountId.Text = model.RefreshAccountId.ToString();
            this.txtCreateTime.Text = model.CreateTime.ToString();
            this.txtRefreshTime.Text = model.RefreshTime.ToString();

        }

        public void btnSave_Click(object sender, EventArgs e)
        {

            //string strErr="";
            //if(!PageValidate.IsNumber(txtProjectInfoId.Text))
            //{
            //    strErr+="所属项目ID格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtDiscussClassId.Text))
            //{
            //    strErr+="所属讨论ID格式错误！\\n";	
            //}
            //if(this.txtTitle.Text.Trim().Length==0)
            //{
            //    strErr+="标题不能为空！\\n";	
            //}
            //if(this.txtCon.Text.Trim().Length==0)
            //{
            //    strErr+="内容不能为空！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtIsTop.Text))
            //{
            //    strErr+="是否置顶格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtIsShow.Text))
            //{
            //    strErr+="是否隐藏格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtSate.Text))
            //{
            //    strErr+="状态 是否开启讨论或可修改格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtSort.Text))
            //{
            //    strErr+="排序格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtCreateAccountId.Text))
            //{
            //    strErr+="添加者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtRefreshAccountId.Text))
            //{
            //    strErr+="更新者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtCreateTime.Text))
            //{
            //    strErr+="创建时间格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtRefreshTime.Text))
            //{
            //    strErr+="更新时间 最近一次编辑的人员I格式错误！\\n";	
            //}

            //if(strErr!="")
            //{
            //    MessageBox.Show(this,strErr);
            //    return;
            //}
            int Id = int.Parse(this.lblId.Text);
            int ProjectInfoId = int.Parse(this.txtProjectInfoId.Text);
            int DiscussClassId = int.Parse(this.txtDiscussClassId.Text);
            string Title = this.txtTitle.Text;
            string Con = this.txtCon.Text;
            int IsTop = int.Parse(this.txtIsTop.Text);
            int IsShow = int.Parse(this.txtIsShow.Text);
            int Sate = int.Parse(this.txtSate.Text);
            int Sort = int.Parse(this.txtSort.Text);
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Text);
            int RefreshAccountId = int.Parse(this.txtRefreshAccountId.Text);
            DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Text);
            DateTime RefreshTime = DateTime.Parse(this.txtRefreshTime.Text);


            XinYiOffice.Model.Discuss model = new XinYiOffice.Model.Discuss();
            model.Id = Id;
            model.ProjectInfoId = ProjectInfoId;
            model.DiscussClassId = DiscussClassId;
            model.Title = Title;
            model.Con = Con;
            model.IsTop = IsTop;
            model.IsShow = IsShow;
            model.Sate = Sate;
            model.Sort = Sort;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.Discuss bll = new XinYiOffice.BLL.Discuss();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
