﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Discuss
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        public int CurPage = 1;
        public int iRowsDiscussRe;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("DISCUSS_LIST_VIEW"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                BasicContent bcMaster = (BasicContent)this.Master as BasicContent;
                bcMaster.SetMarketingPlanCssIsHide();

                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }

                if (!string.IsNullOrEmpty(xytools.url_get("page")))
                {
                    CurPage = SafeConvert.ToInt(xytools.url_get("page"), 1);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.Discuss bll = new XinYiOffice.BLL.Discuss();
            //XinYiOffice.Model.Discuss model = bll.GetModel(Id);

            StringBuilder strWhere = new StringBuilder();

            if (Id != 0)
            {
                strWhere.AppendFormat(" Id={0} ", Id);
                strWhere.AppendFormat(" TenantId={0} ",CurrentTenantId);
                HiddenField_DiscussId.Value = Id.ToString();

                //DataSet dsDiscuss = bll.GetList(strWhere.ToString());
                StringBuilder strSql = new StringBuilder();
                strSql.Append("select * ");
                strSql.Append(" FROM vDiscuss ");
                if (strWhere.ToString().Trim() != "")
                {
                    strSql.Append(" where " + strWhere);
                }
                DataSet dsDiscuss = DbHelperSQL.Query(strSql.ToString());

                if (dsDiscuss != null)
                {

                    txt_Title.Text = string.Format("RE:{0}", dsDiscuss.Tables[0].Rows[0]["Title"].ToString());


                    repDiscuss.DataSource = dsDiscuss;
                    repDiscuss.DataBind();
                }


                DataSet ds = bll.GetList(string.Format("ParentDiscussId={0} and TenantId={1} ", Id,CurrentTenantId));
                iRowsDiscussRe = ds.Tables[0].Rows.Count;

                repDiscussRe.DataSource = ds;
                repDiscussRe.DataBind();

            }


        }

        public string GetAccountsHeadPortrait(string _head)
        {
            return AccountsServer.GetAccountsHeadPortrait(_head);
        }

        protected void repDiscuss_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater repResAttachment = (Repeater)e.Item.FindControl("repResAttachment") as Repeater;
            DataRowView dv = (DataRowView)e.Item.DataItem;

            repResAttachment.DataSource = new BLL.ResAttachment().GetList(string.Format("KeyTable='{0}' and KeyId={1} and TenantId={2} ", "Discuss", SafeConvert.ToInt(dv["Id"], 0),CurrentTenantId));
            repResAttachment.DataBind();
        }

        protected void repDiscussRe_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater repResAttachment = (Repeater)e.Item.FindControl("repResAttachment") as Repeater;
            DataRowView dv = (DataRowView)e.Item.DataItem;

            if (!ValidatePermission("DISCUSS_TOOL_DOWNRES"))
            {
                repResAttachment.Visible = false;
                return;
            }

            repResAttachment.DataSource = new BLL.ResAttachment().GetList(string.Format("KeyTable='{0}' and KeyId={1} and TenantId={2} ", "Discuss", SafeConvert.ToInt(dv["Id"], 0),CurrentTenantId));
            repResAttachment.DataBind();
        }

        #region //楼层显示方法
        public string FloorIndex(int i)//楼层方法
        {
            int _lc = (CurPage - 1) * iRowsDiscussRe + i; //楼(当前页数-1)*每页行数+变量,i获取的是控件中Container.ItemIndex特有的序号

            return _lc.ToString();
        }
        #endregion

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {

                Model.Discuss dis = new Model.Discuss();
                dis.Con = TextBox_Con.Text;
                dis.CreateAccountId = CurrentAccountId;
                dis.CreateTime = DateTime.Now;
                dis.DiscussClassId = 0;
                dis.IsShow = 1;
                dis.IsTop = 0;
                dis.ParentDiscussId = SafeConvert.ToInt(HiddenField_DiscussId.Value);
                dis.ProjectInfoId = 0;
                dis.Sate = 1;
                dis.Sort = 1;
                dis.Title = txt_Title.Text;

                dis.Id=new BLL.Discuss().Add(dis);

                #region //上传附件
                ResAttachmentServer.UpLoadRes(Request.Files, "Discuss", dis.Id.ToString(), CurrentAccount);
                #endregion



                xytools.href_url(string.Format("Show.aspx?Id={0}&page={1}",HiddenField_DiscussId.Value,this.CurPage));
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            finally
            { 
            }

        }

    }
}
