﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Web.Discuss
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.Discuss bll = new XinYiOffice.BLL.Discuss();

        public int ProjectId = 0;
        public int ClassId = 0;

        public int _page = 1;
        public int _pagezs = 0;
        public int _datazs = 0;
        string benye_url;//当前url
        public string pagehtml = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("DISCUSS_LIST"))
            {
                base.NoPermissionPage();
            }
            
            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
            else if (!Page.IsPostBack)
            {
                BindData();
            } 
            
        }
        
        #region gridView        
        public void BindData()
        {
            //DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder();
            strWhere.AppendFormat(" ParentDiscussId=0 ");

            ProjectId = SafeConvert.ToInt(xytools.url_get("ProjectId"), 0);
            ClassId = SafeConvert.ToInt(xytools.url_get("ClassId"),0);

            if (ClassId == 0)
            {
                addLink.Visible = false;
                return;
            }
            else
            {
                if (strWhere.ToString() != string.Empty) { strWhere.Append(" and "); }
                if (ProjectId != 0)
                {
                    addLink.HRef = string.Format("Add.aspx?ProjectId={0}&ClassId={1}", ProjectId, ClassId);
                    strWhere.AppendFormat(" ProjectInfoId={0} and DiscussClassId={1} ", ProjectId, ClassId);
                }
                else
                {
                    addLink.HRef = string.Format("Add.aspx?ClassId={0}",ClassId);
                    strWhere.AppendFormat(" DiscussClassId={0} ", ClassId);
                }
            }
            strWhere.AppendFormat(" and TenantId={0} ",CurrentTenantId);
            //ds = bll.GetList(strWhere.ToString());

            //repDiscuss.DataSource = ds;

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vDiscuss ");
            if (strWhere.ToString().Trim() != "")
            {
                strSql.Append(" where " + strWhere.ToString());
            }

            #region 分页
            if (!string.IsNullOrEmpty(xytools.url_get("page")))
            {
                _page = SafeConvert.ToInt(xytools.url_get("page"), 1);
            }
            PageDataSet pd = new PageDataSet();
            repDiscuss.DataSource = pd.GetListPageBySql(strSql.ToString(), _page, 10, ref _pagezs, ref _datazs);
            PageAttribute pa = new PageAttribute();
            pa.PagCur = _page.ToString();
            pa.PageLinage = "10";
            pa.PageShowFL = "1";
            pa.PageShowGo = "1";

            benye_url = "list.aspx?" + xyurl.GetQueryRemovePage(Request.QueryString);//获取当前url字符串
            pa.PageUrl = benye_url;
            pagehtml = ParseHTML.PageLabel(pa, _pagezs);
            #endregion


            //repDiscuss.DataSource = dt;
            repDiscuss.DataBind();
        }
        #endregion





    }
}
