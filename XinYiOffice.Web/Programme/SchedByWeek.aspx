﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="SchedByWeek.aspx.cs" Inherits="XinYiOffice.Web.Programme.SchedByWeek" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/calendar.css"  type="text/css"/>
<script type="text/javascript" src="/js/calendar.js"></script>

<script type="text/javascript">
    $(function () {

        var tb = [
		{
		    'StartTime': '01:20',
		    'EndTime': '02:00',
		    'con': {
		        'ScheduleType': '',
		        'Title': '吃饭',
		        'Locale': '北京中关村',
		        'Note': '和客户一起吃饭商议合作事项'
		    }
		},
		{
		    'StartTime': '06:20',
		    'EndTime': '10:00',
		    'con': {
		        'ScheduleType': '',
		        'Title': '吃饭',
		        'Locale': '北京中关村',
		        'Note': '和客户一起吃饭商议合作事项'
		    }
		},
		{
		    'StartTime': '12:00',
		    'EndTime': '12:50',
		    'con': {
		        'ScheduleType': '',
		        'Title': '吃饭',
		        'Locale': '北京中关村',
		        'Note': '和客户一起吃饭商议合作事项'
		    }
		},
		{
		    'StartTime': '14:20',
		    'EndTime': '16:00',
		    'con': {
		        'ScheduleType': '',
		        'Title': '吃饭',
		        'Locale': '北京中关村',
		        'Note': '和客户一起吃饭商议合作事项'
		    }
		},
		{
		    'StartTime': '20:20',
		    'EndTime': '23:00',
		    'con': {
		        'ScheduleType': '',
		        'Title': '吃饭',
		        'Locale': '北京中关村',
		        'Note': '和客户一起吃饭商议合作事项'
		    }
		}
	];
        $.each(tb, function (i, n) {
            $('#tb').append('<div></div>');
            var oDiv = $('#tb').find('div').eq(i);
            var wl = $('#tb').width() / tb.length
            var startTime = n.StartTime;
            var endTime = n.EndTime;
            var t = 0;
            var b = 0;
            t += 100 * parseInt(startTime);
            t += parseInt(parseInt(startTime.substring(3)) / 60 * 100);
            b += 100 * parseInt(endTime);
            b += parseInt(parseInt(endTime.substring(3)) / 60 * 100);
            oDiv.css({ 'width': wl, 'top': t - 100, 'left': wl * i - 2, 'height': b - t })
            var oHtml = '<h4>事项：' + n.con.Title + '</h4><p>时间：' + startTime + '-' + endTime + '</p><p>地点：' + n.con.Locale + '</p><span>' + n.con.Note + '</span>';
            oDiv.html(oHtml);
            oDiv.attr('title', '事项：' + n.con.Title + '地点：' + n.con.Locale + '简介：' + n.con.Note);
        });
        $('#week td p').hover(function () {
            $(this).find('span').show()
        }, function () {
            $(this).find('span').hide()
        });
        $('#week td p span').click(function () {
            $(this).parent().remove();
        })
    })
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" runat="server">
 <h4 class="rclb">日程列表</h4>
    <div class="clear date_day">
    
        <asp:linkbutton id="cmdLastDay" runat="server"  CssClass="prev" 
            onclick="cmdLastDay_Click">上一周</asp:linkbutton>
        <p id="lblHeader" runat="server">2013年5月2号星期四</p>

        <asp:linkbutton id="cmdNextDay" runat="server"   
            CssClass="next" onclick="cmdNextDay_Click">下一周</asp:linkbutton>
    	
    </div>

    <div id="week">
    	<table id="table_week"  title="日历">
        	<tr>
              <th style=" color:#d86b19" align="center" abbr="星期日" scope="col">日</th>
              <th align="center" abbr="星期一" scope="col">一</th>
              <th align="center" abbr="星期二" scope="col">二</th>
              <th align="center" abbr="星期三" scope="col">三</th>
              <th align="center" abbr="星期四" scope="col">四</th>
              <th align="center" abbr="星期五" scope="col">五</th>
              <th style=" color:#d86b19"  align="center" abbr="星期六" scope="col">六</th>
            </tr>
            <tr>
            	<td>
                	<a class="tda" title=""><% =day(sDate(0))%></a>
                    <%--<p class="act">12:20 准备吃饭<span></span></p>--%>
                    <%=ssubject(0)%>
                </td>
            	<td><a class="tda" title=""><% =day(sDate(1))%></a><%=ssubject(1)%></td>
            	<td><a class="tda" title=""><% =day(sDate(2))%></a><%=ssubject(2)%></td>
            	<td><a class="tda" title=""><% =day(sDate(3))%></a><%=ssubject(3)%></td>
            	<td><a class="tda" title=""><% =day(sDate(4))%></a><%=ssubject(4)%></td>
            	<td><a class="tda" title=""><% =day(sDate(5))%></a><%=ssubject(5)%></td>
            	<td><a class="tda" title=""><% =day(sDate(6))%></a><%=ssubject(6)%></td>
            </tr>
        </table>
    </div>

</form>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cp2" runat="server">
</asp:Content>
