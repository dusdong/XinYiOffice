﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;
using System.Data;

namespace XinYiOffice.Web.Programme
{
    public partial class Schedule : BasicPage
    {
        private static DateTime date = new DateTime();

        private static DataTable dtProgrammeAll = new DataTable();

        public int cutAccountId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Calendar1.Style.Value = "border-width:1px;border-style:solid;height:353px;width:690px;border-collapse:collapse;";
            if (!ValidatePermission("PROGRAMME_MANAGE"))
            {
                base.NoPermissionPage();
            }


            if(!IsPostBack)
            {
                BasicContent bcMaster = (BasicContent)this.Master as BasicContent;
                bcMaster.SetMarketingPlanCssIsHide();
                cutAccountId = CurrentAccountId;

                if (!string.IsNullOrEmpty(xytools.url_get("date")))
                {
                    date = SafeConvert.ToDateTime(xytools.url_get("date"));
                }
                else
                {
                    date = DateTime.Now;
                }
                 
                GetProgrammeAll();
                Calendar1.VisibleDate = date;
                
            }

        }

        public DateTime GetDate()
        {
            return date;
        }

        public void GetProgrammeAll()
        {
            if (cutAccountId == 0)
            {
                cutAccountId = CurrentAccountId;
            }

            //if (dtProgrammeAll == null || dtProgrammeAll.Columns.Count <= 0)
            //{
            string mysql = " SELECT * FROM vProgrammeMembers WHERE (DATEPART(month, StartTime) >= {0}) AND (DATEPART(year, StartTime) >= {1}) and TenantId={2}";
                mysql = string.Format(mysql, date.Month, date.Year,CurrentTenantId);

                dtProgrammeAll= Common.DbHelperSQL.Query(mysql).Tables[0];
            //} 
        }

        public void calSchedule_DayRender(object sender, DayRenderEventArgs e)
        {
            CalendarDay d = ((DayRenderEventArgs)e).Day;
            TableCell c = ((DayRenderEventArgs)e).Cell;
            //c.Attributes.Add("onclick", "return false;");
            c.Attributes.Add("onclick", string.Format("return setTimeProgramme('{0}')", d.Date.ToString("yyyy-MM-dd")));

            CNDate dt = new CNDate(d.Date);

            if (d.IsToday)
            {
                c.Attributes.Add("style", "background-color:#fff7ce;");
            }
            if (d.IsOtherMonth)
            {
                c.Controls.Clear();
            }
            else
            {
                //string[] st = { "1111", "2222", "33333" };
                //foreach (string s in st)
                //{
                //    System.Web.UI.HtmlControls.HtmlGenericControl p = new HtmlGenericControl("p");
                //    p.InnerText = s;
                //    c.Controls.Add(p);
                //}
                DateTime dtNow = dt.Date;
                if (dtProgrammeAll != null && dtProgrammeAll.Rows.Count <= 0)
                {
                    return;
                }

                DataRow[] dr = dtProgrammeAll.Select(string.Format("StartTime<='{0}' and EndTime>='{0}' and AccountId={1}", dtNow,cutAccountId));
                if (dr.Length > 0)
                {
                    foreach (DataRow _dr in dr)
                    {
                        System.Web.UI.HtmlControls.HtmlGenericControl p = new HtmlGenericControl("p");
                        p.InnerText = SafeConvert.ToString(_dr["Title"]);
                    
                        c.Controls.Add(p);
                    }
                }

            }



            //e.Cell.Attributes["onmouseover"] = "javascript:this.style.backgroundColor='#fff7ce';cursor='hand';";
            //e.Cell.Attributes["onmouseout"] = "javascript:this.style.backgroundColor='#ffffff';";
        }

        protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {

            date = e.NewDate;
            //GetProgrammeAll();

            //Calendar1.SelectionMode = CalendarSelectionMode.DayWeekMonth;
            Response.Redirect("Schedule.aspx?date=" + date.ToShortDateString());
           //xytools.href_url_new("Schedule.aspx?date=" + date.ToShortDateString());
            //GetProgrammeAll();
            //Calendar1.DataBind();
        }

    }
}