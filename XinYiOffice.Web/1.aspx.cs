﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;
using System.Text;
using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.Web
{
    public partial class _1 : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //List<Model.MenuTree> menuList = new BLL.MenuTree().GetModelList(string.Format(" 1=1 "));

            //Model.MenuTree mt = null;

            //foreach(Model.MenuTree _mt in menuList )
            //{
            //    _mt.Guid = Guid.NewGuid().ToString().ToUpper();
            //    _mt.CreateTime = DateTime.Now;
            //    _mt.RefreshTime = DateTime.Now;
            //    _mt.CreateAccountId = -99;
            //    _mt.RefreshAccountId = -99;

            //    new BLL.MenuTree().Update(_mt);
            //}

            //MailTool.SendStmp("84553984@qq.com","测试","内容",true);

            StringBuilder sb = new StringBuilder();
            try
            {

                sb.AppendFormat("租户id:{0}", CurrentTenantId);
                sb.AppendFormat("租户cookie:{0}", XinYiOffice.Common.CookieHelper.GetCookie("xyo_cookie_tenantid"));

                sb.AppendFormat("账户id:{0}", CurrentAccountId);
                sb.AppendFormat("账户cookie:{0}", CookieHelper.GetCookie("xyo_cookie_aid"));

                DataTable dt = AppDataCacheServer.MenuTreeList();
                sb.AppendFormat("菜单缓存:{0}", dt.Rows.Count);

                sb.AppendFormat("权限缓存:{0}", CurrentRolePermissions.Rows.Count);
            }
            catch(Exception ex)
            {
                sb.Append("出错:"+ex.Message.ToString());
            }


           
            Response.Write(sb.ToString());
        }
    }
}