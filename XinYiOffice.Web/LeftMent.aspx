﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeftMent.aspx.cs" Inherits="XinYiOffice.Web.LeftMent" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link href="/css/init.css" rel="stylesheet" type="text/css" />
    <link href="/css/left.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="/js/init_ifream.js"></script>
</head>
<body>
    <asp:Panel ID="panLeftDefault" runat="server">
        <dl id="left1" class="left_list">
            <dt>快捷设置</dt>
            <dd class="list1">
                <a _href="/MyAccount.aspx" href="javascript:;">我的帐号</a></dd>
            <dd class="list2">
                <a _href="/ChangePassword.aspx" href="javascript:;">修改密码</a></dd>
            <dd class="list3">
                <a _href="/DesktopSettings.aspx" href="javascript:;">桌面设置</a></dd>
        </dl>
    </asp:Panel>
    <asp:Panel ID="panLeft" runat="server">
        <dl id="kh_list" class="left_list">
            <dt>
                <asp:Literal ID="litCurrentMenuTreeName" runat="server"></asp:Literal></dt>
            <asp:Repeater ID="repLeftMent" runat="server" OnItemDataBound="repLeftMent_ItemDataBound">
                <ItemTemplate>
                    <dd>
                        <p>
                            <%#DataBinder.Eval(Container.DataItem, "Name")%></p>
                        <ul>
                            <asp:Repeater ID="repLeftMentClass" runat="server">
                                <ItemTemplate>
                                    <li><a _href="<%#DataBinder.Eval(Container.DataItem, "ChainedAddress")%>" href="javascript:;">
                                        <%#DataBinder.Eval(Container.DataItem, "Name")%></a></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </dd>
                </ItemTemplate>
            </asp:Repeater>
        </dl>
    </asp:Panel>
    <script language="javascript">
        var tid = '<%=MenuTreeId%>';

        function InitOpenPage() {
            $(".left_list dd").find("p").first().parent().toggleClass("active");

                var a_first = $(".left_list dd").find("p").first().next("ul").find("li").first().find("a");
                var _href = a_first.attr('_href');
                var _titleName = a_first.html();
                var bStop = false;
                var bStopIndex = 0;

                var show_navLi = topWindow.find('#min_title_list li');
                show_navLi.each(function () {
                    if ($(this).find('span').html() == _titleName) {
                        bStop = true;
                        bStopIndex = show_navLi.index($(this));
                        return false;
                    }
                });
                if (!bStop) {
                    creatIframe(_href, _titleName);
                    min_titleList();
                }

            }
            if (tid != '0')
            {
            window.setTimeout(InitOpenPage, 200);
            }

    </script>
</body>
</html>
