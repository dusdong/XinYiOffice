﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinYiOffice.Common.Web
{
    public class BaseData
    {

        //获取客户端信息大全
        public class ClientInformation
        {
            public static List<TwoString> getClientInformation()
            {
                List<string> info = new List<string>();
                List<TwoString> Clientinfo = new List<TwoString>();
                foreach (object obj in System.Web.HttpContext.Current.Request.ServerVariables)
                {
                    info.Add(obj.ToString());
                }

                foreach (string str in info)
                {
                    Clientinfo.Add(new TwoString(string.Format(@"Request.ServerVariables[""{0}""]", str), System.Web.HttpContext.Current.Request.ServerVariables[str].ToString()));
                }

                return Clientinfo;
            }

            public static string JoinToString(string First, string Second)
            {
                return string.Format("Request对象:{0} 返回值:{1}<br/>", First, Second);
            }

            /// <summary>
            /// 获取客户端头部的所有信息
            /// </summary>
            /// <returns></returns>
            public static string GetAllClientInfoStrnig()
            {
                List<BaseData.TwoString> clientinfo = BaseData.ClientInformation.getClientInformation();

                StringBuilder sb = new StringBuilder();

                foreach (BaseData.TwoString outinf in clientinfo)
                {
                    if (outinf.First == "Request.ServerVariables[\"ALL_RAW\"]" || outinf.First =="Request.ServerVariables[\"ALL_HTTP\"]")
                    {
                        sb.Append(BaseData.ClientInformation.JoinToString(outinf.First, outinf.Second));
                    }


                }

                return sb.ToString();
            }

        }

        public class TwoString
        {
            public string First = string.Empty;
            public string Second = string.Empty;
            public TwoString(string str1, string str2)
            {
                First = str1;
                Second = str2;
            }

        }

    }
}
