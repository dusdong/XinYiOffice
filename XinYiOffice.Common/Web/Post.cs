﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;

namespace XinYiOffice.Common.Web
{
    public class Post
    {
        public static string GetPage(string posturl, string encoding, string postData)
        {
            //return GetPage(posturl, postData, encoding, "GET");


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(posturl + (postData == "" ? "" : "?") + postData);
            request.Method = "GET";
            request.ContentType = string.Format("text/html;charset={0}", encoding);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding(encoding));
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();

            return retString;


        }

        public static string PostPage(string posturl, string encoding, string postData)
        {
            return GetPage(posturl, postData, encoding, "POST");
        }


        public static string GetPage(string posturl, string postData, string strencoding, string method)
        {
            Stream outstream = null;
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = System.Text.Encoding.GetEncoding(strencoding);

            //postData=System.Web.HttpUtility.UrlEncode(postData, encoding);

            byte[] data = encoding.GetBytes(postData);
            // 准备请求...
            try
            {
                // 设置参数
                request = WebRequest.Create(posturl) as HttpWebRequest;

                CookieContainer cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = method;
                //request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentType = "multipart/form-data";
                request.ContentType = "application/json";


                request.ContentLength = data.Length;

                outstream = request.GetRequestStream();
                outstream.Write(data, 0, data.Length);
                outstream.Close();
                //发送请求并获取相应回应数据
                response = request.GetResponse() as HttpWebResponse;

                //直到request.GetResponse()程序才开始向目标网页发送Post请求
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码
                string content = sr.ReadToEnd();
                string err = string.Empty;
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return err;
            }
            finally
            {
                //sr.Close();
                //instream.Close();
            }
        }

        public static string HttpPost(string posturl, string postData, string strencoding, string method)
        {

            Encoding encoding = System.Text.Encoding.GetEncoding(strencoding);

            //把sXmlMessage发送到指定的DsmpUrl地址上
            Encoding encode = System.Text.Encoding.GetEncoding(strencoding);
            byte[] arrB = encode.GetBytes(postData);
            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(posturl);
            myReq.Method = method;
            myReq.ContentType = "application/x-www-form-urlencoded";
            myReq.ContentLength = arrB.Length;
            Stream outStream = myReq.GetRequestStream();
            outStream.Write(arrB, 0, arrB.Length);
            outStream.Close();

            //接收HTTP做出的响应
            WebResponse myResp = myReq.GetResponse();
            Stream ReceiveStream = myResp.GetResponseStream();                
            StreamReader readStream = new StreamReader( ReceiveStream, encode );
            Char[] read = new Char[256];
            int count = readStream.Read( read, 0, 256 );
            string str = null;
            while (count > 0) 
            {
                str += new String(read, 0, count);
                count = readStream.Read(read, 0, 256);
            } 
            readStream.Close();
            myResp.Close();



            return str;


        }
    }
    
}
