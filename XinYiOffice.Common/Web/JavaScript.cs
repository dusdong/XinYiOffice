﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinYiOffice.Common.Web
{
    /// <summary>
    /// 对JavaScript常用操作的封装方法,如alert或window.close等
    /// </summary>
    public class JavaScript
    {
        /// <summary>
        /// 弹出对话框返回刚才页面
        /// </summary>
        /// <param name="message">弹出的信息</param>
        public static void Alert(string message)
        {
            string code = "<script language=\"javascript\">alert(\"{0}\");vbscript:history.back();</script>";
            string jscode = string.Format(code, message);
            System.Web.HttpContext.Current.Response.Write(jscode);
        }

        /// <summary>
        /// 弹出提示框并刷新当前页
        /// </summary>
        /// <param name="message"></param>
        public static void AlertAndRefresh(string message)
        {
            string url = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();

            AlertAndRedirect(message, url, true);
        }

        /// <summary>
        /// 弹出提示框并跳转到指定页面(不刷新)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="locationhref"></param>
        public static void AlertAndRedirect(string message, string locationhref)
        {
            AlertAndRedirect(message, locationhref, false);
        }

        /// <summary>
        /// 弹出提示框并跳转到指定页面
        /// </summary>
        /// <param name="message">弹出的信息内容</param>
        /// <param name="locationhref">要跳转倒地址</param>
        /// <param name="isNewPage">是否为新加载(true:是/false:否)</param>
        public static void AlertAndRedirect(string message, string locationhref, bool isNewPage)
        {
            string code = "<script language=\"javascript\">alert(\"{0}\");this.location.href='{1}'{2}</script>";
            string random = "+'?_ccwrandom='+Math.random();";
            string randomappend = "+'&_ccwrandom='+Math.random();";

            string norandom = ";";
            string locationhref_suffix = string.Empty;

            //随机后缀
            if (locationhref.LastIndexOf("?") > 0)
            {
                locationhref_suffix = randomappend;
            }
            else
            {
                locationhref_suffix = random;
            }

            //若不重新加载,后缀则为;号
            if (!isNewPage)
            {
                locationhref_suffix = norandom;
            }

            System.Web.HttpContext.Current.Response.Write(string.Format(code, message, locationhref, locationhref_suffix));
        }

        /// <summary>
        /// 弹出确认信息,转向yesurl指向地址,否则转向nourl
        /// </summary>
        /// <param name="message">提示的信息内容</param>
        /// <param name="yesurl">点击确定要跳转的地址</param>
        /// <param name="nourl">取消要跳转的地址</param>
        public static void ConfirmAndRedirect(string message, string yesurl, string nourl)
        {
            string code = "<script language=\"javascript\">if(confirm(\"{dy:1}\")){this.location.href='{dy:2} }else{this.location.href='{dy:3} }</script>";
            string random = "?_ccwrandom='+Math.random();";
            string random_append = "&_ccwrandom='+Math.random();";

            string yesurl_suffix = string.Empty;
            string nourl_suffix = string.Empty;

            if (yesurl.LastIndexOf("?") > 0)
            {
                yesurl_suffix = random_append;
            }
            else
            {
                yesurl_suffix = random;
            }

            if (nourl.LastIndexOf("?") > 0)
            {
                nourl_suffix = random_append;
            }
            else
            {
                nourl_suffix = random;
            }

            StringBuilder jscode = new StringBuilder(code);
            jscode.Replace("{dy:1}", message);
            jscode.Replace("{dy:2}", yesurl + yesurl_suffix);
            jscode.Replace("{dy:3}", nourl + nourl_suffix);

            System.Web.HttpContext.Current.Response.Write(jscode.ToString());
        }

        /// <summary>
        /// 关闭当前页(不提示)
        /// </summary>
        public static void WindowCloseNone()
        {
            System.Web.HttpContext.Current.Response.Write(@"<script>window.opener=null;window.open('','_self');window.close();</script>");
        }

        /// <summary>
        /// 关闭当前页(询问是否关闭)
        /// </summary>
        public static void WindowClose()
        {
            System.Web.HttpContext.Current.Response.Write(@"<script>window.close();</script>");
        }


        /// <summary>
        /// 打开指定大小的新窗体
        /// </summary>
        /// <param name="url">地址</param>
        /// <param name="width">宽</param>
        /// <param name="heigth">高</param>
        /// <param name="top">头位置</param>
        /// <param name="left">左位置</param>
        public static void OpenWindowSize(string url, int width, int heigth, int top, int left)
        {
            string code = "<script language=\"javaScript\" >window.open('{0}','','height={1},width={2},top={3},left={4},location=yes,menubar=yes,resizable=yes,scrollbars=yes,status=yes,titlebar=yes,toolbar=yes,directories=yes');</script>";

            string jscode = string.Format(code,
                url,
                heigth,
                width,
                top,
                left
                );

            System.Web.HttpContext.Current.Response.Write(jscode);
        }

    }

}
