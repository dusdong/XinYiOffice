﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;

namespace XinYiOffice.Common
{
    public class xyurl
    {
        /// <summary>
        /// 当前页面的绝对地址,带参数,如 http://www.xinyicms.com/gaowenlong.aspx?page=2&sid=1
        /// </summary>
        /// <returns></returns>
        public static string GetCurrURL()
        {
            //应用程序路径 如 http://127.0.0.1
            string m_Application;

            if (HttpContext.Current.Request.ApplicationPath.Equals("/"))
            {
                m_Application = "http://" + HttpContext.Current.Request.Url.Authority;
            }
            else
                m_Application = HttpContext.Current.Request.ApplicationPath;

            //请求完整路径 如 /manage/login.aspx
            string q_url = HttpContext.Current.Request.CurrentExecutionFilePath.ToString();

            if (HttpContext.Current.Request.QueryString != null && HttpContext.Current.Request.QueryString.ToString() != "")
            {
                q_url += "?" + HttpContext.Current.Request.QueryString.ToString();
            }

            return m_Application+q_url;
        }
        /// <summary>
        /// 获取当前url传递参数[分页专用]
        /// </summary>
        /// <param name="QS"></param>
        /// <returns></returns>
        public static string GetQueryRemovePage(System.Collections.Specialized.NameValueCollection QS)
        {
            System.Collections.Specialized.NameValueCollection coll = new System.Collections.Specialized.NameValueCollection(QS);

            //此键值为了使url参数中一直存在 page=$page$ 将此字符串传递给 ParseHTML.PageLabel,做替换输出处理
            if (coll["page"] != null)
            {
                coll.Set("page", "$page$");
            }
            else
            {
                coll.Add("page", "$page$");
            }
            StringBuilder sr = new StringBuilder();
            int loop1, loop2;
            String[] arr1 = coll.AllKeys;
            for (loop1 = 0; loop1 < arr1.Length; loop1++)
            {
                sr.Append(HttpUtility.HtmlEncode(arr1[loop1]));

                String[] arr2 = coll.GetValues(arr1[loop1]);
                for (loop2 = 0; loop2 < arr2.Length; loop2++)
                {
                    sr.Append("=" + HttpUtility.HtmlEncode(arr2[loop2]));
                }

                if (loop1 < (arr1.Length - 1))
                {
                    sr.Append("&");
                }
            }

            return sr.ToString();
        }


        public static bool IsNumeric(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            int len = value.Length;
            if ('-' != value[0] && '+' != value[0] && !char.IsNumber(value[0]))
            {
                return false;
            }
            for (int i = 1; i < len; i++)
            {
                if (!char.IsNumber(value[i]))
                {
                    return false;
                }
            }
            return true;
        }


        public static string GetTopLevelDomain(string domain)
        {
            string str = domain;
            if (str.IndexOf(".") > 0)
            {
                string[] strArr = str.Split(':')[0].Split('.');
                if (IsNumeric(strArr[strArr.Length - 1]))
                {
                    return str;
                }
                else
                {
                    string domainRules = "||com.cn|net.cn|org.cn|gov.cn|com.hk|公司|中国|网络|com|net|org|int|edu|gov|mil|arpa|Asia|biz|info|name|pro|coop|aero|museum|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cf|cg|ch|ci|ck|cl|cm|cn|co|cq|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|eh|es|et|ev|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gp|gr|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|ml|mm|mn|mo|mp|mq|mr|ms|mt|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|va|vc|ve|vg|vn|vu|wf|ws|ye|yu|za|zm|zr|zw|";
                    string tempDomain;
                    if (strArr.Length >= 4)
                    {
                        tempDomain = strArr[strArr.Length - 3] + "." + strArr[strArr.Length - 2] + "." + strArr[strArr.Length - 1];
                        if (domainRules.IndexOf("|" + tempDomain + "|") > 0)
                        {
                            return strArr[strArr.Length - 4] + "." + tempDomain;
                        }
                    }
                    if (strArr.Length >= 3)
                    {
                        tempDomain = strArr[strArr.Length - 2] + "." + strArr[strArr.Length - 1];
                        if (domainRules.IndexOf("|" + tempDomain + "|") > 0)
                        {
                            return strArr[strArr.Length - 3] + "." + tempDomain;
                        }
                    }
                    if (strArr.Length >= 2)
                    {
                        tempDomain = strArr[strArr.Length - 1];
                        if (domainRules.IndexOf("|" + tempDomain + "|") > 0)
                        {
                            return strArr[strArr.Length - 2] + "." + tempDomain;
                        }
                    }
                }
            }
            return str;
        }

    }
}
