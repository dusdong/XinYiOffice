﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinYiOffice.Common
{
    using System;
    using System.Web.Caching;


    public class xyCache
    {
        protected string strCacheName = "";//缓存名称
        protected string strCacheItemName = "";//缓存项目名称
        protected int intExpireTime = 0;//缓存过期时间,单位分钟
        protected Object objCacheObject = new object();//缓存对象

        protected string strCacheKey = "";//缓存键名称
        protected string strLastUpdate = "";//缓存最后更新时间
        protected string strResult = "";//操作结果


        public xyCache(string strCacheItemName)
        {
            //strCacheKey = strCacheItemName + "_" + strCacheName;

            strCacheKey = strCacheItemName;
            this.strCacheItemName = strCacheItemName;//项目名就作为缓存名
        }

        #region 设置/获得缓存项目
        /// <summary>
        /// 设置/获得缓存项目。
        /// </summary>
        public string CacheItemName
        {
            get
            {
                return strCacheItemName;
            }
            set
            {
                strCacheItemName = value;
                strCacheKey = strCacheItemName + "_" + strCacheName;
            }
        }
        #endregion

        #region 设置/获得缓存名称
        /// <summary>
        /// 设置/获得缓存名称。
        /// </summary>
        public string CacheName
        {
            get
            {
                return strCacheName;
            }
            set
            {
                strCacheName = value;
                strCacheKey = strCacheItemName + "_" + strCacheName;
            }
        }
        #endregion

        #region 设置缓存过期时间间隔
        /// <summary>
        /// 设置缓存过期时间间隔。
        /// </summary>
        public int ExpireTime
        {
            get
            {
                return intExpireTime;
            }
            set
            {
                intExpireTime = value;
            }
        }
        #endregion

        #region 获得缓存最后更新时间
        /// <summary>
        /// 获得缓存最后更新时间。
        /// </summary>
        public string getLastUpdatetime
        {
            get
            {
                if (System.Web.HttpContext.Current.Cache[strCacheKey + "_UpdateTime"] != null)
                {
                    return System.Web.HttpContext.Current.Cache[strCacheKey + "_UpdateTime"].ToString();
                }
                else
                {
                    return "";
                }
            }
        }
        #endregion

        #region 获得缓存过期时间
        /// <summary>
        /// 读取缓存过期时间。
        /// </summary>
        public string getLostTime
        {
            get
            {
                if (System.Web.HttpContext.Current.Cache[strCacheKey + "_LostDateTime"] != null)
                {
                    return System.Web.HttpContext.Current.Cache[strCacheKey + "_LostDateTime"].ToString();
                }
                else
                {
                    return "";
                }
            }
        }
        #endregion

        #region 保存对象到缓存中
        /// <summary>
        /// 保存对象到缓存中。
        /// </summary>
        public void SetCache(object objContent)
        {
            if (CheckParameter() == false) return;
            lock (objCacheObject)
            {
                DateTime Dt = DateTime.Now;
                System.Web.HttpContext.Current.Cache.Insert(strCacheKey, objContent, null, Dt.AddMinutes(intExpireTime), System.TimeSpan.Zero);
                System.Web.HttpContext.Current.Cache.Insert(strCacheKey + "_UpdateTime", Dt.ToString(), null, Dt.AddSeconds(intExpireTime), System.TimeSpan.Zero);
                System.Web.HttpContext.Current.Cache.Insert(strCacheKey + "_LostDateTime", Dt.AddSeconds(intExpireTime), null, Dt.AddSeconds(intExpireTime), System.TimeSpan.Zero);
            }
        }
        #endregion

        #region 从缓存中取出对象
        /// <summary>
        /// 从缓存中取出对象。
        /// </summary>
        public object GetCache()
        {
            if (CheckParameter() == false) return null;
            lock (objCacheObject)
            {
                if (System.Web.HttpContext.Current.Cache[strCacheKey] != null)
                {
                    return System.Web.HttpContext.Current.Cache[strCacheKey];
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion

        #region 从缓存中清空对象
        /// <summary>
        /// 从缓存中清空对象。
        /// </summary>
        public void Clear()
        {
            if (CheckParameter() == false) return;
            lock (objCacheObject)
            {
                if (System.Web.HttpContext.Current.Cache[strCacheKey] != null)
                {
                    System.Web.HttpContext.Current.Cache.Remove(strCacheKey);
                }
                if (System.Web.HttpContext.Current.Cache[strCacheKey + "_UpdateTime"] != null)
                {
                    System.Web.HttpContext.Current.Cache.Remove(strCacheKey + "_UpdateTime");
                }
                if (System.Web.HttpContext.Current.Cache[strCacheKey + "_LostDateTime"] != null)
                {
                    System.Web.HttpContext.Current.Cache.Remove(strCacheKey + "_LostDateTime");
                }
            }
        }
        #endregion

        #region 缓存对象是否有效
        /// <summary>
        /// 缓存对象是否有效。
        /// </summary>
        public bool ValidCache()
        {
            if (CheckParameter() == false) return false;
            lock (objCacheObject)
            {
                if (System.Web.HttpContext.Current.Cache[strCacheKey] == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        #endregion

        #region 获得缓存操作结果
        /// <summary>
        /// 获得缓存操作结果。
        /// </summary>
        public string getResult
        {
            get
            {
                return strResult;
            }
        }
        #endregion

        #region 检查缓存参数
        /// <summary>
        /// 检查缓存参数。
        /// </summary>
        public bool CheckParameter()
        {
            if (strCacheItemName == "")
            {
                strResult = "缓存项目名称为空";
                return false;
            }
            //if (strCacheName == "")
            //{
            //    strResult = "缓存名称为空";
            //    return false;
            //}
            return true;
        }
        #endregion
    }


}
