﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:ClientLiaisons
	/// </summary>
	public partial class ClientLiaisons
	{
		public ClientLiaisons()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "ClientLiaisons"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ClientLiaisons");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.ClientLiaisons model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ClientLiaisons(");
			strSql.Append("ClientInfoId,PersonNumber,FullName,Power,Phone,OtherPhone,Fax,Email,QQ,Birthday,ByAccountId,Department,FunctionName,DirectSuperior_BK2_,Source,IfContact,ClassiFication,Business,PrimaryContact,ShortPhone,Sex,CountriesRegions,Zip,Address,Province,County,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,DirectSuperior,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@ClientInfoId,@PersonNumber,@FullName,@Power,@Phone,@OtherPhone,@Fax,@Email,@QQ,@Birthday,@ByAccountId,@Department,@FunctionName,@DirectSuperior_BK2_,@Source,@IfContact,@ClassiFication,@Business,@PrimaryContact,@ShortPhone,@Sex,@CountriesRegions,@Zip,@Address,@Province,@County,@Remarks,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@DirectSuperior,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ClientInfoId", SqlDbType.Int,4),
					new SqlParameter("@PersonNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@FullName", SqlDbType.VarChar,4000),
					new SqlParameter("@Power", SqlDbType.Int,4),
					new SqlParameter("@Phone", SqlDbType.VarChar,4000),
					new SqlParameter("@OtherPhone", SqlDbType.VarChar,4000),
					new SqlParameter("@Fax", SqlDbType.VarChar,4000),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@QQ", SqlDbType.VarChar,4000),
					new SqlParameter("@Birthday", SqlDbType.VarChar,4000),
					new SqlParameter("@ByAccountId", SqlDbType.Int,4),
					new SqlParameter("@Department", SqlDbType.VarChar,4000),
					new SqlParameter("@FunctionName", SqlDbType.VarChar,4000),
					new SqlParameter("@DirectSuperior_BK2_", SqlDbType.VarChar,4000),
					new SqlParameter("@Source", SqlDbType.Int,4),
					new SqlParameter("@IfContact", SqlDbType.Int,4),
					new SqlParameter("@ClassiFication", SqlDbType.Int,4),
					new SqlParameter("@Business", SqlDbType.VarChar,4000),
					new SqlParameter("@PrimaryContact", SqlDbType.Int,4),
					new SqlParameter("@ShortPhone", SqlDbType.VarChar,4000),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@CountriesRegions", SqlDbType.VarChar,4000),
					new SqlParameter("@Zip", SqlDbType.VarChar,4000),
					new SqlParameter("@Address", SqlDbType.VarChar,4000),
					new SqlParameter("@Province", SqlDbType.Int,4),
					new SqlParameter("@County", SqlDbType.Int,4),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@DirectSuperior", SqlDbType.Int,4),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.ClientInfoId;
			parameters[1].Value = model.PersonNumber;
			parameters[2].Value = model.FullName;
			parameters[3].Value = model.Power;
			parameters[4].Value = model.Phone;
			parameters[5].Value = model.OtherPhone;
			parameters[6].Value = model.Fax;
			parameters[7].Value = model.Email;
			parameters[8].Value = model.QQ;
			parameters[9].Value = model.Birthday;
			parameters[10].Value = model.ByAccountId;
			parameters[11].Value = model.Department;
			parameters[12].Value = model.FunctionName;
			parameters[13].Value = model.DirectSuperior_BK2_;
			parameters[14].Value = model.Source;
			parameters[15].Value = model.IfContact;
			parameters[16].Value = model.ClassiFication;
			parameters[17].Value = model.Business;
			parameters[18].Value = model.PrimaryContact;
			parameters[19].Value = model.ShortPhone;
			parameters[20].Value = model.Sex;
			parameters[21].Value = model.CountriesRegions;
			parameters[22].Value = model.Zip;
			parameters[23].Value = model.Address;
			parameters[24].Value = model.Province;
			parameters[25].Value = model.County;
			parameters[26].Value = model.Remarks;
			parameters[27].Value = model.CreateAccountId;
			parameters[28].Value = model.RefreshAccountId;
			parameters[29].Value = model.CreateTime;
			parameters[30].Value = model.RefreshTime;
			parameters[31].Value = model.DirectSuperior;
			parameters[32].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ClientLiaisons model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ClientLiaisons set ");
			strSql.Append("ClientInfoId=@ClientInfoId,");
			strSql.Append("PersonNumber=@PersonNumber,");
			strSql.Append("FullName=@FullName,");
			strSql.Append("Power=@Power,");
			strSql.Append("Phone=@Phone,");
			strSql.Append("OtherPhone=@OtherPhone,");
			strSql.Append("Fax=@Fax,");
			strSql.Append("Email=@Email,");
			strSql.Append("QQ=@QQ,");
			strSql.Append("Birthday=@Birthday,");
			strSql.Append("ByAccountId=@ByAccountId,");
			strSql.Append("Department=@Department,");
			strSql.Append("FunctionName=@FunctionName,");
			strSql.Append("DirectSuperior_BK2_=@DirectSuperior_BK2_,");
			strSql.Append("Source=@Source,");
			strSql.Append("IfContact=@IfContact,");
			strSql.Append("ClassiFication=@ClassiFication,");
			strSql.Append("Business=@Business,");
			strSql.Append("PrimaryContact=@PrimaryContact,");
			strSql.Append("ShortPhone=@ShortPhone,");
			strSql.Append("Sex=@Sex,");
			strSql.Append("CountriesRegions=@CountriesRegions,");
			strSql.Append("Zip=@Zip,");
			strSql.Append("Address=@Address,");
			strSql.Append("Province=@Province,");
			strSql.Append("County=@County,");
			strSql.Append("Remarks=@Remarks,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("DirectSuperior=@DirectSuperior,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@ClientInfoId", SqlDbType.Int,4),
					new SqlParameter("@PersonNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@FullName", SqlDbType.VarChar,4000),
					new SqlParameter("@Power", SqlDbType.Int,4),
					new SqlParameter("@Phone", SqlDbType.VarChar,4000),
					new SqlParameter("@OtherPhone", SqlDbType.VarChar,4000),
					new SqlParameter("@Fax", SqlDbType.VarChar,4000),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@QQ", SqlDbType.VarChar,4000),
					new SqlParameter("@Birthday", SqlDbType.VarChar,4000),
					new SqlParameter("@ByAccountId", SqlDbType.Int,4),
					new SqlParameter("@Department", SqlDbType.VarChar,4000),
					new SqlParameter("@FunctionName", SqlDbType.VarChar,4000),
					new SqlParameter("@DirectSuperior_BK2_", SqlDbType.VarChar,4000),
					new SqlParameter("@Source", SqlDbType.Int,4),
					new SqlParameter("@IfContact", SqlDbType.Int,4),
					new SqlParameter("@ClassiFication", SqlDbType.Int,4),
					new SqlParameter("@Business", SqlDbType.VarChar,4000),
					new SqlParameter("@PrimaryContact", SqlDbType.Int,4),
					new SqlParameter("@ShortPhone", SqlDbType.VarChar,4000),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@CountriesRegions", SqlDbType.VarChar,4000),
					new SqlParameter("@Zip", SqlDbType.VarChar,4000),
					new SqlParameter("@Address", SqlDbType.VarChar,4000),
					new SqlParameter("@Province", SqlDbType.Int,4),
					new SqlParameter("@County", SqlDbType.Int,4),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@DirectSuperior", SqlDbType.Int,4),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.ClientInfoId;
			parameters[1].Value = model.PersonNumber;
			parameters[2].Value = model.FullName;
			parameters[3].Value = model.Power;
			parameters[4].Value = model.Phone;
			parameters[5].Value = model.OtherPhone;
			parameters[6].Value = model.Fax;
			parameters[7].Value = model.Email;
			parameters[8].Value = model.QQ;
			parameters[9].Value = model.Birthday;
			parameters[10].Value = model.ByAccountId;
			parameters[11].Value = model.Department;
			parameters[12].Value = model.FunctionName;
			parameters[13].Value = model.DirectSuperior_BK2_;
			parameters[14].Value = model.Source;
			parameters[15].Value = model.IfContact;
			parameters[16].Value = model.ClassiFication;
			parameters[17].Value = model.Business;
			parameters[18].Value = model.PrimaryContact;
			parameters[19].Value = model.ShortPhone;
			parameters[20].Value = model.Sex;
			parameters[21].Value = model.CountriesRegions;
			parameters[22].Value = model.Zip;
			parameters[23].Value = model.Address;
			parameters[24].Value = model.Province;
			parameters[25].Value = model.County;
			parameters[26].Value = model.Remarks;
			parameters[27].Value = model.CreateAccountId;
			parameters[28].Value = model.RefreshAccountId;
			parameters[29].Value = model.CreateTime;
			parameters[30].Value = model.RefreshTime;
			parameters[31].Value = model.DirectSuperior;
			parameters[32].Value = model.TenantId;
			parameters[33].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ClientLiaisons ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ClientLiaisons ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ClientLiaisons GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,ClientInfoId,PersonNumber,FullName,Power,Phone,OtherPhone,Fax,Email,QQ,Birthday,ByAccountId,Department,FunctionName,DirectSuperior_BK2_,Source,IfContact,ClassiFication,Business,PrimaryContact,ShortPhone,Sex,CountriesRegions,Zip,Address,Province,County,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,DirectSuperior,TenantId from ClientLiaisons ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.ClientLiaisons model=new XinYiOffice.Model.ClientLiaisons();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ClientInfoId"]!=null && ds.Tables[0].Rows[0]["ClientInfoId"].ToString()!="")
				{
					model.ClientInfoId=int.Parse(ds.Tables[0].Rows[0]["ClientInfoId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PersonNumber"]!=null && ds.Tables[0].Rows[0]["PersonNumber"].ToString()!="")
				{
					model.PersonNumber=ds.Tables[0].Rows[0]["PersonNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FullName"]!=null && ds.Tables[0].Rows[0]["FullName"].ToString()!="")
				{
					model.FullName=ds.Tables[0].Rows[0]["FullName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Power"]!=null && ds.Tables[0].Rows[0]["Power"].ToString()!="")
				{
					model.Power=int.Parse(ds.Tables[0].Rows[0]["Power"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Phone"]!=null && ds.Tables[0].Rows[0]["Phone"].ToString()!="")
				{
					model.Phone=ds.Tables[0].Rows[0]["Phone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OtherPhone"]!=null && ds.Tables[0].Rows[0]["OtherPhone"].ToString()!="")
				{
					model.OtherPhone=ds.Tables[0].Rows[0]["OtherPhone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Fax"]!=null && ds.Tables[0].Rows[0]["Fax"].ToString()!="")
				{
					model.Fax=ds.Tables[0].Rows[0]["Fax"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Email"]!=null && ds.Tables[0].Rows[0]["Email"].ToString()!="")
				{
					model.Email=ds.Tables[0].Rows[0]["Email"].ToString();
				}
				if(ds.Tables[0].Rows[0]["QQ"]!=null && ds.Tables[0].Rows[0]["QQ"].ToString()!="")
				{
					model.QQ=ds.Tables[0].Rows[0]["QQ"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Birthday"]!=null && ds.Tables[0].Rows[0]["Birthday"].ToString()!="")
				{
					model.Birthday=ds.Tables[0].Rows[0]["Birthday"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ByAccountId"]!=null && ds.Tables[0].Rows[0]["ByAccountId"].ToString()!="")
				{
					model.ByAccountId=int.Parse(ds.Tables[0].Rows[0]["ByAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Department"]!=null && ds.Tables[0].Rows[0]["Department"].ToString()!="")
				{
					model.Department=ds.Tables[0].Rows[0]["Department"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FunctionName"]!=null && ds.Tables[0].Rows[0]["FunctionName"].ToString()!="")
				{
					model.FunctionName=ds.Tables[0].Rows[0]["FunctionName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DirectSuperior_BK2_"]!=null && ds.Tables[0].Rows[0]["DirectSuperior_BK2_"].ToString()!="")
				{
					model.DirectSuperior_BK2_=ds.Tables[0].Rows[0]["DirectSuperior_BK2_"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Source"]!=null && ds.Tables[0].Rows[0]["Source"].ToString()!="")
				{
					model.Source=int.Parse(ds.Tables[0].Rows[0]["Source"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IfContact"]!=null && ds.Tables[0].Rows[0]["IfContact"].ToString()!="")
				{
					model.IfContact=int.Parse(ds.Tables[0].Rows[0]["IfContact"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ClassiFication"]!=null && ds.Tables[0].Rows[0]["ClassiFication"].ToString()!="")
				{
					model.ClassiFication=int.Parse(ds.Tables[0].Rows[0]["ClassiFication"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Business"]!=null && ds.Tables[0].Rows[0]["Business"].ToString()!="")
				{
					model.Business=ds.Tables[0].Rows[0]["Business"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PrimaryContact"]!=null && ds.Tables[0].Rows[0]["PrimaryContact"].ToString()!="")
				{
					model.PrimaryContact=int.Parse(ds.Tables[0].Rows[0]["PrimaryContact"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ShortPhone"]!=null && ds.Tables[0].Rows[0]["ShortPhone"].ToString()!="")
				{
					model.ShortPhone=ds.Tables[0].Rows[0]["ShortPhone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sex"]!=null && ds.Tables[0].Rows[0]["Sex"].ToString()!="")
				{
					model.Sex=int.Parse(ds.Tables[0].Rows[0]["Sex"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CountriesRegions"]!=null && ds.Tables[0].Rows[0]["CountriesRegions"].ToString()!="")
				{
					model.CountriesRegions=ds.Tables[0].Rows[0]["CountriesRegions"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Zip"]!=null && ds.Tables[0].Rows[0]["Zip"].ToString()!="")
				{
					model.Zip=ds.Tables[0].Rows[0]["Zip"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Address"]!=null && ds.Tables[0].Rows[0]["Address"].ToString()!="")
				{
					model.Address=ds.Tables[0].Rows[0]["Address"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Province"]!=null && ds.Tables[0].Rows[0]["Province"].ToString()!="")
				{
					model.Province=int.Parse(ds.Tables[0].Rows[0]["Province"].ToString());
				}
				if(ds.Tables[0].Rows[0]["County"]!=null && ds.Tables[0].Rows[0]["County"].ToString()!="")
				{
					model.County=int.Parse(ds.Tables[0].Rows[0]["County"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Remarks"]!=null && ds.Tables[0].Rows[0]["Remarks"].ToString()!="")
				{
					model.Remarks=ds.Tables[0].Rows[0]["Remarks"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DirectSuperior"]!=null && ds.Tables[0].Rows[0]["DirectSuperior"].ToString()!="")
				{
					model.DirectSuperior=int.Parse(ds.Tables[0].Rows[0]["DirectSuperior"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,ClientInfoId,PersonNumber,FullName,Power,Phone,OtherPhone,Fax,Email,QQ,Birthday,ByAccountId,Department,FunctionName,DirectSuperior_BK2_,Source,IfContact,ClassiFication,Business,PrimaryContact,ShortPhone,Sex,CountriesRegions,Zip,Address,Province,County,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,DirectSuperior,TenantId ");
			strSql.Append(" FROM ClientLiaisons ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,ClientInfoId,PersonNumber,FullName,Power,Phone,OtherPhone,Fax,Email,QQ,Birthday,ByAccountId,Department,FunctionName,DirectSuperior_BK2_,Source,IfContact,ClassiFication,Business,PrimaryContact,ShortPhone,Sex,CountriesRegions,Zip,Address,Province,County,Remarks,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,DirectSuperior,TenantId ");
			strSql.Append(" FROM ClientLiaisons ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ClientLiaisons ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from ClientLiaisons T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ClientLiaisons";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

