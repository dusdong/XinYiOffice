﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:BankAccountDetails
	/// </summary>
	public partial class BankAccountDetails
	{
		public BankAccountDetails()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "BankAccountDetails"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from BankAccountDetails");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.BankAccountDetails model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into BankAccountDetails(");
			strSql.Append("BankAccountId,IncreaseOrReduce,SpecificAmount,Remarks,OccurrenceTime,MakeCollectionsId,AdvicePaymentId,CreateAccountId,CreateTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@BankAccountId,@IncreaseOrReduce,@SpecificAmount,@Remarks,@OccurrenceTime,@MakeCollectionsId,@AdvicePaymentId,@CreateAccountId,@CreateTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BankAccountId", SqlDbType.Int,4),
					new SqlParameter("@IncreaseOrReduce", SqlDbType.Int,4),
					new SqlParameter("@SpecificAmount", SqlDbType.VarChar,4000),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@OccurrenceTime", SqlDbType.DateTime),
					new SqlParameter("@MakeCollectionsId", SqlDbType.Int,4),
					new SqlParameter("@AdvicePaymentId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.BankAccountId;
			parameters[1].Value = model.IncreaseOrReduce;
			parameters[2].Value = model.SpecificAmount;
			parameters[3].Value = model.Remarks;
			parameters[4].Value = model.OccurrenceTime;
			parameters[5].Value = model.MakeCollectionsId;
			parameters[6].Value = model.AdvicePaymentId;
			parameters[7].Value = model.CreateAccountId;
			parameters[8].Value = model.CreateTime;
			parameters[9].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.BankAccountDetails model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update BankAccountDetails set ");
			strSql.Append("BankAccountId=@BankAccountId,");
			strSql.Append("IncreaseOrReduce=@IncreaseOrReduce,");
			strSql.Append("SpecificAmount=@SpecificAmount,");
			strSql.Append("Remarks=@Remarks,");
			strSql.Append("OccurrenceTime=@OccurrenceTime,");
			strSql.Append("MakeCollectionsId=@MakeCollectionsId,");
			strSql.Append("AdvicePaymentId=@AdvicePaymentId,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@BankAccountId", SqlDbType.Int,4),
					new SqlParameter("@IncreaseOrReduce", SqlDbType.Int,4),
					new SqlParameter("@SpecificAmount", SqlDbType.VarChar,4000),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@OccurrenceTime", SqlDbType.DateTime),
					new SqlParameter("@MakeCollectionsId", SqlDbType.Int,4),
					new SqlParameter("@AdvicePaymentId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.BankAccountId;
			parameters[1].Value = model.IncreaseOrReduce;
			parameters[2].Value = model.SpecificAmount;
			parameters[3].Value = model.Remarks;
			parameters[4].Value = model.OccurrenceTime;
			parameters[5].Value = model.MakeCollectionsId;
			parameters[6].Value = model.AdvicePaymentId;
			parameters[7].Value = model.CreateAccountId;
			parameters[8].Value = model.CreateTime;
			parameters[9].Value = model.TenantId;
			parameters[10].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BankAccountDetails ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BankAccountDetails ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.BankAccountDetails GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,BankAccountId,IncreaseOrReduce,SpecificAmount,Remarks,OccurrenceTime,MakeCollectionsId,AdvicePaymentId,CreateAccountId,CreateTime,TenantId from BankAccountDetails ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.BankAccountDetails model=new XinYiOffice.Model.BankAccountDetails();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BankAccountId"]!=null && ds.Tables[0].Rows[0]["BankAccountId"].ToString()!="")
				{
					model.BankAccountId=int.Parse(ds.Tables[0].Rows[0]["BankAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IncreaseOrReduce"]!=null && ds.Tables[0].Rows[0]["IncreaseOrReduce"].ToString()!="")
				{
					model.IncreaseOrReduce=int.Parse(ds.Tables[0].Rows[0]["IncreaseOrReduce"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SpecificAmount"]!=null && ds.Tables[0].Rows[0]["SpecificAmount"].ToString()!="")
				{
					model.SpecificAmount=ds.Tables[0].Rows[0]["SpecificAmount"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remarks"]!=null && ds.Tables[0].Rows[0]["Remarks"].ToString()!="")
				{
					model.Remarks=ds.Tables[0].Rows[0]["Remarks"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OccurrenceTime"]!=null && ds.Tables[0].Rows[0]["OccurrenceTime"].ToString()!="")
				{
					model.OccurrenceTime=DateTime.Parse(ds.Tables[0].Rows[0]["OccurrenceTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["MakeCollectionsId"]!=null && ds.Tables[0].Rows[0]["MakeCollectionsId"].ToString()!="")
				{
					model.MakeCollectionsId=int.Parse(ds.Tables[0].Rows[0]["MakeCollectionsId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AdvicePaymentId"]!=null && ds.Tables[0].Rows[0]["AdvicePaymentId"].ToString()!="")
				{
					model.AdvicePaymentId=int.Parse(ds.Tables[0].Rows[0]["AdvicePaymentId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,BankAccountId,IncreaseOrReduce,SpecificAmount,Remarks,OccurrenceTime,MakeCollectionsId,AdvicePaymentId,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM BankAccountDetails ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,BankAccountId,IncreaseOrReduce,SpecificAmount,Remarks,OccurrenceTime,MakeCollectionsId,AdvicePaymentId,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM BankAccountDetails ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM BankAccountDetails ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from BankAccountDetails T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "BankAccountDetails";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

