﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:Accounts
	/// </summary>
	public partial class Accounts
	{
		public Accounts()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "Accounts"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Accounts");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.Accounts model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Accounts(");
			strSql.Append("AccountName,AccountPassword,Sate,NiceName,FullName,EnName,TimeZone,Email,HeadPortrait,Phone,AccountType,KeyId,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@AccountName,@AccountPassword,@Sate,@NiceName,@FullName,@EnName,@TimeZone,@Email,@HeadPortrait,@Phone,@AccountType,@KeyId,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@AccountName", SqlDbType.VarChar,4000),
					new SqlParameter("@AccountPassword", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@NiceName", SqlDbType.VarChar,4000),
					new SqlParameter("@FullName", SqlDbType.VarChar,4000),
					new SqlParameter("@EnName", SqlDbType.VarChar,4000),
					new SqlParameter("@TimeZone", SqlDbType.Int,4),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@HeadPortrait", SqlDbType.VarChar,4000),
					new SqlParameter("@Phone", SqlDbType.VarChar,4000),
					new SqlParameter("@AccountType", SqlDbType.Int,4),
					new SqlParameter("@KeyId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.AccountName;
			parameters[1].Value = model.AccountPassword;
			parameters[2].Value = model.Sate;
			parameters[3].Value = model.NiceName;
			parameters[4].Value = model.FullName;
			parameters[5].Value = model.EnName;
			parameters[6].Value = model.TimeZone;
			parameters[7].Value = model.Email;
			parameters[8].Value = model.HeadPortrait;
			parameters[9].Value = model.Phone;
			parameters[10].Value = model.AccountType;
			parameters[11].Value = model.KeyId;
			parameters[12].Value = model.CreateAccountId;
			parameters[13].Value = model.RefreshAccountId;
			parameters[14].Value = model.CreateTime;
			parameters[15].Value = model.RefreshTime;
			parameters[16].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.Accounts model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Accounts set ");
			strSql.Append("AccountName=@AccountName,");
			strSql.Append("AccountPassword=@AccountPassword,");
			strSql.Append("Sate=@Sate,");
			strSql.Append("NiceName=@NiceName,");
			strSql.Append("FullName=@FullName,");
			strSql.Append("EnName=@EnName,");
			strSql.Append("TimeZone=@TimeZone,");
			strSql.Append("Email=@Email,");
			strSql.Append("HeadPortrait=@HeadPortrait,");
			strSql.Append("Phone=@Phone,");
			strSql.Append("AccountType=@AccountType,");
			strSql.Append("KeyId=@KeyId,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@AccountName", SqlDbType.VarChar,4000),
					new SqlParameter("@AccountPassword", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@NiceName", SqlDbType.VarChar,4000),
					new SqlParameter("@FullName", SqlDbType.VarChar,4000),
					new SqlParameter("@EnName", SqlDbType.VarChar,4000),
					new SqlParameter("@TimeZone", SqlDbType.Int,4),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@HeadPortrait", SqlDbType.VarChar,4000),
					new SqlParameter("@Phone", SqlDbType.VarChar,4000),
					new SqlParameter("@AccountType", SqlDbType.Int,4),
					new SqlParameter("@KeyId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.AccountName;
			parameters[1].Value = model.AccountPassword;
			parameters[2].Value = model.Sate;
			parameters[3].Value = model.NiceName;
			parameters[4].Value = model.FullName;
			parameters[5].Value = model.EnName;
			parameters[6].Value = model.TimeZone;
			parameters[7].Value = model.Email;
			parameters[8].Value = model.HeadPortrait;
			parameters[9].Value = model.Phone;
			parameters[10].Value = model.AccountType;
			parameters[11].Value = model.KeyId;
			parameters[12].Value = model.CreateAccountId;
			parameters[13].Value = model.RefreshAccountId;
			parameters[14].Value = model.CreateTime;
			parameters[15].Value = model.RefreshTime;
			parameters[16].Value = model.TenantId;
			parameters[17].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Accounts ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Accounts ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.Accounts GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,AccountName,AccountPassword,Sate,NiceName,FullName,EnName,TimeZone,Email,HeadPortrait,Phone,AccountType,KeyId,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from Accounts ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.Accounts model=new XinYiOffice.Model.Accounts();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AccountName"]!=null && ds.Tables[0].Rows[0]["AccountName"].ToString()!="")
				{
					model.AccountName=ds.Tables[0].Rows[0]["AccountName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AccountPassword"]!=null && ds.Tables[0].Rows[0]["AccountPassword"].ToString()!="")
				{
					model.AccountPassword=ds.Tables[0].Rows[0]["AccountPassword"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sate"]!=null && ds.Tables[0].Rows[0]["Sate"].ToString()!="")
				{
					model.Sate=int.Parse(ds.Tables[0].Rows[0]["Sate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["NiceName"]!=null && ds.Tables[0].Rows[0]["NiceName"].ToString()!="")
				{
					model.NiceName=ds.Tables[0].Rows[0]["NiceName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FullName"]!=null && ds.Tables[0].Rows[0]["FullName"].ToString()!="")
				{
					model.FullName=ds.Tables[0].Rows[0]["FullName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EnName"]!=null && ds.Tables[0].Rows[0]["EnName"].ToString()!="")
				{
					model.EnName=ds.Tables[0].Rows[0]["EnName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TimeZone"]!=null && ds.Tables[0].Rows[0]["TimeZone"].ToString()!="")
				{
					model.TimeZone=int.Parse(ds.Tables[0].Rows[0]["TimeZone"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Email"]!=null && ds.Tables[0].Rows[0]["Email"].ToString()!="")
				{
					model.Email=ds.Tables[0].Rows[0]["Email"].ToString();
				}
				if(ds.Tables[0].Rows[0]["HeadPortrait"]!=null && ds.Tables[0].Rows[0]["HeadPortrait"].ToString()!="")
				{
					model.HeadPortrait=ds.Tables[0].Rows[0]["HeadPortrait"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Phone"]!=null && ds.Tables[0].Rows[0]["Phone"].ToString()!="")
				{
					model.Phone=ds.Tables[0].Rows[0]["Phone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AccountType"]!=null && ds.Tables[0].Rows[0]["AccountType"].ToString()!="")
				{
					model.AccountType=int.Parse(ds.Tables[0].Rows[0]["AccountType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["KeyId"]!=null && ds.Tables[0].Rows[0]["KeyId"].ToString()!="")
				{
					model.KeyId=int.Parse(ds.Tables[0].Rows[0]["KeyId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,AccountName,AccountPassword,Sate,NiceName,FullName,EnName,TimeZone,Email,HeadPortrait,Phone,AccountType,KeyId,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM Accounts ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,AccountName,AccountPassword,Sate,NiceName,FullName,EnName,TimeZone,Email,HeadPortrait,Phone,AccountType,KeyId,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM Accounts ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Accounts ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from Accounts T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Accounts";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

