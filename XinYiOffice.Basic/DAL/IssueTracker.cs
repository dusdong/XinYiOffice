﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:IssueTracker
	/// </summary>
	public partial class IssueTracker
	{
		public IssueTracker()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "IssueTracker"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from IssueTracker");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.IssueTracker model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into IssueTracker(");
			strSql.Append("ProjectInfoId,Title,Tabloid,Item,Priority,Solver,Sate,Version,Url,Constitute,Serious,AuthorsAccountId,AssignerAccountId,StartTime,EndTime,TYPE,IsEdit,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@ProjectInfoId,@Title,@Tabloid,@Item,@Priority,@Solver,@Sate,@Version,@Url,@Constitute,@Serious,@AuthorsAccountId,@AssignerAccountId,@StartTime,@EndTime,@TYPE,@IsEdit,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ProjectInfoId", SqlDbType.Int,4),
					new SqlParameter("@Title", SqlDbType.VarChar,4000),
					new SqlParameter("@Tabloid", SqlDbType.VarChar,4000),
					new SqlParameter("@Item", SqlDbType.VarChar,4000),
					new SqlParameter("@Priority", SqlDbType.VarChar,4000),
					new SqlParameter("@Solver", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@Version", SqlDbType.VarChar,4000),
					new SqlParameter("@Url", SqlDbType.VarChar,4000),
					new SqlParameter("@Constitute", SqlDbType.VarChar,4000),
					new SqlParameter("@Serious", SqlDbType.VarChar,4000),
					new SqlParameter("@AuthorsAccountId", SqlDbType.Int,4),
					new SqlParameter("@AssignerAccountId", SqlDbType.Int,4),
					new SqlParameter("@StartTime", SqlDbType.DateTime),
					new SqlParameter("@EndTime", SqlDbType.DateTime),
					new SqlParameter("@TYPE", SqlDbType.Int,4),
					new SqlParameter("@IsEdit", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.ProjectInfoId;
			parameters[1].Value = model.Title;
			parameters[2].Value = model.Tabloid;
			parameters[3].Value = model.Item;
			parameters[4].Value = model.Priority;
			parameters[5].Value = model.Solver;
			parameters[6].Value = model.Sate;
			parameters[7].Value = model.Version;
			parameters[8].Value = model.Url;
			parameters[9].Value = model.Constitute;
			parameters[10].Value = model.Serious;
			parameters[11].Value = model.AuthorsAccountId;
			parameters[12].Value = model.AssignerAccountId;
			parameters[13].Value = model.StartTime;
			parameters[14].Value = model.EndTime;
			parameters[15].Value = model.TYPE;
			parameters[16].Value = model.IsEdit;
			parameters[17].Value = model.CreateAccountId;
			parameters[18].Value = model.RefreshAccountId;
			parameters[19].Value = model.CreateTime;
			parameters[20].Value = model.RefreshTime;
			parameters[21].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.IssueTracker model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update IssueTracker set ");
			strSql.Append("ProjectInfoId=@ProjectInfoId,");
			strSql.Append("Title=@Title,");
			strSql.Append("Tabloid=@Tabloid,");
			strSql.Append("Item=@Item,");
			strSql.Append("Priority=@Priority,");
			strSql.Append("Solver=@Solver,");
			strSql.Append("Sate=@Sate,");
			strSql.Append("Version=@Version,");
			strSql.Append("Url=@Url,");
			strSql.Append("Constitute=@Constitute,");
			strSql.Append("Serious=@Serious,");
			strSql.Append("AuthorsAccountId=@AuthorsAccountId,");
			strSql.Append("AssignerAccountId=@AssignerAccountId,");
			strSql.Append("StartTime=@StartTime,");
			strSql.Append("EndTime=@EndTime,");
			strSql.Append("TYPE=@TYPE,");
			strSql.Append("IsEdit=@IsEdit,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@ProjectInfoId", SqlDbType.Int,4),
					new SqlParameter("@Title", SqlDbType.VarChar,4000),
					new SqlParameter("@Tabloid", SqlDbType.VarChar,4000),
					new SqlParameter("@Item", SqlDbType.VarChar,4000),
					new SqlParameter("@Priority", SqlDbType.VarChar,4000),
					new SqlParameter("@Solver", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@Version", SqlDbType.VarChar,4000),
					new SqlParameter("@Url", SqlDbType.VarChar,4000),
					new SqlParameter("@Constitute", SqlDbType.VarChar,4000),
					new SqlParameter("@Serious", SqlDbType.VarChar,4000),
					new SqlParameter("@AuthorsAccountId", SqlDbType.Int,4),
					new SqlParameter("@AssignerAccountId", SqlDbType.Int,4),
					new SqlParameter("@StartTime", SqlDbType.DateTime),
					new SqlParameter("@EndTime", SqlDbType.DateTime),
					new SqlParameter("@TYPE", SqlDbType.Int,4),
					new SqlParameter("@IsEdit", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.ProjectInfoId;
			parameters[1].Value = model.Title;
			parameters[2].Value = model.Tabloid;
			parameters[3].Value = model.Item;
			parameters[4].Value = model.Priority;
			parameters[5].Value = model.Solver;
			parameters[6].Value = model.Sate;
			parameters[7].Value = model.Version;
			parameters[8].Value = model.Url;
			parameters[9].Value = model.Constitute;
			parameters[10].Value = model.Serious;
			parameters[11].Value = model.AuthorsAccountId;
			parameters[12].Value = model.AssignerAccountId;
			parameters[13].Value = model.StartTime;
			parameters[14].Value = model.EndTime;
			parameters[15].Value = model.TYPE;
			parameters[16].Value = model.IsEdit;
			parameters[17].Value = model.CreateAccountId;
			parameters[18].Value = model.RefreshAccountId;
			parameters[19].Value = model.CreateTime;
			parameters[20].Value = model.RefreshTime;
			parameters[21].Value = model.TenantId;
			parameters[22].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from IssueTracker ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from IssueTracker ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.IssueTracker GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,ProjectInfoId,Title,Tabloid,Item,Priority,Solver,Sate,Version,Url,Constitute,Serious,AuthorsAccountId,AssignerAccountId,StartTime,EndTime,TYPE,IsEdit,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from IssueTracker ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.IssueTracker model=new XinYiOffice.Model.IssueTracker();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ProjectInfoId"]!=null && ds.Tables[0].Rows[0]["ProjectInfoId"].ToString()!="")
				{
					model.ProjectInfoId=int.Parse(ds.Tables[0].Rows[0]["ProjectInfoId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Title"]!=null && ds.Tables[0].Rows[0]["Title"].ToString()!="")
				{
					model.Title=ds.Tables[0].Rows[0]["Title"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Tabloid"]!=null && ds.Tables[0].Rows[0]["Tabloid"].ToString()!="")
				{
					model.Tabloid=ds.Tables[0].Rows[0]["Tabloid"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Item"]!=null && ds.Tables[0].Rows[0]["Item"].ToString()!="")
				{
					model.Item=ds.Tables[0].Rows[0]["Item"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Priority"]!=null && ds.Tables[0].Rows[0]["Priority"].ToString()!="")
				{
					model.Priority=ds.Tables[0].Rows[0]["Priority"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Solver"]!=null && ds.Tables[0].Rows[0]["Solver"].ToString()!="")
				{
					model.Solver=ds.Tables[0].Rows[0]["Solver"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sate"]!=null && ds.Tables[0].Rows[0]["Sate"].ToString()!="")
				{
					model.Sate=int.Parse(ds.Tables[0].Rows[0]["Sate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Version"]!=null && ds.Tables[0].Rows[0]["Version"].ToString()!="")
				{
					model.Version=ds.Tables[0].Rows[0]["Version"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Url"]!=null && ds.Tables[0].Rows[0]["Url"].ToString()!="")
				{
					model.Url=ds.Tables[0].Rows[0]["Url"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Constitute"]!=null && ds.Tables[0].Rows[0]["Constitute"].ToString()!="")
				{
					model.Constitute=ds.Tables[0].Rows[0]["Constitute"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Serious"]!=null && ds.Tables[0].Rows[0]["Serious"].ToString()!="")
				{
					model.Serious=ds.Tables[0].Rows[0]["Serious"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AuthorsAccountId"]!=null && ds.Tables[0].Rows[0]["AuthorsAccountId"].ToString()!="")
				{
					model.AuthorsAccountId=int.Parse(ds.Tables[0].Rows[0]["AuthorsAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AssignerAccountId"]!=null && ds.Tables[0].Rows[0]["AssignerAccountId"].ToString()!="")
				{
					model.AssignerAccountId=int.Parse(ds.Tables[0].Rows[0]["AssignerAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StartTime"]!=null && ds.Tables[0].Rows[0]["StartTime"].ToString()!="")
				{
					model.StartTime=DateTime.Parse(ds.Tables[0].Rows[0]["StartTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EndTime"]!=null && ds.Tables[0].Rows[0]["EndTime"].ToString()!="")
				{
					model.EndTime=DateTime.Parse(ds.Tables[0].Rows[0]["EndTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TYPE"]!=null && ds.Tables[0].Rows[0]["TYPE"].ToString()!="")
				{
					model.TYPE=int.Parse(ds.Tables[0].Rows[0]["TYPE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IsEdit"]!=null && ds.Tables[0].Rows[0]["IsEdit"].ToString()!="")
				{
					model.IsEdit=int.Parse(ds.Tables[0].Rows[0]["IsEdit"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,ProjectInfoId,Title,Tabloid,Item,Priority,Solver,Sate,Version,Url,Constitute,Serious,AuthorsAccountId,AssignerAccountId,StartTime,EndTime,TYPE,IsEdit,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM IssueTracker ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,ProjectInfoId,Title,Tabloid,Item,Priority,Solver,Sate,Version,Url,Constitute,Serious,AuthorsAccountId,AssignerAccountId,StartTime,EndTime,TYPE,IsEdit,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM IssueTracker ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM IssueTracker ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from IssueTracker T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "IssueTracker";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

