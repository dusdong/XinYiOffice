﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:InternalReceiver
	/// </summary>
	public partial class InternalReceiver
	{
		public InternalReceiver()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "InternalReceiver"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from InternalReceiver");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.InternalReceiver model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into InternalReceiver(");
			strSql.Append("InternalLetterId,RecipientAccountId,RecipientType,IsView,IsDelete,IsReply,LetterPosition,PositionFoler,CreateAccountId,RefreshAccountId,CheckDate,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@InternalLetterId,@RecipientAccountId,@RecipientType,@IsView,@IsDelete,@IsReply,@LetterPosition,@PositionFoler,@CreateAccountId,@RefreshAccountId,@CheckDate,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@InternalLetterId", SqlDbType.Int,4),
					new SqlParameter("@RecipientAccountId", SqlDbType.Int,4),
					new SqlParameter("@RecipientType", SqlDbType.Int,4),
					new SqlParameter("@IsView", SqlDbType.Int,4),
					new SqlParameter("@IsDelete", SqlDbType.Int,4),
					new SqlParameter("@IsReply", SqlDbType.Int,4),
					new SqlParameter("@LetterPosition", SqlDbType.Int,4),
					new SqlParameter("@PositionFoler", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CheckDate", SqlDbType.DateTime),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.InternalLetterId;
			parameters[1].Value = model.RecipientAccountId;
			parameters[2].Value = model.RecipientType;
			parameters[3].Value = model.IsView;
			parameters[4].Value = model.IsDelete;
			parameters[5].Value = model.IsReply;
			parameters[6].Value = model.LetterPosition;
			parameters[7].Value = model.PositionFoler;
			parameters[8].Value = model.CreateAccountId;
			parameters[9].Value = model.RefreshAccountId;
			parameters[10].Value = model.CheckDate;
			parameters[11].Value = model.CreateTime;
			parameters[12].Value = model.RefreshTime;
			parameters[13].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.InternalReceiver model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update InternalReceiver set ");
			strSql.Append("InternalLetterId=@InternalLetterId,");
			strSql.Append("RecipientAccountId=@RecipientAccountId,");
			strSql.Append("RecipientType=@RecipientType,");
			strSql.Append("IsView=@IsView,");
			strSql.Append("IsDelete=@IsDelete,");
			strSql.Append("IsReply=@IsReply,");
			strSql.Append("LetterPosition=@LetterPosition,");
			strSql.Append("PositionFoler=@PositionFoler,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CheckDate=@CheckDate,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@InternalLetterId", SqlDbType.Int,4),
					new SqlParameter("@RecipientAccountId", SqlDbType.Int,4),
					new SqlParameter("@RecipientType", SqlDbType.Int,4),
					new SqlParameter("@IsView", SqlDbType.Int,4),
					new SqlParameter("@IsDelete", SqlDbType.Int,4),
					new SqlParameter("@IsReply", SqlDbType.Int,4),
					new SqlParameter("@LetterPosition", SqlDbType.Int,4),
					new SqlParameter("@PositionFoler", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CheckDate", SqlDbType.DateTime),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.InternalLetterId;
			parameters[1].Value = model.RecipientAccountId;
			parameters[2].Value = model.RecipientType;
			parameters[3].Value = model.IsView;
			parameters[4].Value = model.IsDelete;
			parameters[5].Value = model.IsReply;
			parameters[6].Value = model.LetterPosition;
			parameters[7].Value = model.PositionFoler;
			parameters[8].Value = model.CreateAccountId;
			parameters[9].Value = model.RefreshAccountId;
			parameters[10].Value = model.CheckDate;
			parameters[11].Value = model.CreateTime;
			parameters[12].Value = model.RefreshTime;
			parameters[13].Value = model.TenantId;
			parameters[14].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from InternalReceiver ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from InternalReceiver ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.InternalReceiver GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,InternalLetterId,RecipientAccountId,RecipientType,IsView,IsDelete,IsReply,LetterPosition,PositionFoler,CreateAccountId,RefreshAccountId,CheckDate,CreateTime,RefreshTime,TenantId from InternalReceiver ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.InternalReceiver model=new XinYiOffice.Model.InternalReceiver();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["InternalLetterId"]!=null && ds.Tables[0].Rows[0]["InternalLetterId"].ToString()!="")
				{
					model.InternalLetterId=int.Parse(ds.Tables[0].Rows[0]["InternalLetterId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RecipientAccountId"]!=null && ds.Tables[0].Rows[0]["RecipientAccountId"].ToString()!="")
				{
					model.RecipientAccountId=int.Parse(ds.Tables[0].Rows[0]["RecipientAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RecipientType"]!=null && ds.Tables[0].Rows[0]["RecipientType"].ToString()!="")
				{
					model.RecipientType=int.Parse(ds.Tables[0].Rows[0]["RecipientType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IsView"]!=null && ds.Tables[0].Rows[0]["IsView"].ToString()!="")
				{
					model.IsView=int.Parse(ds.Tables[0].Rows[0]["IsView"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IsDelete"]!=null && ds.Tables[0].Rows[0]["IsDelete"].ToString()!="")
				{
					model.IsDelete=int.Parse(ds.Tables[0].Rows[0]["IsDelete"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IsReply"]!=null && ds.Tables[0].Rows[0]["IsReply"].ToString()!="")
				{
					model.IsReply=int.Parse(ds.Tables[0].Rows[0]["IsReply"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LetterPosition"]!=null && ds.Tables[0].Rows[0]["LetterPosition"].ToString()!="")
				{
					model.LetterPosition=int.Parse(ds.Tables[0].Rows[0]["LetterPosition"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PositionFoler"]!=null && ds.Tables[0].Rows[0]["PositionFoler"].ToString()!="")
				{
					model.PositionFoler=ds.Tables[0].Rows[0]["PositionFoler"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CheckDate"]!=null && ds.Tables[0].Rows[0]["CheckDate"].ToString()!="")
				{
					model.CheckDate=DateTime.Parse(ds.Tables[0].Rows[0]["CheckDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,InternalLetterId,RecipientAccountId,RecipientType,IsView,IsDelete,IsReply,LetterPosition,PositionFoler,CreateAccountId,RefreshAccountId,CheckDate,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM InternalReceiver ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,InternalLetterId,RecipientAccountId,RecipientType,IsView,IsDelete,IsReply,LetterPosition,PositionFoler,CreateAccountId,RefreshAccountId,CheckDate,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM InternalReceiver ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM InternalReceiver ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from InternalReceiver T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "InternalReceiver";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

