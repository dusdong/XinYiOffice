﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public class OfficeWorkerServer
    {
        /// <summary>
        /// 全部部门信息
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="topNum"></param>
        /// <returns></returns>
        public static DataTable GetOfficeWorkerList(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vOfficeWorker ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }
            strSql.Append(" order by CreateTime desc");
            return DbHelperSQL.Query(strSql.ToString()).Tables[0];
        }

    }
}
