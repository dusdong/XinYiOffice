﻿using System;
using System.Collections.Generic;
using System.Text;
using XinYiOffice.Common;
using System.Web;
using System.Collections;
using System.Web.Caching;


namespace XinYiOffice.Basic
{
    public class OutLoginAction
    {
        public static void OutAndClearAll()
        {
            try
            {
                int aid = AppDataCacheServer.GetCliCookie();
                string username = CookieHelper.GetCookie("xyo_cookie_aname");

                //SystemJournalServer.SetSysLog(string.Format("用户{0}已成功退出", username), aid, AppDataCacheServer.CurrentTenantId());

                CookieHelper.Remove("xyo_cookie_aid");
                CookieHelper.Remove("xyo_cookie_aname");
                CookieHelper.Remove("xyo_cookie_nickname");
                CookieHelper.Remove("xyo_cookie_tenantid");

                HttpContext.Current.Session.Clear();
                
                Cache c = new Cache();
                foreach (DictionaryEntry dEntry in HttpContext.Current.Cache)
                {
                    c.Remove(dEntry.Key.ToString());
                  
                }
                

            }
            catch (Exception ex)
            {
                //EasyLog.WriteLog(ex);
            }

        }
    }
}
