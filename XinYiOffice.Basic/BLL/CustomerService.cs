﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 客户服务
	/// </summary>
	public partial class CustomerService
	{
		private readonly XinYiOffice.DAL.CustomerService dal=new XinYiOffice.DAL.CustomerService();
		public CustomerService()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.CustomerService model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.CustomerService model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.CustomerService GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.CustomerService GetModelByCache(int Id)
		{
			
			string CacheKey = "CustomerServiceModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.CustomerService)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.CustomerService> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.CustomerService> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.CustomerService> modelList = new List<XinYiOffice.Model.CustomerService>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.CustomerService model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.CustomerService();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["Name"]!=null && dt.Rows[n]["Name"].ToString()!="")
					{
					model.Name=dt.Rows[n]["Name"].ToString();
					}
					if(dt.Rows[n]["ByClientInfoId"]!=null && dt.Rows[n]["ByClientInfoId"].ToString()!="")
					{
						model.ByClientInfoId=int.Parse(dt.Rows[n]["ByClientInfoId"].ToString());
					}
					if(dt.Rows[n]["ByPersonnelAccountId"]!=null && dt.Rows[n]["ByPersonnelAccountId"].ToString()!="")
					{
						model.ByPersonnelAccountId=int.Parse(dt.Rows[n]["ByPersonnelAccountId"].ToString());
					}
					if(dt.Rows[n]["ServiceType"]!=null && dt.Rows[n]["ServiceType"].ToString()!="")
					{
						model.ServiceType=int.Parse(dt.Rows[n]["ServiceType"].ToString());
					}
					if(dt.Rows[n]["ServiceMode"]!=null && dt.Rows[n]["ServiceMode"].ToString()!="")
					{
						model.ServiceMode=int.Parse(dt.Rows[n]["ServiceMode"].ToString());
					}
					if(dt.Rows[n]["Sate"]!=null && dt.Rows[n]["Sate"].ToString()!="")
					{
						model.Sate=int.Parse(dt.Rows[n]["Sate"].ToString());
					}
					if(dt.Rows[n]["ExecutorAccountId"]!=null && dt.Rows[n]["ExecutorAccountId"].ToString()!="")
					{
						model.ExecutorAccountId=int.Parse(dt.Rows[n]["ExecutorAccountId"].ToString());
					}
					if(dt.Rows[n]["ServiceContent"]!=null && dt.Rows[n]["ServiceContent"].ToString()!="")
					{
					model.ServiceContent=dt.Rows[n]["ServiceContent"].ToString();
					}
					if(dt.Rows[n]["CustomerFeedback"]!=null && dt.Rows[n]["CustomerFeedback"].ToString()!="")
					{
					model.CustomerFeedback=dt.Rows[n]["CustomerFeedback"].ToString();
					}
					if(dt.Rows[n]["CustomerSatisfaction"]!=null && dt.Rows[n]["CustomerSatisfaction"].ToString()!="")
					{
					model.CustomerSatisfaction=dt.Rows[n]["CustomerSatisfaction"].ToString();
					}
					if(dt.Rows[n]["Remarks"]!=null && dt.Rows[n]["Remarks"].ToString()!="")
					{
					model.Remarks=dt.Rows[n]["Remarks"].ToString();
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["ImpTime"]!=null && dt.Rows[n]["ImpTime"].ToString()!="")
					{
						model.ImpTime=DateTime.Parse(dt.Rows[n]["ImpTime"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

