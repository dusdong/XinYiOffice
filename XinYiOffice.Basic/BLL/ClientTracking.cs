﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 客户跟踪表 对某个销
	/// </summary>
	public partial class ClientTracking
	{
		private readonly XinYiOffice.DAL.ClientTracking dal=new XinYiOffice.DAL.ClientTracking();
		public ClientTracking()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.ClientTracking model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ClientTracking model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ClientTracking GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.ClientTracking GetModelByCache(int Id)
		{
			
			string CacheKey = "ClientTrackingModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.ClientTracking)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ClientTracking> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ClientTracking> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.ClientTracking> modelList = new List<XinYiOffice.Model.ClientTracking>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.ClientTracking model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.ClientTracking();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["ClientHeat"]!=null && dt.Rows[n]["ClientHeat"].ToString()!="")
					{
						model.ClientHeat=int.Parse(dt.Rows[n]["ClientHeat"].ToString());
					}
					if(dt.Rows[n]["SalesOpportunitiesId"]!=null && dt.Rows[n]["SalesOpportunitiesId"].ToString()!="")
					{
						model.SalesOpportunitiesId=int.Parse(dt.Rows[n]["SalesOpportunitiesId"].ToString());
					}
					if(dt.Rows[n]["SalesOpportunitiesSate"]!=null && dt.Rows[n]["SalesOpportunitiesSate"].ToString()!="")
					{
						model.SalesOpportunitiesSate=int.Parse(dt.Rows[n]["SalesOpportunitiesSate"].ToString());
					}
					if(dt.Rows[n]["BackNotes"]!=null && dt.Rows[n]["BackNotes"].ToString()!="")
					{
					model.BackNotes=dt.Rows[n]["BackNotes"].ToString();
					}
					if(dt.Rows[n]["Sate"]!=null && dt.Rows[n]["Sate"].ToString()!="")
					{
						model.Sate=int.Parse(dt.Rows[n]["Sate"].ToString());
					}
					if(dt.Rows[n]["IsFollowUp"]!=null && dt.Rows[n]["IsFollowUp"].ToString()!="")
					{
						model.IsFollowUp=int.Parse(dt.Rows[n]["IsFollowUp"].ToString());
					}
					if(dt.Rows[n]["CloseNotes"]!=null && dt.Rows[n]["CloseNotes"].ToString()!="")
					{
					model.CloseNotes=dt.Rows[n]["CloseNotes"].ToString();
					}
					if(dt.Rows[n]["ReturnAccountId"]!=null && dt.Rows[n]["ReturnAccountId"].ToString()!="")
					{
						model.ReturnAccountId=int.Parse(dt.Rows[n]["ReturnAccountId"].ToString());
					}
					if(dt.Rows[n]["ActualTime"]!=null && dt.Rows[n]["ActualTime"].ToString()!="")
					{
						model.ActualTime=DateTime.Parse(dt.Rows[n]["ActualTime"].ToString());
					}
					if(dt.Rows[n]["PlanTime"]!=null && dt.Rows[n]["PlanTime"].ToString()!="")
					{
						model.PlanTime=DateTime.Parse(dt.Rows[n]["PlanTime"].ToString());
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

