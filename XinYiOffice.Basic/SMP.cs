﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Configuration.Provider;
using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public class SMP : StaticSiteMapProvider
    {

        private bool _Initialized = false;
        /// <summary>
        /// 站点地图根节点
        /// </summary>
        private SiteMapNode _RootNode = null;
        /// <summary>
        /// 清除站点地图中的节点。
        /// </summary>
        protected override void Clear()
        {
            lock (this)
            {
                _RootNode = null;
                base.Clear();
            }
        }

        public void SetSiteMapNode(ref SiteMapNode rootNode, int pid)
        {

            DataRow[] dr = AppDataCacheServer.MenuTreeList().Select(string.Format("MenuTreeId={0}", pid));

            SiteMapNode childNode = null;

            foreach (DataRow _dr in dr)
            {
                string rootNodeId = SafeConvert.ToString(_dr["Id"]);
                string PrentId = SafeConvert.ToString(_dr["MenuTreeId"]);

                childNode = new SiteMapNode(this,
                    SafeConvert.ToString(_dr["Id"]),
                    SafeConvert.ToString(_dr["ChainedAddress"]),
                    SafeConvert.ToString(_dr["Name"]));

                //将图标信息添加到节点的自定义属性中。
                childNode["Icon"] = SafeConvert.ToString(_dr["Icon"]);

                //IsExitesNode(SafeConvert.ToInt(rootNodeId))
                //向根节点中添加子节点。
                if (PrentId == "0"||rootNode == null)
                {
                    rootNode = childNode;
                }
                else
                {
                    childNode.ParentNode = rootNode;
                    AddNode(childNode, rootNode);
                }

                SetSiteMapNode(ref rootNode, SafeConvert.ToInt(rootNodeId));

                //childNode.ChildNodes.Add(rootNode);
            }


        }

        public bool IsExitesNode(int MenuTreeId)
        {
            bool t = false;
            DataRow[] dr = AppDataCacheServer.MenuTreeList().Select(string.Format("MenuTreeId={0}", MenuTreeId));
            if (dr.Length > 0)
            {
                t = true;
            }

            return t;

        }

        /// <summary>
        /// 从数据库中检索站点数据并构建站点地图
        /// </summary>
        /// <returns></returns>
        public override SiteMapNode BuildSiteMap()
        {
            //因为SiteMap类是静态的，所以应确保站点地图被构建完成之前，他不要被修改。
            lock (this)
            {
                //如果提供程序没有被初始化，抛出异常。
                if (!IsInitialized)
                {
                    throw new ProviderException("BuildSiteMap called incorrectly.");
                }

                if (_RootNode == null)
                {
                    //清空节点
                    Clear();
                    // 构造根节点

                    //TODO:具体程序中修改之
                    SetSiteMapNode(ref _RootNode, 0);

                    #region MyRegion
                    //try
                    //{
                    //    if (dt != null && dt.Rows.Count > 0)
                    //    {

                    //        rootNodeId = SafeConvert.ToString(dt.Rows[0]["Id"]);
                    //        // 为当前的 StaticSiteMapProvider创建一个 SiteMapNode 根节点 .
                    //        _RootNode = new SiteMapNode(this,
                    //                                     rootNodeId,
                    //                                     SafeConvert.ToString(dt.Rows[0]["ChainedAddress"]),
                    //                                     SafeConvert.ToString(dt.Rows[0]["Name"]));

                    //    }
                    //    else
                    //    {
                    //        return null;
                    //    }
                    //}
                    //catch
                    //{
                    //}


                    //// 构造子节点
                    ////TODO:具体程序中修改之
                    //DataRow[] dr = dt.Select(string.Format("MenuTreeId={0}", rootNodeId));


                    //SiteMapNode childNode = null;

                    //foreach (DataRow _dr in dr)
                    //{
                    //    childNode = new SiteMapNode(this,
                    //                                      SafeConvert.ToString(dt.Rows[0]["Id"]),
                    //                                     SafeConvert.ToString(dt.Rows[0]["ChainedAddress"]),
                    //                                     SafeConvert.ToString(dt.Rows[0]["Name"]));

                    //    //将图标信息添加到节点的自定义属性中。
                    //    childNode["Icon"] = SafeConvert.ToString(dt.Rows[0]["Icon"]);

                    //    //向根节点中添加子节点。
                    //    AddNode(childNode, _RootNode);
                    //} 
                    #endregion



                }
                //返回构建后的根节点。
                return _RootNode;
            }

        }


        /// <summary>
        /// 获得已经构建完成的根节点。
        /// </summary>
        /// <returns>SiteMap根节点</returns>
        protected override SiteMapNode GetRootNodeCore()
        {
            return RootNode;
        }
        /// <summary>
        /// 获取当前提供程序是否已经被初始化。
        /// </summary>
        public virtual bool IsInitialized
        {
            get
            {
                return _Initialized;
            }
        }
        /// <summary>
        /// 获取SiteMap根节点
        /// </summary>
        public override SiteMapNode RootNode
        {
            get
            {
                SiteMapNode temp = null;
                temp = BuildSiteMap();
                return temp;
            }
        }
        /// <summary>
        /// 初始化提供程序。
        /// </summary>
        /// <param name="name">提供程序的名称</param>
        /// <param name="config">配置参数</param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            if (string.IsNullOrEmpty(name))
            {
                name = "AspNetSqlSiteMapProvider";
            }
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "SiteMapSqlProvider_description");
            }
            base.Initialize(name, config);
            _Initialized = true;
        }
    }


}
