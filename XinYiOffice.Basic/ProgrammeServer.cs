﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace XinYiOffice.Basic
{
    public class ProgrammeServer
    {
        /// <summary>
        /// 根据用户id获取我的日程
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="topNum"></param>
        /// <returns></returns>
        public static DataTable GetMyProgramme(int accountId)
        {
            string mysql = string.Format("select * from vMyProgramme where AccountId={0} or ByAccountId={0}", accountId);

            return Common.DbHelperSQL.Query(mysql).Tables[0];
        }

        public static DataTable GetMyProgramme(int accountId,DateTime dt)
        {
            string mysql = string.Format("select * from vMyProgramme where (AccountId={0} or ByAccountId={0}) and (StartTime>='{1}' and EndTime<='{1}')", accountId,dt.ToString());

            return Common.DbHelperSQL.Query(mysql).Tables[0];
        }

        /// <summary>
        /// 符合当前年,月的全部日程
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DataTable GetProgrammeAll(DateTime dt)
        {
            string mysql = " SELECT * FROM vProgrammeMembers WHERE (DATEPART(month, StartTime) >= {0}) AND (DATEPART(year, StartTime) >= {1})";
            mysql = string.Format(mysql,dt.Month,dt.Year);

            return Common.DbHelperSQL.Query(mysql).Tables[0];
        }
    }
}
