﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 收款单
	/// </summary>
	[Serializable]
	public partial class MakeCollections
	{
		public MakeCollections()
		{}
		#region Model
		private int _id;
		private string _documentnumber;
		private string _documentdescription;
		private int? _methodpayment;
		private string _suppliername;
		private string _gatheringfullname;
		private int? _gatheringaccountid;
		private DateTime? _receiptdate;
		private DateTime? _targetdate;
		private string _remark;
		private string _clientname;
		private decimal? _gatheringamount;
		private int? _gatheringbankaccountid;
		private int? _sate;
		private int? _createaccountid;
		private DateTime? _createtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 单据编号
		/// </summary>
		public string DocumentNumber
		{
			set{ _documentnumber=value;}
			get{return _documentnumber;}
		}
		/// <summary>
		/// 单据描述
		/// </summary>
		public string DocumentDescription
		{
			set{ _documentdescription=value;}
			get{return _documentdescription;}
		}
		/// <summary>
		/// 收款方式
		/// </summary>
		public int? MethodPayment
		{
			set{ _methodpayment=value;}
			get{return _methodpayment;}
		}
		/// <summary>
		/// 供应商
		/// </summary>
		public string SupplierName
		{
			set{ _suppliername=value;}
			get{return _suppliername;}
		}
		/// <summary>
		/// 收款人
		/// </summary>
		public string GatheringFullName
		{
			set{ _gatheringfullname=value;}
			get{return _gatheringfullname;}
		}
		/// <summary>
		/// 收款人帐号
		/// </summary>
		public int? GatheringAccountId
		{
			set{ _gatheringaccountid=value;}
			get{return _gatheringaccountid;}
		}
		/// <summary>
		/// 收款日期
		/// </summary>
		public DateTime? ReceiptDate
		{
			set{ _receiptdate=value;}
			get{return _receiptdate;}
		}
		/// <summary>
		/// 约定收款日期
		/// </summary>
		public DateTime? TargetDate
		{
			set{ _targetdate=value;}
			get{return _targetdate;}
		}
		/// <summary>
		/// 摘要
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 客户
		/// </summary>
		public string ClientName
		{
			set{ _clientname=value;}
			get{return _clientname;}
		}
		/// <summary>
		/// 收款金额
		/// </summary>
		public decimal? GatheringAmount
		{
			set{ _gatheringamount=value;}
			get{return _gatheringamount;}
		}
		/// <summary>
		/// 收款银行账户
		/// </summary>
		public int? GatheringBankAccountId
		{
			set{ _gatheringbankaccountid=value;}
			get{return _gatheringbankaccountid;}
		}
		/// <summary>
		/// 状态 0-应收,1-已收
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 创建账户
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 添加时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

