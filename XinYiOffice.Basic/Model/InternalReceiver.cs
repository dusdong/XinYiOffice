﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 内信接收表 一个人有没有信件，主要看这个表
	/// </summary>
	[Serializable]
	public partial class InternalReceiver
	{
		public InternalReceiver()
		{}
		#region Model
		private int _id;
		private int? _internalletterid;
		private int? _recipientaccountid;
		private int? _recipienttype;
		private int? _isview;
		private int? _isdelete;
		private int? _isreply;
		private int? _letterposition;
		private string _positionfoler;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _checkdate;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 信件ID
		/// </summary>
		public int? InternalLetterId
		{
			set{ _internalletterid=value;}
			get{return _internalletterid;}
		}
		/// <summary>
		/// 收件人ID 为0时，则发布到机构
		/// </summary>
		public int? RecipientAccountId
		{
			set{ _recipientaccountid=value;}
			get{return _recipientaccountid;}
		}
		/// <summary>
		/// 收件人类型 1-个人，2-部门，3-机构若为个人则会出现多条收件人ID与信件ID关联
		/// </summary>
		public int? RecipientType
		{
			set{ _recipienttype=value;}
			get{return _recipienttype;}
		}
		/// <summary>
		/// 是否已查看
		/// </summary>
		public int? IsView
		{
			set{ _isview=value;}
			get{return _isview;}
		}
		/// <summary>
		/// 是否删除
		/// </summary>
		public int? IsDelete
		{
			set{ _isdelete=value;}
			get{return _isdelete;}
		}
		/// <summary>
		/// 是否已回复
		/// </summary>
		public int? IsReply
		{
			set{ _isreply=value;}
			get{return _isreply;}
		}
		/// <summary>
		/// 信件位置 1-收件箱,2-发件箱,3-草稿,4-垃圾箱,5-文件夹
		/// </summary>
		public int? LetterPosition
		{
			set{ _letterposition=value;}
			get{return _letterposition;}
		}
		/// <summary>
		/// 位置标识 文件夹名称
		/// </summary>
		public string PositionFoler
		{
			set{ _positionfoler=value;}
			get{return _positionfoler;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 查看日期
		/// </summary>
		public DateTime? CheckDate
		{
			set{ _checkdate=value;}
			get{return _checkdate;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

