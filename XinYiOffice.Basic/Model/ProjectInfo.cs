﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 项目表 项目,需求说明,工作单,任务，模块，事项
	/// </summary>
	[Serializable]
	public partial class ProjectInfo
	{
		public ProjectInfo()
		{}
		#region Model
		private int _id;
		private int? _parentprojectid;
		private string _projectpropertie;
		private int? _projecttypeid;
		private string _title;
		private string _name;
		private int? _projectmanageraccountid;
		private int? _organiseraccountid;
		private int? _executoraccountid;
		private DateTime? _completiontime;
		private DateTime? _actualfinishtime;
		private decimal? _estdisbursement;
		private decimal? _actualpayment;
		private DateTime? _actualstarttime;
		private DateTime? _esttimestart;
		private int? _completeworkday;
		private int? _actualworkingday;
		private int? _state;
		private string _jobdescription;
		private string _taskdescription;
		private string _url;
		private decimal? _estexpenses;
		private decimal? _actualexpenditure;
		private decimal? _anticipatedrevenue;
		private decimal? _income;
		private int? _predecessortaskprojectid;
		private int? _surveyoraccountid;
		private int? _bysalesopportunitiesid;
		private int? _byclientinfoid;
		private string _remarks;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 所属上级项目ID
		/// </summary>
		public int? ParentProjectId
		{
			set{ _parentprojectid=value;}
			get{return _parentprojectid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProjectPropertie
		{
			set{ _projectpropertie=value;}
			get{return _projectpropertie;}
		}
		/// <summary>
		/// 项目类型Id
		/// </summary>
		public int? ProjectTypeId
		{
			set{ _projecttypeid=value;}
			get{return _projecttypeid;}
		}
		/// <summary>
		/// 项目标题
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 项目名称
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 项目负责人
		/// </summary>
		public int? ProjectManagerAccountId
		{
			set{ _projectmanageraccountid=value;}
			get{return _projectmanageraccountid;}
		}
		/// <summary>
		/// 发起人
		/// </summary>
		public int? OrganiserAccountId
		{
			set{ _organiseraccountid=value;}
			get{return _organiseraccountid;}
		}
		/// <summary>
		/// 执行者
		/// </summary>
		public int? ExecutorAccountId
		{
			set{ _executoraccountid=value;}
			get{return _executoraccountid;}
		}
		/// <summary>
		/// 预计完成时间
		/// </summary>
		public DateTime? CompletionTime
		{
			set{ _completiontime=value;}
			get{return _completiontime;}
		}
		/// <summary>
		/// 项目实际完成时间
		/// </summary>
		public DateTime? ActualFinishTime
		{
			set{ _actualfinishtime=value;}
			get{return _actualfinishtime;}
		}
		/// <summary>
		/// 预计费用
		/// </summary>
		public decimal? EstDisbursement
		{
			set{ _estdisbursement=value;}
			get{return _estdisbursement;}
		}
		/// <summary>
		/// 支付费用
		/// </summary>
		public decimal? ActualPayment
		{
			set{ _actualpayment=value;}
			get{return _actualpayment;}
		}
		/// <summary>
		/// 实际开始时间
		/// </summary>
		public DateTime? ActualStartTime
		{
			set{ _actualstarttime=value;}
			get{return _actualstarttime;}
		}
		/// <summary>
		/// 预计开始时间
		/// </summary>
		public DateTime? EstTimeStart
		{
			set{ _esttimestart=value;}
			get{return _esttimestart;}
		}
		/// <summary>
		/// 完成工作日
		/// </summary>
		public int? CompleteWorkDay
		{
			set{ _completeworkday=value;}
			get{return _completeworkday;}
		}
		/// <summary>
		/// 实际完成工作日
		/// </summary>
		public int? ActualWorkingDay
		{
			set{ _actualworkingday=value;}
			get{return _actualworkingday;}
		}
		/// <summary>
		/// 状态 1-完成,2-打开,3-进行中,4-已搁浅,5-计划
		/// </summary>
		public int? State
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 工作简述
		/// </summary>
		public string JobDescription
		{
			set{ _jobdescription=value;}
			get{return _jobdescription;}
		}
		/// <summary>
		/// 工作描述
		/// </summary>
		public string TaskDescription
		{
			set{ _taskdescription=value;}
			get{return _taskdescription;}
		}
		/// <summary>
		/// 网址
		/// </summary>
		public string URL
		{
			set{ _url=value;}
			get{return _url;}
		}
		/// <summary>
		/// 预计支出
		/// </summary>
		public decimal? EstExpenses
		{
			set{ _estexpenses=value;}
			get{return _estexpenses;}
		}
		/// <summary>
		/// 实际支出
		/// </summary>
		public decimal? ActualExpenditure
		{
			set{ _actualexpenditure=value;}
			get{return _actualexpenditure;}
		}
		/// <summary>
		/// 预计收入
		/// </summary>
		public decimal? AnticipatedRevenue
		{
			set{ _anticipatedrevenue=value;}
			get{return _anticipatedrevenue;}
		}
		/// <summary>
		/// 实际收入
		/// </summary>
		public decimal? Income
		{
			set{ _income=value;}
			get{return _income;}
		}
		/// <summary>
		/// 前置任务ID
		/// </summary>
		public int? PredecessorTaskProjectId
		{
			set{ _predecessortaskprojectid=value;}
			get{return _predecessortaskprojectid;}
		}
		/// <summary>
		/// 验收人
		/// </summary>
		public int? SurveyorAccountId
		{
			set{ _surveyoraccountid=value;}
			get{return _surveyoraccountid;}
		}
		/// <summary>
		/// 来源销售机会
		/// </summary>
		public int? BySalesOpportunitiesId
		{
			set{ _bysalesopportunitiesid=value;}
			get{return _bysalesopportunitiesid;}
		}
		/// <summary>
		/// 所属客户
		/// </summary>
		public int? ByClientInfoId
		{
			set{ _byclientinfoid=value;}
			get{return _byclientinfoid;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

