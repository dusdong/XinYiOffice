﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 项目类型表
	/// </summary>
	[Serializable]
	public partial class ProjectTypes
	{
		public ProjectTypes()
		{}
		#region Model
		private int _id;
		private string _typename;
		private string _typedescribe;
		private string _typeico;
		private string _typeicomax;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 项目类型名
		/// </summary>
		public string TypeName
		{
			set{ _typename=value;}
			get{return _typename;}
		}
		/// <summary>
		/// 类型描述
		/// </summary>
		public string TypeDescribe
		{
			set{ _typedescribe=value;}
			get{return _typedescribe;}
		}
		/// <summary>
		/// 类型小图标 图标路径
		/// </summary>
		public string TypeIco
		{
			set{ _typeico=value;}
			get{return _typeico;}
		}
		/// <summary>
		/// 类型大图标 大图标路径
		/// </summary>
		public string TypeIcoMax
		{
			set{ _typeicomax=value;}
			get{return _typeicomax;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

