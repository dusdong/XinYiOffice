﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 资源及附件表
	/// </summary>
	[Serializable]
	public partial class ResAttachment
	{
		public ResAttachment()
		{}
		#region Model
		private int _id;
		private string _keytable;
		private int? _keyid;
		private string _title;
		private string _con;
		private int? _views;
		private string _annex;
		private int? _resfolderid;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 关键表名
		/// </summary>
		public string KeyTable
		{
			set{ _keytable=value;}
			get{return _keytable;}
		}
		/// <summary>
		/// 关键ID
		/// </summary>
		public int? KeyId
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 标题
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 内容
		/// </summary>
		public string Con
		{
			set{ _con=value;}
			get{return _con;}
		}
		/// <summary>
		/// 查看数
		/// </summary>
		public int? Views
		{
			set{ _views=value;}
			get{return _views;}
		}
		/// <summary>
		/// 附件地址
		/// </summary>
		public string Annex
		{
			set{ _annex=value;}
			get{return _annex;}
		}
		/// <summary>
		/// 所属文件夹Id
		/// </summary>
		public int? ResFolderId
		{
			set{ _resfolderid=value;}
			get{return _resfolderid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

