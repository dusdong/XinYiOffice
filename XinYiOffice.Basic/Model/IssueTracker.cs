﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 任务/议题追踪表
	/// </summary>
	[Serializable]
	public partial class IssueTracker
	{
		public IssueTracker()
		{}
		#region Model
		private int _id;
		private int? _projectinfoid;
		private string _title;
		private string _tabloid;
		private string _item;
		private string _priority;
		private string _solver;
		private int? _sate;
		private string _version;
		private string _url;
		private string _constitute;
		private string _serious;
		private int? _authorsaccountid;
		private int? _assigneraccountid;
		private DateTime? _starttime;
		private DateTime? _endtime;
		private int? _type;
		private int? _isedit;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 项目ID
		/// </summary>
		public int? ProjectInfoId
		{
			set{ _projectinfoid=value;}
			get{return _projectinfoid;}
		}
		/// <summary>
		/// 标题
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 摘要
		/// </summary>
		public string Tabloid
		{
			set{ _tabloid=value;}
			get{return _tabloid;}
		}
		/// <summary>
		/// 内容明细
		/// </summary>
		public string Item
		{
			set{ _item=value;}
			get{return _item;}
		}
		/// <summary>
		/// 优先级
		/// </summary>
		public string Priority
		{
			set{ _priority=value;}
			get{return _priority;}
		}
		/// <summary>
		/// 解决者
		/// </summary>
		public string Solver
		{
			set{ _solver=value;}
			get{return _solver;}
		}
		/// <summary>
		/// 状态 1-开启,2-已解决,3-不准备解决,4-无法解决,5-无法重现
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 版本
		/// </summary>
		public string Version
		{
			set{ _version=value;}
			get{return _version;}
		}
		/// <summary>
		/// URL
		/// </summary>
		public string Url
		{
			set{ _url=value;}
			get{return _url;}
		}
		/// <summary>
		/// 组成
		/// </summary>
		public string Constitute
		{
			set{ _constitute=value;}
			get{return _constitute;}
		}
		/// <summary>
		/// 严重
		/// </summary>
		public string Serious
		{
			set{ _serious=value;}
			get{return _serious;}
		}
		/// <summary>
		/// 发起者
		/// </summary>
		public int? AuthorsAccountId
		{
			set{ _authorsaccountid=value;}
			get{return _authorsaccountid;}
		}
		/// <summary>
		/// 解决者
		/// </summary>
		public int? AssignerAccountId
		{
			set{ _assigneraccountid=value;}
			get{return _assigneraccountid;}
		}
		/// <summary>
		/// 开始时间
		/// </summary>
		public DateTime? StartTime
		{
			set{ _starttime=value;}
			get{return _starttime;}
		}
		/// <summary>
		/// 结案时间
		/// </summary>
		public DateTime? EndTime
		{
			set{ _endtime=value;}
			get{return _endtime;}
		}
		/// <summary>
		/// 所属类型
		/// </summary>
		public int? TYPE
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 是否允许修改 若被管理员锁定，则不能修改任何信息
		/// </summary>
		public int? IsEdit
		{
			set{ _isedit=value;}
			get{return _isedit;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

