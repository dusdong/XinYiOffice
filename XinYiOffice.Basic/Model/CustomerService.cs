﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 客户服务
	/// </summary>
	[Serializable]
	public partial class CustomerService
	{
		public CustomerService()
		{}
		#region Model
		private int _id;
		private string _name;
		private int? _byclientinfoid;
		private int? _bypersonnelaccountid;
		private int? _servicetype;
		private int? _servicemode;
		private int? _sate;
		private int? _executoraccountid;
		private string _servicecontent;
		private string _customerfeedback;
		private string _customersatisfaction;
		private string _remarks;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _imptime;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 服务名称
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 所属公司
		/// </summary>
		public int? ByClientInfoId
		{
			set{ _byclientinfoid=value;}
			get{return _byclientinfoid;}
		}
		/// <summary>
		/// 所属人员
		/// </summary>
		public int? ByPersonnelAccountId
		{
			set{ _bypersonnelaccountid=value;}
			get{return _bypersonnelaccountid;}
		}
		/// <summary>
		/// 服务类型
		/// </summary>
		public int? ServiceType
		{
			set{ _servicetype=value;}
			get{return _servicetype;}
		}
		/// <summary>
		/// 服务方式
		/// </summary>
		public int? ServiceMode
		{
			set{ _servicemode=value;}
			get{return _servicemode;}
		}
		/// <summary>
		/// 状态
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 执行人
		/// </summary>
		public int? ExecutorAccountId
		{
			set{ _executoraccountid=value;}
			get{return _executoraccountid;}
		}
		/// <summary>
		/// 服务内容
		/// </summary>
		public string ServiceContent
		{
			set{ _servicecontent=value;}
			get{return _servicecontent;}
		}
		/// <summary>
		/// 客户反馈
		/// </summary>
		public string CustomerFeedback
		{
			set{ _customerfeedback=value;}
			get{return _customerfeedback;}
		}
		/// <summary>
		/// 客户满意度
		/// </summary>
		public string CustomerSatisfaction
		{
			set{ _customersatisfaction=value;}
			get{return _customersatisfaction;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 实施时间
		/// </summary>
		public DateTime? ImpTime
		{
			set{ _imptime=value;}
			get{return _imptime;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

