﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using XinYiOffice.Common;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;

namespace XinYiOffice.Elf
{
    public class SettingService
    {

        public static bool DataIsNoNull(params object[] obj)
        {
            bool t = true;
            for (int i = 0; i < obj.Length; i++)
            {
                if (obj[i] == null || obj[i].ToString() == "")
                {
                    t = false;
                    break;
                }
            }

            return t;
        }


        /// <summary>
        /// 判断服务器是否正确
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool GetvUrl(string url)
        {
            bool t = false;

            try
            {
                if (url.IndexOf("http://") > -1)
                {
                    string file = System.IO.Path.GetExtension(url);
                    if (string.IsNullOrEmpty(file))
                    {
                        url = url.TrimEnd('/');

                        url = url + "/api/authority.ashx";

                        string data = XinYiOffice.Common.Web.Post.GetPage(url, "utf-8", string.Empty);
                        JObject jo = (JObject)JsonConvert.DeserializeObject(data);
                        //string json_data = jo["data"].ToString();
                        //jo = (JObject)JsonConvert.DeserializeObject(json_data);

                        string code = jo["code"].ToString();

                        if (!string.IsNullOrEmpty(code) && code == "-99")
                        {
                            t = true;
                        }

                    }

                }
                else
                {
                    //不正确
                }
            }
            catch
            {
            }



            return t;
        }

        /// <summary>
        /// 获取配置数据
        /// </summary>
        /// <param name="server_url"></param>
        /// <param name="dname"></param>
        /// <param name="username"></param>
        /// <param name="pwd"></param>
        /// <param name="start"></param>
        /// <param name="homepage"></param>
        /// <returns></returns>
        public static bool GetInitData(ref string server_url, ref string dname, ref string username, ref string pwd, ref string start, ref string homepage)
        {

            server_url = ConfigurationManager.AppSettings["server_url"];
            dname = ConfigurationManager.AppSettings["dname"];
            username = ConfigurationManager.AppSettings["username"];
            pwd = ConfigurationManager.AppSettings["pwd"];
            start = ConfigurationManager.AppSettings["start"];
            homepage = ConfigurationManager.AppSettings["homepage"];

            bool t = false;

            if (DataIsNoNull(server_url, dname, username, pwd, start, homepage))
            {
                t = true;
            }
            else
            {
                t = false;
            }

            return t;
        }


        /// <summary>
        /// 获取基础配置数据
        /// </summary>
        /// <returns></returns>
        public static bool GetInitData()
        {

            string server_url = ConfigurationManager.AppSettings["server_url"];
            string dname = ConfigurationManager.AppSettings["dname"];
            string username = ConfigurationManager.AppSettings["username"];
            string pwd = ConfigurationManager.AppSettings["pwd"];
            string start = ConfigurationManager.AppSettings["start"];
            string homepage = ConfigurationManager.AppSettings["homepage"];

            bool t = false;

            if (DataIsNoNull(server_url, dname, username, pwd, start, homepage))
            {
                t = true;
            }
            else
            {
                t = false;
            }

            return t;
        }

        /// <summary>
        /// 保存设置
        /// </summary>
        /// <param name="url"></param>
        /// <param name="dname"></param>
        /// <param name="uid"></param>
        /// <param name="pwd"></param>
        /// <param name="start"></param>
        /// <param name="homepage"></param>
        /// <returns></returns>
        public static bool SetInitData(string url, string dname, string uid, string pwd, string start, string homepage)
        {
            bool t = false;
            try
            {
                Configuration cfa = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                cfa.AppSettings.Settings["server_url"].Value = url;
                cfa.AppSettings.Settings["dname"].Value = dname;
                cfa.AppSettings.Settings["username"].Value = uid;
                cfa.AppSettings.Settings["pwd"].Value = pwd;
                cfa.AppSettings.Settings["start"].Value = start;
                cfa.AppSettings.Settings["homepage"].Value = homepage;
                cfa.Save();
                t = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("保存出错:" + ex.Message.ToString());
            }

            return t;

        }



        public static void SetSystemStart(System.Windows.Forms.CheckBox  AutoMenu,System.Windows.Forms.Form win)
        {
            string strName = Application.ExecutablePath;
            string strnewName = strName.Substring(strName.LastIndexOf("\\") + 1);
            if (AutoMenu.Checked)
            {
                //修改注册表，使程序开机时不自动执行。  
                AutoMenu.Checked = false;
                Microsoft.Win32.RegistryKey Rkey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
                Rkey.DeleteValue(strnewName, false);
                //MessageBox.Show("程序设置完成！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                AutoMenu.Checked = true;
                if (!File.Exists(strName))//指定文件是否存在  
                    return;
                Microsoft.Win32.RegistryKey Rkey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (Rkey == null)
                    Rkey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
                Rkey.SetValue(strnewName, strName);//修改注册表，使程序开机时自动执行。  
                //MessageBox.Show("程序设置完成，重新启动计算机后即可生效！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    
    }
}
